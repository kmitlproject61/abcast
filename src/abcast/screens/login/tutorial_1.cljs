(ns abcast.screens.login.tutorial-1
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.requires.images :as image]
            [abcast.constance :as constance]))

(defn runAnimation [this]
  (-> (.sequence c/animated
                 #js [(-> (.timing c/animated (.. this -state -bounceValue) #js {:toValue -80 :duration 1000}))
                      (-> (.timing c/animated (.. this -state -bounceValue) #js {:toValue -100 :duration 1000}))])
      (.start (fn [] (runAnimation this)))))

(defn ClockHuman []
  (fn [this]
    [c/View {:style {:margin-top 70}}
     [c/Image {:source image/clock :style {:width 180 :height 180}}]
     [c/AnimatedImage {:source image/human :style {:margin-left 100 :margin-top (.. this -state -bounceValue)}}]]))

(defn Tutorial1 []
  (r/create-class
   {:get-initial-state
    #(clj->js {:bounceValue (new c/animated-value -100)})
    :component-did-mount
    (fn [this]
      (.setValue (.. this -state -bounceValue) -100)
      (runAnimation this))
    :reagent-render
    (fn []
      (let [this (r/current-component)]
        [c/View {:style {:width "100%" :height "100%" :padding s/ml}}
         [c/Text {:style {:color s/black :font-size s/txt-xlg :font-weight s/txt-bold :align-self "center"}} "Save your time a lot"]
         [ClockHuman this]
         [c/View {:style {:max-width 250 :position "absolute" :bottom 80 :margin-left 100}}
          [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} "Not to take attendance with calling or yelling anymore."]]]))}))
