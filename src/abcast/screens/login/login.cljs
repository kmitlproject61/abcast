(ns abcast.screens.login.login
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.requires.images :as image]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.screens.login.tutorial-1 :refer [Tutorial1]]
            [abcast.screens.login.tutorial-2 :refer [Tutorial2]]
            [abcast.utils.web3 :as w]
            [abcast.constance :as constance]
            [abcast.utils.authenticate :refer [signout-google google-config]]
            [abcast.states.global-state :as host-state]
            [abcast.states.attender-global-state :as attender-state]
            [abcast.components.modal :as modal]))

;; --------------------------
;; ---------- Atom ----------

(def signing-in (atom false))
(def logged-in (atom false))
(def popup-login (atom false))
(def prikey-form (atom ""))
(def prikey-form-err (atom ""))

;; --------------------------
;; -------- Function --------

(defn save-state [gmail myAddress]
  (if-not (nil? (re-matches #"[0-9]{8}@kmitl.ac.th" gmail.user.email))
    (do
      (set! gmail.user.role "student")
      (reset! attender-state/account gmail.user)
      (reset! attender-state/address myAddress))
    (do
      (set! gmail.user.role "teacher")
      (reset! host-state/account gmail.user)
      (reset! host-state/address myAddress)
      (reset! host-state/idToken gmail.idToken)))
  (-> (.setItem c/AsyncStorage "account" (js/JSON.stringify gmail.user))
      (.then #(-> (.setItem c/AsyncStorage "address" myAddress)
                  (.then (fn []
                           (reset! logged-in gmail.user.role)
                           (reset! popup-login false)
                           (reset! signing-in false)))))))

(defn create-user [gmail myAddress]
  "Verify gmail with idToken and then send ether and create contract user at backend"
  (if-not js/goog.DEBUG
    (.getAddress c/AbcastBluetooth (fn [bt-addr]
                                     (if-not (empty? bt-addr)
                                       (let [body (js/JSON.stringify #js {:address myAddress :btAddr bt-addr})]
                                         (-> (js/fetch constance/api-register #js {:method "POST"
                                                                                   :headers #js {:Content-Type "application/json"
                                                                                                 :Authorization (str "Bearer " gmail.idToken)}
                                                                                   :body body})
                                             (.then #(.json %))
                                             (.then (fn [response]
                                                      (if (= response.success false)
                                                        (cond (= response.error "EMAIL_MUST_BE_KMITL") (throw "Please sign in with KMITL's email")
                                                              (= response.error "EMAIL_HAS_NOT_VERIFIED") (throw "Email hasn't been verified")
                                                              (= response.error "INVALID_TOKEN") (throw "Invalid account")
                                                              (= response.error "INVALID_KMITL_EMAIL") (throw "KMITL mail is invalid")
                                                              (= response.error "CREATE_ACCOUNT_FAILED") (throw "Create account failed")
                                                              (= response.error "TRANSFER_ETHER_FAILED") (throw "Create blockchain user failed")
                                                              :else (throw "Create account failed"))
                                                      ; Register Successfull
                                                        (save-state gmail myAddress))))
                                             (.catch (fn [e]
                                                       (js/alert e)
                                                       (prn "Verify google account invalid")
                                                       (reset! signing-in false)
                                                       (signout-google))))))))
    (let [body (js/JSON.stringify #js {:address myAddress :btAddr "AA:AA:AA:AA:AA:AA"})]
      (-> (js/fetch constance/api-register #js {:method "POST"
                                                :headers #js {:Content-Type "application/json"
                                                              :Authorization (str "Bearer " gmail.idToken)}
                                                :body body})
          (.then #(.json %))
          (.then (fn [response]
                   (if (= response.success false)
                     (cond (= response.error "EMAIL_MUST_BE_KMITL") (throw "Please sign in with KMITL's email")
                           (= response.error "EMAIL_HAS_NOT_VERIFIED") (throw "Email hasn't been verified")
                           (= response.error "INVALID_TOKEN") (throw "Invalid account")
                           (= response.error "INVALID_KMITL_EMAIL") (throw "KMITL mail is invalid")
                           (= response.error "CREATE_ACCOUNT_FAILED") (throw "Create account failed")
                           (= response.error "TRANSFER_ETHER_FAILED") (throw "Create blockchain user failed")
                           :else (throw "Create account failed"))
      ; Register Successfull
                     (save-state gmail myAddress))))
          (.catch (fn [e]
                    (js/alert e)
                    (prn "Verify google account invalid")
                    (reset! signing-in false)
                    (signout-google)))))))

(defn login-user [gmail myAddress]
  (prn myAddress)
  "Verify gmail and blockchain address are exist or not"
  (let [body (js/JSON.stringify #js {:address myAddress})]
    (-> (js/fetch constance/api-login #js {:method "POST"
                                           :headers #js {:Content-Type "application/json"
                                                         :Authorization (str "Bearer " gmail.idToken)}
                                           :body body})
        (.then #(.json %))
        (.then (fn [response]
                 (if (= (.-success response) false)
                   (cond (= (.-error response) "EMAIL_MUST_BE_KMITL") (throw "Please sign in with KMITL's email")
                         (= (.-error response) "EMAIL_HAS_NOT_VERIFIED") (throw "Email hasn't been verified")
                         (= (.-error response) "INVALID_TOKEN") (throw "Invalid account")
                         (= (.-error response) "INVALID_KMITL_EMAIL") (throw "KMITL mail is invalid")
                         :else (throw "Login failed"))
                   ; Login Successfull
                   (save-state gmail myAddress))))
        (.catch (fn [e]
                  (js/alert e)
                  (prn "Verify google account invalid")
                  (reset! signing-in false)
                  (signout-google))))))

(defn signin-google-oauth [myAddress is-login]
  (google-config)
  (try
    (-> (.signIn c/google-signin)
        (.then (fn [gmail]
                 (reset! signing-in true)
                 (if (= is-login true)
                   (login-user gmail myAddress)
                   (create-user gmail myAddress))))
        (.catch (fn [e]
                  (reset! signing-in false)
                  (js/alert "Google sign in failed!"))))
    (catch js/Error e
      (reset! signing-in false)
      (prn "Sign in google oauth failed")
      (js/alert e))))

(defn create-wallet []
  (let [myAccount (-> w/web3 .-eth .-accounts .create)]
    (-> w/web3 .-eth .-accounts .-wallet .clear)
    (-> w/web3 .-eth .-accounts .-wallet (.create 0))
    (-> w/web3 .-eth .-accounts .-wallet (.add (.-address myAccount)))
    (let [myAddress (-> w/web3 .-eth .-accounts (.privateKeyToAccount (.-address myAccount)))]
      (try
        (.setItem c/AsyncStorage "pk" myAddress.privateKey)
        (signin-google-oauth myAddress.address false)
        (catch js/Error e
          (reset! signing-in false)
          (prn "Cannot store pk")
          (js/alert "Create wallet failed"))))))

(defn handle-register []
  (-> (.getItem c/AsyncStorage "account")
      (.then (fn [account]
               (if (nil? account)
                 (create-wallet))))
      (.catch (fn [e] (prn "Account is already exist")))))

(defn prikey-towallet []
  (try
    (let [myAccount (-> w/web3 .-eth .-accounts (.privateKeyToAccount @prikey-form))]
      (-> w/web3 .-eth .-accounts .-wallet .clear)
      (-> w/web3 .-eth .-accounts .-wallet (.create 0))
      (-> w/web3 .-eth .-accounts .-wallet (.add myAccount))
      (let [myAddress (-> w/web3 .-eth .-accounts (.privateKeyToAccount (.-privateKey myAccount)))]
        (try
          (.setItem c/AsyncStorage "pk" @prikey-form)
          (signin-google-oauth myAddress.address true)
          (catch js/Error e
            (reset! signing-in false)
            (js/alert "Cannot store keystore")))))
    (catch js/Error e
      (reset! prikey-form-err "Invalid private key"))))

(defn handle-login []
  (-> (.getItem c/AsyncStorage "account")
      (.then (fn [account]
               (if (nil? account)
                 (prikey-towallet))))
      (.catch (fn [e] (prn "Account is already exist")))))


(comment
  (-> (.removeItem c/AsyncStorage "keystore"))
  (-> (.clear c/AsyncStorage))
  (-> (.getItem c/AsyncStorage "account")
      (.then (fn [r]
               (prn (js/JSON.parse r))))
      (.catch (fn [e] (js/alert "Cannot save item in localstorage"))))
  (google-config)
  (signout-google)
  (handle-register)
  ; Student
  (do
    (reset! prikey-form "0x00cfdAE8D7870B6F59448D47a364Df94dC14b2C5")
    (handle-login))
  ; Teacher
  (do
    (reset! prikey-form "0xE37B672A655FB0792Eb2cAdA90Ce0a597362b4b6")
    (handle-login)))

;; --------------------------
;; -------- View --------

(defn LoginModal []
  (fn []
    [modal/Custom
     ""
     "Sign-in"
     "Cancel"
     "Sign-in"
     @popup-login
     #(reset! popup-login false)
     #(handle-login)
     [c/View
      [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} "Private Key"]
       [c/TextInput {:style {:padding 10 :width 220 :color s/black :font-size s/txt-sm}
                     :underlineColorAndroid s/primary
                     :on-change-text (fn [value] (reset! prikey-form-err "") (reset! prikey-form value))
                     :placeholder "Ex. 0xE298982Bf0b531Df92..."}]]
      (if-not (empty? @prikey-form-err)
        [c/Text {:style {:align-self "center" :font-size s/txt-sm :color s/danger}} @prikey-form-err])]]))

(defn CircleBlue []
  (fn []
    [c/View {:style {:position "absolute" :bottom 0 :left -90}}
     [c/View {:style {:width 200
                      :height 200
                      :border-radius 100
                      :background-color s/primary}}]]))

(defn CircleSuccess []
  (fn []
    [c/View {:style {:position "absolute" :top 80 :right -100}}
     [c/View {:style {:width 200
                      :height 200
                      :border-radius 100
                      :background-color s/success}}]]))

(defn CircleWarning []
  (fn []
    [c/View {:style {:position "absolute" :bottom (+ 90 10) :right 10}}
     [c/View {:style {:width 60
                      :height 60
                      :border-radius 30
                      :background-color s/warning}}]]))

(defn LoginGoogleButton []
  (fn []
    [c/TouchableOpacity {:style (merge s/btn s/btn-lg {:flex-direction "row" :width 200 :border-color "rgba(22, 22, 22, 0.6)"})
                         :on-press #(handle-register)}
     [c/Image {:source image/google-logo :style {:width 20 :height 20 :margin-right 24}}]
     [c/Text {:style {:font-weight s/txt-bold}} "Sign up with Google"]]))

(defn Tutorial []
  (fn []
    [c/View {:flex 1 :background-color "red"}]
    [c/Swiper {:showButtons true :index 0 :loop false :style {:height (- constance/HEIGHT 90 constance/STATUSBAR-HEIGHT)}}
     [Tutorial1]
     [Tutorial2]]))

(defn LoginScreen []
  (fn []
    [c/View {:style {:flex 1}}
     [CircleWarning]
     [CircleBlue]
     [CircleSuccess]
     [Tutorial]
     [c/View {:style {:background-color "transparent"}}
      [c/View {:style {:width "100%" :height 90
                       :background-color "rgba(255,255,255,1)"
                       :elevation 10
                       :align-items "center"
                       :justify-content "space-around"
                       :padding-top 16 :padding-bottom 10}}
       [LoginGoogleButton]
       [c/View {:style {:flex-direction "row"}}
        [c/Text {:style {:font-size s/txt-xsm :color s/black :opacity 0.6}} "Already have an account.  "]
        [c/TouchableOpacity {:on-press #(reset! popup-login true)}
         [c/Text {:style {:font-size s/txt-xsm :color s/primary :opacity 0.6}} "Sign-in here"]]]]]]))
