(ns abcast.screens.login.tutorial-2
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.requires.images :as image]))
              ; [abcast.constance :as constance]))


(defn runAnimation [this]
  (-> (.sequence c/animated
                 #js [(-> (.timing c/animated (.. this -state -bounceValue) #js {:toValue -80 :duration 1000}))
                      (-> (.timing c/animated (.. this -state -bounceValue) #js {:toValue -100 :duration 1000}))])
      (.start (fn [] (runAnimation this)))))

(defn StepToUse []
  (fn []
    [c/View
     [c/View {:style {:margin-top 70 :flex-direction "row" :height 55 :width 200}}
      [c/View {:style {:width 55 :height 55 :elevation 2 :border-radius 28 :justify-content "center" :align-items "center" :background-color s/white}}
       [c/Image {:source image/bluetooth :style {:width 34 :height 34}}]]
      [c/View {:style {:justify-content "center" :margin-left 10}}
       [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} "Enable Bluetooth"]]]
     [c/View {:style {:margin-top 40 :flex-direction "row" :height 55 :width 200 :margin-left 60}}
      [c/View {:style {:width 55 :height 55 :elevation 2 :border-radius 28 :justify-content "center" :align-items "center" :background-color s/white}}
       [c/Image {:source image/finger :style {:width 34 :height 34}}]]
      [c/View {:style {:justify-content "center" :margin-left 10}}
       [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} "Press Scan"]]]
     [c/View {:style {:margin-top 40 :flex-direction "row" :height 55 :width 200 :margin-left 120}}
      [c/View {:style {:width 55 :height 55 :elevation 2 :border-radius 28 :justify-content "center" :align-items "center" :background-color s/white}}
       [c/Image {:source image/emoji :style {:width 34 :height 34}}]]
      [c/View {:style {:justify-content "center" :margin-left 10}}
       [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} "Do your activity"]]]]))

(defn Tutorial2 []
  (r/create-class
   {:component-did-mount
    (fn [])
    :reagent-render
    (fn []
      [c/View {:style {:width "100%" :height "100%" :padding s/ml}}
       [c/Text {:style {:color s/black :font-size s/txt-xlg :font-weight s/txt-bold :align-self "center"}} "Easy step to take attendance"]
       [StepToUse]
       [c/View {:style {:max-width 250 :position "absolute" :bottom 80 :margin-left 100}}
        [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} "Skip \"Press Scan\" if you are attender."]]])}))
