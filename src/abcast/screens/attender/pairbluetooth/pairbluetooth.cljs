(ns abcast.screens.attender.pairbluetooth.pairbluetooth
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.utils.bluetooth-service :refer [init-bluetooth-config bluetooth-on switch-bluetooth on-state-change pairedDevices]]))

;; --------------------------
;; -------- Atom --------

(def scaning-opacity (atom 0.5))
(def is-scan (atom false))
(def devices (atom []))

;; --------------------------
;; -------- Function --------

(defn on-device-found [device]
  (let [d device]
    (js-delete d "rssi")
    (js-delete d "uuids")
    (prn device)
    (if (not= (.-name d) nil)
      (if (= (.indexOf @devices d) -1)
        (reset! devices (conj @devices d))))))

(defn bluetooth-scan []
  (reset! devices [])
  (reset! is-scan true)
  (-> (.startScan c/bluetooth-classic)
      (.then (fn []
               (reset! is-scan false)))
      (.catch (fn [err]
                (js/alert "Something wrong with scan bluetooth")
                (reset! is-scan false)))))

(defn bluetooth-connect [device]
  (-> (.connect c/bluetooth-classic device)
      (.then (fn []
               (js/alert "connected")))
      (.catch (fn [err]
                (js/alert "Error with bluetooth connection")
                (prn err)))))

;; --------------------------
;; -------- View --------

(defn Header []
  (fn []
    (let [[scan-color scan-opacity] [(if (= @bluetooth-on true) s/primary s/black) (if (= @bluetooth-on true) 1 0.5)]]
      [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white}}
       [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
        [c/Text {:style {:font-size s/txt-xlg
                         :font-weight s/txt-bold
                         :color s/black
                         :margin-bottom 10}}
         "Bluetooth\nconfiguration"]
        [c/TouchableOpacity {:style {:margin-right 20 :align-items "center"} :on-press #(bluetooth-scan)}
         [c/View {:style {:flex-direction "row" :justify-content "center" :align-items "center"}}
          [c/Text {:style {:font-size s/txt-md :font-weight s/txt-bold :color scan-color :opacity scan-opacity :margin-right 5}} "("]
          [c/Text {:style {:font-size s/txt-lg :font-weight s/txt-bold :color scan-color :opacity scan-opacity :margin-right 5}} "("]
          [c/Icon {:name "md-bluetooth" :color scan-color :size s/txt-xxxlg :style {:opacity scan-opacity}}]
          [c/Text {:style {:font-size s/txt-lg :font-weight s/txt-bold :color scan-color :opacity scan-opacity :margin-left 5}} ")"]
          [c/Text {:style {:font-size s/txt-md :font-weight s/txt-bold :color scan-color :opacity scan-opacity :margin-left 5}} ")"]]
         [c/Text {:style {:font-size s/txt-sm :color scan-color :opacity scan-opacity :margin-right 5 :font-weight s/txt-bold}}
          (if (= @is-scan true) "Scaning..." "Scan now")]]]])))

(defn Menu []
  (fn []
    [c/View
     [c/View {:style {:padding s/ml :background-color s/white :margin-top 5}}
      [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
       [c/Text {:style {:font-size s/txt-sm :color s/black}} "Bluetooth Enable"]
       [c/SwitchToggle {:switchOn @bluetooth-on
                        :onPress #(switch-bluetooth)
                        :containerStyle {:width 42 :height 20 :border-radius 10 :background-color s/white :padding 0}
                        :circleStyle {:width 20 :height 20 :border-radius 14}
                        :circleColorOff "gray"
                        :circleColorOn s/primary
                        :duration 200}]]]]))

(defn MenuScanning []
  (fn []
    [c/View
     ;Paired devices
     [c/View {:style {:padding s/ml :background-color s/white :margin-top 5}}
      [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight s/txt-bold}} "Paired devices"]]
      (for [[i device] (map-indexed vector @pairedDevices)]
        [c/TouchableOpacity {:style {:padding-top s/ml :flex-direction "row" :justify-content "space-between" :align-items "center"}
                             :key i
                             :on-press #(bluetooth-connect device)}
         [c/View {:style {:flex-direction "row" :align-items "center"}}
          [c/View {:style {:width s/txt-xlg :height s/txt-xlg :margin-right 10 :border-radius (/ s/txt-xlg 2) :justify-content "center" :align-items "center"
                           :background-color (cond (not= (re-matches #"\d{8}" (.-name device)) nil) s/secondary
                                                   (not= (re-matches #"[a-z]+\.[a-z][a-z]" (.-name device)) nil) s/success
                                                   :else s/warning)}}
           [c/Icon {:color s/white
                    :size s/txt-md
                    :name (cond (= (-> device :role) "teacher") "md-person"
                                (= (-> device :role) "student") "md-contacts"
                                :else "md-phone-portrait")}]]
          [c/Text {:style {:font-size s/txt-sm :color s/black}} (.-name device)]]
         [c/Icon {:name "ios-arrow-dropright" :color s/black :size s/txt-md :style {:opacity 0.6}}]])]
     ;Avialable part
     [c/View {:style {:padding s/ml :background-color s/white :margin-top 5}}
      [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight s/txt-bold}} "Avialable devices"]]
      (for [[i device] (map-indexed vector @devices)]
        [c/TouchableOpacity {:style {:padding-top s/ml :flex-direction "row" :justify-content "space-between" :align-items "center"}
                             :on-press #(bluetooth-connect device)
                             :key i}
         [c/View {:style {:flex-direction "row" :align-items "center"}}
          [c/View {:style {:width s/txt-xlg :height s/txt-xlg :margin-right 10 :border-radius (/ s/txt-xlg 2) :justify-content "center" :align-items "center" :background-color s/secondary}}
           [c/Icon {:color s/white :size s/txt-md :name "md-information"}]]
          [c/Text {:style {:font-size s/txt-sm :color s/black}} (.-name device)]]
         [c/Icon {:name "ios-arrow-dropright" :color s/black :size s/txt-md :style {:opacity 0.6}}]])]]))

(defn PairScreen []
  (r/create-class
   {:component-will-mount
    (fn []
      (init-bluetooth-config)
      (.addOnDeviceFoundListener c/bluetooth-classic on-device-found)
      (.addEventListener c/RNBluetoothInfo "change" on-state-change))
    :reagent-render
    (fn []
      [c/ScrollView {:style {:flex 1}}
       [Header]
       [Menu]
       (if (= @bluetooth-on true)
         [MenuScanning])])}))
