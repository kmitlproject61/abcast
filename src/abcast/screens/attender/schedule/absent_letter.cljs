(ns abcast.screens.attender.schedule.absent-letter
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.utils.schedule-date :as schedule-date]))

(defn Header []
  (fn [navigation]
    [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Programming 1"]
      [c/View {:style {:margin-right 20 :align-items "center"}}
       [c/Text {:style {:color s/primary :font-size s/txt-xlg :font-weight s/txt-bold}}
        (schedule-date/get-date (.-dateString (.getParam navigation "date" "Unknow")))]
       [c/Text {:style {:color s/primary :font-size s/txt-lg :font-weight s/txt-bold}}
        (schedule-date/get-month-shortname (.-dateString (.getParam navigation "date" "Unknow")))]]]
     [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md :margin-top -16.5}} "1019983"]]))

(defn FormAbsent []
  (fn []
    [c/View {:style {:width "100%" :background-color s/white :padding s/ml :margin-top 5}}
     [c/View {:style {:padding-bottom s/ml}}
      [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md}} "Upload absent letter"]]
     [c/View {:style {:width "100%" :align-items "center"}}
      [c/Icon {:name "md-copy" :color s/black :size 70}]
      [c/Icon {:name "md-add" :color s/black :size 35 :style {:margin-top -25 :margin-right -60}}]
      [c/TouchableOpacity
       [c/Text {:style {:font-size s/txt-sm :color s/black :font-weight s/txt-bold}} "Upload file"]]]
     [c/View {:style {:width "100%" :align-items "flex-end" :padding-top s/ml}}
      [c/TouchableOpacity {:style (merge s/btn s/btn-success {:padding-right 10 :padding-left 10})}
       [c/Text {:style {:font-size s/txt-sm :color s/success}} "Submit"]]]]))

(defn AbsentLetterScreen []
  (fn [{:keys [navigation]} props]
    [c/View {:style {:flex 1}}
     [Header navigation]
     [FormAbsent]]))
