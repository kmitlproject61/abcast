(ns abcast.screens.attender.main.tabview-main
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :refer [createMaterialTopTabNavigator]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.screens.attender.main.timeline-schedule :refer [TimelineSchedule]]
            [abcast.states.attender-global-state :as state]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.components.no-data :refer [NoDataScreen]]
            [abcast.utils.attender-class-service :refer [selected-class]]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; ---------- Atom ----------

(def is-tabview-timelineschedule (atom false))

(defn create-tab-navigator [tab-infomation]
  (createMaterialTopTabNavigator
   (or (clj->js tab-infomation) #js {:September #js {:screen (r/reactify-component #(fn [] [c/Text "Loading"]))
                                                     :navigationOptions #js {:tabBarLabel "Loading"}}})
   #js {:tabBarOptions #js {:style #js {:height constance/TABBAR-HEIGHT
                                        :elevation 0
                                        :backgroundColor s/white}
                            :labelStyle #js {:color s/black}
                            :indicatorStyle #js {:backgroundColor s/primary}
                            :upperCaseLabel true
                            :swipeEnable false
                            :lazy true
                            :scrollEnabled (if (<= (- (apply max @state/range-month) (apply min @state/range-month)) 2) false true)}}))

(def MainTab (atom (create-tab-navigator nil)))
(def TabViewMain (atom #(fn [] [LoadingScreen])))
(reset! is-tabview-timelineschedule false) ; View as an atom

;; --------------------------
;; -------- Function --------

(defn create-range-month []
  ; Create tab navigator infomation
  (let [tab-infomation (atom {})]
    (doseq [month-i (range (apply min @state/range-month) (inc (apply max @state/range-month)))]
      (let [month (-> (c/moment) (.month (dec month-i)) (.format "MMMM"))]
        (swap! tab-infomation assoc (keyword month) #js {:screen (r/reactify-component TimelineSchedule)
                                                         :navigationOptions #js {:tabBarLabel month}})))
    (reset! MainTab (create-tab-navigator @tab-infomation))
    (reset! TabViewMain (r/adapt-react-class @MainTab))
    (reset! is-tabview-timelineschedule true)))

(defn get-attendance-timeline []
  (-> @w/contract-classfactory .-methods
      (.getAttendance (get @selected-class :mapcode) @state/address)
      (.call #js {:from @state/address})
      (.then (fn [timestamps]
               (doseq [timestamp timestamps]
                 (swap! state/range-month conj (js/parseInt (-> c/moment (.unix timestamp) (.format "MM")))))
               (swap! state/attendances [] (js->clj timestamps))))
      (.catch (fn [e] (prn "catch") (prn e)))
      (.finally (fn []
                  (if (empty? @state/range-month)
                    (do
                      (reset! is-tabview-timelineschedule false)
                      (reset! TabViewMain #(fn [] [NoDataScreen "md-today" "You have no data yet" "Get checking attendance for next time"])))
                    (create-range-month))))))

(defn pre-get-attendance-timeline []
  (swap! state/range-month [] [])
  (get-attendance-timeline))