(ns abcast.screens.attender.main.setting.setting
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.states.attender-global-state :as state]
            [abcast.utils.web3 :as w]
            [abcast.components.modal :as modal]
            [abcast.utils.authenticate :refer [signout-google google-config]]))

;; --------------------------
;; ---------- Atom ----------

(def form-error (atom {:period "" :numpresent "" :bt-addr ""}))
(def updating-bt (atom false))
(def show-logout-modal (atom false))
(def updated (atom false))
(def show-backup-modal (atom false))
(def backup-is-copied (atom "Copy"))
(def prikey (atom ""))

;; --------------------------
;; -------- Function --------

(comment
  (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION))
  (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION))
  (-> c/PermissionsAndroid (.check c/PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION)
      (.then (fn [r] (prn r) (prn c/PermissionsAndroid.RESULTS.GRANTED)))))

(defn update-bt-addr []
  (if (= @updated false)
    (try
      (reset! updated false)
      (reset! updating-bt true)
      (.getAddress c/AbcastBluetooth
                   (fn [bt-addr]
                     (prn bt-addr)
                     (if-not (empty? bt-addr)
                       (do
                         (-> @w/contract-user .-methods
                             (.setBtAddr @state/address (w/web3.utils.stringToHex bt-addr))
                             (.send #js {:from @state/address :gas 3000000})
                             (.then (fn [r] (prn r) (reset! updated true)))
                             (.catch (fn [e] (prn e) (swap! form-error assoc :bt-addr "Update bluetooth address failed!")))
                             (.finally (fn [] (reset! updating-bt false)))))
                       (reset! updating-bt false))))
      (catch js/Error e (swap! form-error assoc :bt-addr "Get bluetooth address failed!")))))

(defn backup-prikey []
  (reset! backup-is-copied "Copy")
  (-> (.getItem c/AsyncStorage "pk")
      (.then (fn [result]
               (reset! prikey result)))
      (.catch (fn [e] (js/alert "Cannot get private key, Try again")))
      (.finally #(reset! show-backup-modal true))))

(defn logout []
  (-> (.clear c/AsyncStorage)
      (.then (fn []
               (google-config)
               (signout-google)
               (reset! show-logout-modal false)
               (js/setTimeout #(.Restart c/rn-restart) 1000)))))

;; --------------------------
;; ---------- View ----------

(defn LogoutQuestionModal []
  (fn [this navigation]
    [modal/Question
     "danger"
     "Are you sure to logout?"
     "No"
     "Yes"
     @show-logout-modal
     (fn [] (reset! show-logout-modal false))
     (fn [] (logout))
     "You have to make sure you have backed up your private key. Because if you want to login, you have to login with your private key"]))

(defn BackupQuestionModal []
  (fn [this navigation]
    [modal/Question
     "primary"
     "Copy your private key and save in safe place"
     "Cancel"
     @backup-is-copied
     @show-backup-modal
     (fn [] (reset! show-backup-modal false))
     (fn [] (.setString c/Clipboard @prikey) (reset! backup-is-copied "Coppied") (js/setTimeout #(reset! show-backup-modal false) 500))
     "Copy your private key and save in the some safe place. Private key will use again in sign in.\n\nYou can not restore your account if you lose your private key."]))

(defn Loading []
  (fn []
    [modal/Loading
     @updating-bt
     "Loading..."]))

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white :flex-direction "row" :justify-content "space-between" :align-items "center"}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Settings"]]]))

(defn SettingList []
  (fn []
    (let [input2 (atom nil)]
      [c/View
       [c/View {:style {:background-color s/white :width "100%" :padding s/ml :margin-top s/ml}}
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} "Bluetooth Device Address"]
         [c/TouchableOpacity {:style (merge {:padding-left 17
                                             :padding-right 17
                                             :border-radius 22
                                             :height 30
                                             :align-items "center"
                                             :justify-content "center"
                                             :background-color (if (= @updated true) s/success s/primary)})
                              :on-press #(update-bt-addr)}
          [c/Text {:style {:font-size s/txt-xsm :color s/white}} (if (= @updated true) "UPDATED" "UPDATE")]]]
        [c/Text {:style {:font-size s/txt-sm :color s/danger}} (get @form-error :bt-addr)]]
       [c/View {:style {:background-color s/white :width "100%" :padding s/ml :margin-top s/ml}}
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} "Backup your private key"]
         [c/TouchableOpacity {:style {:padding-left 17
                                      :padding-right 17
                                      :border-radius 22
                                      :height 30
                                      :align-items "center"
                                      :justify-content "center"
                                      :background-color s/primary}
                              :on-press (fn [] (backup-prikey))}
          [c/Text {:style {:font-size s/txt-xsm :color s/white}} "BACKUP"]]]]
       [c/View {:style {:background-color s/white :width "100%" :padding s/ml :margin-top s/ml}}
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} ""]
         [c/TouchableOpacity {:style (merge s/btn s/btn-danger s/btn-md {:padding-left 15
                                                                         :padding-right 15
                                                                         :border-radius 22
                                                                         :height 30
                                                                         :align-items "center"
                                                                         :justify-content "center"
                                                                         :background-color s/danger})
                              :on-press #(reset! show-logout-modal true)}
          [c/View {:flex-direction "row" :align-items "center"}
           [c/Icon {:name "ios-log-out" :size s/txt-sm :color s/white :style {:margin-right 5}}]
           [c/Text {:style {:font-size s/txt-xsm :color s/white}} "Log out"]]]]]])))

(defn SettingScreen []
  (fn []
    [c/View {:style {:flex 1}}
     [Loading]
     [LogoutQuestionModal]
     [BackupQuestionModal]
     [Header]
     [SettingList]]))
