(ns abcast.screens.attender.main.mainscreen
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.states.attender-global-state :as state]
            [abcast.requires.images :as image]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.screens.attender.main.tabview-main :refer [TabViewMain pre-get-attendance-timeline is-tabview-timelineschedule]]
            [abcast.utils.attender-class-service :refer [selected-class]]))

;; --------------------------
;; ---------- Atom ----------

(def refreshing (atom false))

;; --------------------------
;; -------- Function --------

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white :flex-direction "row" :justify-content "space-between" :align-items "center"}}
     [c/View
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       (get @selected-class :class-name)]
      [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md}} (get @selected-class :class-code)]]
     [c/View {:align-items "center"}
      [c/Text {:style {:font-weight s/txt-bold :color s/success :font-size s/txt-md}} (try (w/web3.utils.hexToString (get @selected-class :mapcode))
                                                                                           (catch js/Error e (prn "error hex to string")))]]]))

(defn MainScreen []
  (r/create-class
   {:component-will-mount
    (fn [] (reset! state/refresh-tabview #(pre-get-attendance-timeline)))
    :component-will-unmount
    (fn [] (reset! TabViewMain #(fn [] [LoadingScreen])))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/drawer-toggle-button navigation)
      (reset! state/main-navigation navigation)
      [c/View {:style {:flex 1}}
       [Header]
       (if (not= @is-tabview-timelineschedule true)
         [c/ScrollView {:contentContainerStyle {:flexGrow 1}
                        :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(pre-get-attendance-timeline)}])}
          [@TabViewMain]])
       (if (= @is-tabview-timelineschedule true)
         [@TabViewMain])])}))