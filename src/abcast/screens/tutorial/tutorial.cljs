(ns abcast.screens.tutorial.tutorial
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.images :as image]
            [abcast.constance :as constance]
            [abcast.requires.library :as c]))

(defn AttenderTutorial [skip-function]
  (fn []
    [c/Swiper {:showButtons true :index 0 :loop false :style {:flex 1}}
     [c/Image {:source image/tutorial-student :style {:height "100%" :width "100%"} :resize-mode "contain"}]
     [c/Image {:source image/tutorial-student2 :style {:width "100%" :height "100%"} :resize-mode "contain"}]
     [c/View
      [c/Image {:source image/tutorial-student3 :style {:width "100%" :height "100%"} :resize-mode "contain"}]
      [c/TouchableOpacity {:style {:position "absolute" :bottom 60 :right 30 :width 60 :height 30 :align-items "center" :justify-content "center"}
                           :on-press #(skip-function)}
       [c/Text {:style {:color s/black :font-size s/txt-md :opacity 0.7}} "Skip"]]]]))

(defn HostTutorial [skip-function]
  (fn []
    [c/Swiper {:showButtons true :index 0 :loop false :style {:flex 1}}
     [c/Image {:source image/tutorial-teacher :style {:height "100%" :width "100%"} :resize-mode "contain"}]
     [c/Image {:source image/tutorial-teacher2 :style {:width "100%" :height "100%"} :resize-mode "contain"}]
     [c/Image {:source image/tutorial-teacher3 :style {:width "100%" :height "100%"} :resize-mode "contain"}]
     [c/View
      [c/Image {:source image/tutorial-teacher4 :style {:width "100%" :height "100%"} :resize-mode "contain"}]
      [c/TouchableOpacity {:style {:position "absolute" :bottom 60 :right 30 :width 60 :height 30 :align-items "center" :justify-content "center"}
                           :on-press #(skip-function)}
       [c/Text {:style {:color s/black :font-size s/txt-md :opacity 0.7}} "Skip"]]]]))