(ns abcast.screens.host.joinedattenders.joinedattenders
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.constance :as constance]
            [abcast.styles :as s]
            [abcast.components.modal :as modal]
            [abcast.requires.images :as image]
            [abcast.utils.web3 :as w]
            [abcast.states.global-state :as state]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.utils.import-attendees :as excel]))

;; --------------------------
;; ---------- Atom ----------

(def refreshing (atom false))
(def loading (atom true))
(def show-import-excel-modal (atom false))

;; --------------------------
;; -------- Function --------

(defn get-user [addresses attendees-i]
  (-> @w/contract-user .-methods
      (.getUser (get addresses attendees-i))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee]
               (let [attendee (js->clj (js/Object.assign #js {} js-attendee))]
                 (swap! state/attendees assoc (count @state/attendees) attendee))
               (if (< (inc attendees-i) (count addresses))
                 (get-user addresses (inc attendees-i)))))
      (.catch (fn [e] (prn e)))))

(defn get-all-attendee []
  (reset! loading true)
  (swap! state/attendees [] [])
  (-> @w/contract-classfactory .-methods
      (.getClassAttendees (get @state/selected-class "0") (js/parseInt (get-in @state/selected-class ["7" 0 "groupId"])))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee-addresses]
               (let [attendee-addresses (js->clj js-attendee-addresses)]
                 (if-not (empty? attendee-addresses)
                   (get-user attendee-addresses 0)))))
      (.catch (fn [e] (prn e) (js/alert "Cannot get attendees")))))

(comment
  (swap! state/attendees assoc-in [0 "1"] "58010418")) ; Fix repeatly studentid for testing)

;; --------------------------
;; ---------- View ----------

(defn ImportExcelModal []
  (fn []
    [modal/Custom
     ""
     "How to import excel?"
     "CANCEL"
     "IMPORT"
     @show-import-excel-modal
     (fn [] (reset! show-import-excel-modal false))
     (fn []
       (excel/import-students :loading loading)
       (reset! show-import-excel-modal false))
     [c/ScrollView
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- Support file .xlsx only"]
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- Student ID must be at column A"]
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- First student must begin at second row (Row A2)"]
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- Do not skip even one line, Student ID must continue line by line"]
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- Store in first sheet (Any sheet name)"]
      [c/Text {:style {:color s/black :font-size s/txt-sm}} "- Import file name must be English language only"]
      [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight "bold" :margin-top 20}} "Example"]
      [c/View {:style {:margin-top 20}}
       [c/Image {:source image/example-excel :style {:width "100%" :resize-mode "contain" :margin-top -200 :margin-bottom -200}}]]]]))

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 130 constance/TABBAR-HEIGHT) :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Course people"]]]))

(defn AttenderList []
  (fn []
    [c/View
     [c/View {:style {:width "100%" :background-color s/white :margin-top 10 :padding s/ml}}
      [c/Text {:style {:color s/primary :font-size s/txt-lg}} "Lecturer"]
      [c/View {:style {:height 1 :background-color s/primary :width "100%" :margin-top 5}}]
      [c/View {:style {:flex-direction "row" :margin-top s/ml}}
       [c/Icon {:name "md-person" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]
       [c/Text {:style {:color s/black :font-size s/txt-md}} (.-name @state/account)]]]
     [c/View {:style {:width "100%" :background-color s/white :margin-top 10 :padding s/ml}}
      [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
       [c/Text {:style {:color s/primary :font-size s/txt-lg}} "Students"]
       [c/TouchableOpacity {:style (merge s/btn s/btn-success s/btn-md {:padding 10 :flex-direction "row"})
                            :on-press #(reset! show-import-excel-modal true)}
        [c/Icon {:name "md-add" :size s/txt-md :color s/success}]
        [c/Text {:style {:color s/success :font-size s/txt-xsm :font-weight s/txt-bold :margin-left 5}} "Imports"]]]
      [c/View {:style {:height 1 :background-color s/primary :width "100%" :margin-top 5}}]
      (if (= @loading false)
        (if-not (empty? @state/attendees-template)
          (doall (for [[i attendee-template] (map-indexed vector @state/attendees-template)]
                   [c/View {:style {:flex-direction "row" :justify-content "space-between" :margin-top s/ml} :key i}
                    [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
                     [c/Icon {:name "md-person" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]
                     [c/Text {:style {:color s/black :font-size s/txt-md}} "[" (get attendee-template "studentId") "] - "]
                     [c/Text {:style {:color s/black :font-size s/txt-md}}
                      (let [name (filter #(not= false %) (vec (keep-indexed #(if (= (get %2 "1") (get attendee-template "studentId")) (get %2 "2") false) @state/attendees)))]
                        (if-not (empty? name) name "Unknown"))]]
                    [c/Text {:style (merge {:font-weight "bold" :font-size s/txt-md :color s/black :opacity 0.6}
                                           (if (= (get attendee-template "joined") true) {:color s/success :opacity 1}))}
                     (if (= (get attendee-template "joined") true) "Joined" "Disjoined")]]))
          (if-not (empty? @state/attendees)
            (doall (for [[i attendee] (map-indexed vector @state/attendees)]
                     [c/View {:style {:flex-direction "row" :justify-content "space-between" :margin-top s/ml} :key i}
                      [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
                       [c/Icon {:name "md-person" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]
                       [c/Text {:style {:color s/black :font-size s/txt-md}} "[" (get attendee "1") "] - "]
                       [c/Text {:style {:color s/black :font-size s/txt-md}} (get attendee "2")]]]))
            [c/Text {:style {:color s/black :font-size s/txt-md :margin-top s/ml}} "No student"]))
        [c/Text {:style {:color s/black :font-size s/txt-md :margin-top s/ml}} "Loading"])]]))

(defn JoinedAttenders []
  (r/create-class
   {:component-did-mount
    #(excel/get-defined-list :loading loading)
    :reagent-render
    (fn []
      [c/ScrollView {:style {:flex 1}
                     :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(get-all-attendee)}])}
       [Header]
       [ImportExcelModal]
       [AttenderList]])}))
