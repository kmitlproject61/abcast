(ns abcast.screens.host.main.timeline-aday
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.states.global-state :as state]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; ---------- Atom ----------

(def period-times (atom []))
(def class-date (atom {:date nil}))
(comment "State cannot declare here bcoz they don't update")

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:y 0
                                                                         :opacity 1
                                                                         :transition #js {:ease "easeOut" :duration 500}}
                                                           :hidden #js {:y 150 :opacity 0 :transition #js {:duration 1}}})))


;; --------------------------
;; -------- Function --------

(defn get-num-present-absent []
  (reset! state/numpresent (Math/ceil (* (count @period-times) 0.5)))
  (let [presents (atom [])]
    (doseq [attender (get @state/timeline-aday :attendees-conclude)]
      (if (>= (get (frequencies (-> attender :attention)) true) @state/numpresent)
        (swap! presents assoc (count @presents) (get (frequencies (-> attender :attention)) true))))
    (swap! state/timeline-aday assoc :num-present (count @presents))))

(defn create-data-conclude []
  (doseq [attendance-data @state/attendances]
    (doseq [attendance (get attendance-data :timestamps)]
      (if (= (get @class-date :date)
             (-> c/moment (.unix attendance) (.format "YYYY-MM-DD")))
        (doseq [[time-i time] (map-indexed vector @period-times)]
          (if (and (>= (-> (-> c/moment (.unix attendance))
                           (.diff (-> (c/moment (str (get @class-date :date) " " time) "YYYY-MM-DD HH:mm")))) 0)
                   (<= (-> (-> c/moment (.unix attendance))
                           (.diff (-> (c/moment (str (get @class-date :date) " " (-> (c/moment time "HH:mm") (.add @state/period "minutes") (.format "HH:mm"))) "YYYY-MM-DD HH:mm")))) 0))
            (doseq [[attendee-i attendee] (map-indexed vector (get @state/timeline-aday :attendees-conclude))]
              (if (= (get attendance-data :address) (get attendee :student-addr))
                (swap! state/timeline-aday assoc-in [:attendees-conclude attendee-i :attention time-i] true)))
            (doseq [[attendee-i attendee] (map-indexed vector (get @state/timeline-aday :attendees-conclude))]
              (if (= (get attendance-data :address) (get attendee :student-addr))
                (if (not= (get-in @state/timeline-aday [:attendees-conclude attendee-i :attention time-i]) true)
                  (swap! state/timeline-aday assoc-in [:attendees-conclude attendee-i :attention time-i] false))))))))))

(defn create-attendees-conclude []
  (swap! state/timeline-aday assoc :attendees-conclude [])
  (doseq [[i _] (map-indexed vector @state/attendees)]
    (swap! state/timeline-aday assoc :attendees-conclude (conj (get @state/timeline-aday :attendees-conclude) {:studentid (get-in @state/attendees [i "1"])
                                                                                                               :name (get-in @state/attendees [i "2"])
                                                                                                               :img (get-in @state/attendees [i "4"])
                                                                                                               :time @period-times
                                                                                                               :bt-address (w/web3.utils.hexToString (get-in @state/attendees [i "5"]))
                                                                                                               :student-addr (get-in @state/attendees [i "0"])
                                                                                                               :attention []})))
  (create-data-conclude))

(defn switch-tab [tab-num]
  (swap! state/timeline-aday assoc :tab tab-num)
  (swap! state/timeline-aday assoc :rendered false)
  (js/setTimeout #(swap! state/timeline-aday assoc :rendered true) 200))

(comment
  (get-in @state/timeline-aday [:attendees-conclude])
  (swap! state/timeline-aday assoc :attendees-conclude
         [{:studentid "58010176"
           :name "jitiwattana robru"
           :time ["14:40" "14:41" "14:42" "14:43"]
           :img
           "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
           :bt-address "AA:AA:AA:AA:AA:AA"
           :student-addr "0x7A188132182bf3CeE160A16E26f71c20cC341643"
           :attention [true true true true]}
          {:studentid "58010176"
           :name "jitiwattana robru"
           :time ["14:40" "14:41" "14:42" "14:43"]
           :img "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
           :bt-address "AA:AA:AA:AA:AA:AA"
           :student-addr "0x4cf7B94A3320F6e4215a7463919148B8C9c3Ef65"
           :attention [nil nil true false]}
          {:studentid "58010176"
           :name "jitiwattana robru"
           :time ["14:40" "14:41" "14:42" "14:43"]
           :img "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
           :bt-address "AA:AA:AA:AA:AA:AA"
           :student-addr "0x4cf7B94A3320F6e4215a7463919148B8C9c3Ef65"
           :attention [nil nil true false]}]))

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn [navigation]
    [c/View {:style {:justify-content "space-between" :padding s/ml :width "100%" :height 180 :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       (get @selected-class :class-name)]
      [c/View {:style {:margin-right 20 :align-items "center"}}
       [c/Text {:style {:color s/primary :font-size s/txt-xlg :font-weight s/txt-bold}} (-> (c/moment (get @class-date :date) "YYYY-MM-DD") (.format "DD"))]
       [c/Text {:style {:color s/primary :font-size s/txt-lg :font-weight s/txt-bold}} (-> (c/moment (get @class-date :date) "YYYY-MM-DD") (.format "MMM"))]]]
     [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md :margin-top -16.5}} (get @selected-class :class-code)]
     [c/View {:style {:width "100%" :justify-content "center" :flex-direction "row"}}
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-top-right-radius 0
                                                                       :border-bottom-right-radius 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= (get @state/timeline-aday :tab) 0) s/primary s/white)})
                           :on-press #(switch-tab 0)}
       [c/Text {:style {:color (if (= (get @state/timeline-aday :tab) 0) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Both (" (count @state/attendees) ")")]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= (get @state/timeline-aday :tab) 1) s/primary s/white)})
                           :on-press #(switch-tab 1)}
       [c/Text {:style {:color (if (= (get @state/timeline-aday :tab) 1) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Present (" (get @state/timeline-aday :num-present) ")")]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-bottom-left-radius 0
                                                                       :border-top-left-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= (get @state/timeline-aday :tab) 2) s/primary s/white)})
                           :on-press #(switch-tab 2)}
       [c/Text {:style {:color (if (= (get @state/timeline-aday :tab) 2) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Absent (" (- (count @state/attendees) (get @state/timeline-aday :num-present)) ")")]]]]))

(defn AttenderLists [attender]
  (fn []
    [c/View {:style {:justify-content "space-between" :flex-direction "row"}}
     [c/View {:style {:flex-direction "row"}}
      [c/Image {:source {:uri (-> attender :img)}
                :style {:margin-right s/ml
                        :width 50
                        :height 50
                        :border-radius 25}}]
      [c/View {:style {:max-width (- constance/WIDTH 160) :flex-direction "column"}}
       [c/Text {:style {:font-size s/txt-md :color s/black}} (get attender :name)]
       [c/Text {:style {:font-size s/txt-xsm :color s/black :padding 2}} (get attender :studentid)]
       [c/View {:style {:flex-wrap "wrap" :flex-direction "row"}}
        (for [[i t] (map-indexed vector @period-times)]
          [c/Text {:key t :style {:font-size s/txt-xsm
                                  :color (if (= (get-in attender [:attention i]) true)
                                           s/success
                                           (if (= (get-in attender [:attention i]) nil)
                                             s/black s/danger))}}
           (str t "   ")])]]]
     [c/Text {:style {:text-align "center"
                      :width 70
                      :font-weight s/txt-bold
                      :color (if (>= (get (frequencies (-> attender :attention)) true) @state/numpresent) s/success s/danger)}}
      (if (>= (get (frequencies (-> attender :attention)) true) @state/numpresent) "Present" "Absent")]]))

(defn AttenderList []
  (fn []
    [c/ScrollView
     [ViewAnimated {:pose (if (= (get @state/timeline-aday :rendered) true) "visible" "hidden")
                    :style {:align-items "center"
                            :width "100%"
                            :height (- constance/HEIGHT constance/STATUSBAR-HEIGHT constance/TABBAR-HEIGHT 180)
                            :background-color s/white :padding-top 10}}
      (if (= (get @state/timeline-aday :tab) 1)
        (doall
         (comment [c/View {:style {:align-items "center" :width "100%" :height "100%" :background-color s/white :padding-top 10}}])
         (if-not (empty? (get @state/timeline-aday :attendees-conclude))
           (doall (for [[i attender] (map-indexed vector (get @state/timeline-aday :attendees-conclude))]
                    (if (>= (get (frequencies (-> attender :attention)) true) @state/numpresent)
                      [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
                       [AttenderLists attender]]))))))
      (if (= (get @state/timeline-aday :tab) 2)
        (doall
         (if-not (empty? (get @state/timeline-aday :attendees-conclude))
           (doall (for [[i attender] (map-indexed vector (get @state/timeline-aday :attendees-conclude))]
                    (if (< (get (frequencies (-> attender :attention)) true) @state/numpresent)
                      [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
                       [AttenderLists attender]]))))))
      (if (= (get @state/timeline-aday :tab) 0)
        (doall
         (if-not (empty? (get @state/timeline-aday :attendees-conclude))
           (doall (for [[i attender] (map-indexed vector (get @state/timeline-aday :attendees-conclude))]
                    [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
                     [AttenderLists attender]])))))]]))


(defn TimelineAday []
  (r/create-class
   {:component-will-mount
    (fn [this]
      (let [navigation (get-in this.props.argv [1 :navigation])]
        (swap! period-times [] (get (.getParam navigation "class-time") :period))
        (swap! class-date assoc :date (.getParam navigation "date")))
      (create-attendees-conclude)
      (get-num-present-absent))
    :component-did-mount
    (fn [] (js/setTimeout #(swap! state/timeline-aday assoc :rendered true) 500))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/timeline-aday-navigation navigation)
      [c/View {:style {:flex 1}}
       [Header navigation]
       [c/ScrollView
        [AttenderList]]])}))