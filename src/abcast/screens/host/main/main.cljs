(ns abcast.screens.host.main.main
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.states.global-state :as state]
            [abcast.requires.images :as image]
            [abcast.constance :as constance]
            [abcast.components.modal :as modal]
            [abcast.utils.web3 :as w]
            [abcast.screens.host.main.tabview-main :refer [TabViewMain pre-get-attendee-timeline is-tabview-timelineschedule]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.utils.class-service :refer [selected-class]]))

;; --------------------------
;; ---------- Atom ----------

(def refreshing (atom false))
(def show-excel-modal (atom false))

;; --------------------------
;; ---------- Function ----------

;; --------------------------
;; ---------- View ----------

(defn ShowExportXlsxQuestion []
  (fn []
    [modal/Question
     "primary"
     "Export as excel"
     "Cancel"
     "Save"
     @show-excel-modal
     (fn [] (reset! show-excel-modal false))
     (fn [] (@state/export-excel) (reset! show-excel-modal false))
     "Data will be saved as excel at Download folder (/storage/emulated/0/Download) as name \"ABCAS-Subject-SecN.xlsx\""]))

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white :flex-direction "row" :justify-content "space-between" :align-items "center"}}
     [c/View
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       (get @selected-class :class-name)]
      [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md}} (get @selected-class :class-code)]]
     [c/View {:align-items "center"}
      [c/Text {:style {:font-weight s/txt-bold :color s/success :font-size s/txt-md}} (try (w/web3.utils.hexToString (get @selected-class :mapcode))
                                                                                           (catch js/Error e (prn "error hex to string")))]
      (if-not @state/exporting
        [c/TouchableOpacity {:on-press #(reset! show-excel-modal true)}
         [c/Icon {:name "md-download" :size s/txt-lg :color s/black :style {:margin-top 15}}]]
        [c/Text {:style {:font-size s/txt-md :color s/black :margin-top 15}} "Exporting..."])]]))

(defn MainScreen []
  (r/create-class
   {:component-will-mount
    (fn [] (reset! state/refresh-tabview #(pre-get-attendee-timeline)))
    :component-will-unmount
    (fn [] (reset! TabViewMain #(fn [] [LoadingScreen])))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/drawer-toggle-button navigation)
      (reset! state/main-navigation navigation)
      [c/View {:style {:flex 1}}
       [Header]
       [ShowExportXlsxQuestion]
       (if (not= @is-tabview-timelineschedule true)
         [c/ScrollView {:contentContainerStyle {:flexGrow 1}
                        :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(pre-get-attendee-timeline)}])}
          [@TabViewMain]])
       (if (= @is-tabview-timelineschedule true)
         [@TabViewMain])])}))