(ns abcast.screens.host.main.tabview-main
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :refer [createMaterialTopTabNavigator]]
            [abcast.styles :as s]
            [abcast.screens.host.main.timeline-schedule :refer [TimelineSchedule]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.components.no-data :refer [NoDataScreen]]
            [abcast.constance :as constance]
            [abcast.requires.library :as c]
            [abcast.states.global-state :as state]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; ---------- Atom ----------

(def is-tabview-timelineschedule (atom false))

(defn create-tab-navigator [tab-infomation]
  (createMaterialTopTabNavigator
   (or (clj->js tab-infomation) #js {:September #js {:screen (r/reactify-component #(fn [] [c/Text "Loading"]))
                                                     :navigationOptions #js {:tabBarLabel "Loading"}}})
   #js {:tabBarOptions #js {:style #js {:height constance/TABBAR-HEIGHT
                                        :elevation 0
                                        :backgroundColor s/white}
                            :labelStyle #js {:color s/black}
                            :indicatorStyle #js {:backgroundColor s/primary}
                            :upperCaseLabel true
                            :swipeEnable false
                            :lazy true
                            :scrollEnabled (if (<= (- (apply max @state/range-month) (apply min @state/range-month)) 2) false true)}}))

(def MainTab (atom (create-tab-navigator nil)))
(def TabViewMain (atom #(fn [] [LoadingScreen])))
(reset! is-tabview-timelineschedule false) ; View as an atom

;; --------------------------
;; -------- Function --------

(defn create-range-month []
  ; Create tab navigator infomation
  (let [tab-infomation (atom {})]
    (doseq [month-i (range (apply min @state/range-month) (inc (apply max @state/range-month)))]
      (let [month (-> (c/moment) (.month (dec month-i)) (.format "MMMM"))]
        (swap! tab-infomation assoc (keyword month) #js {:screen (r/reactify-component TimelineSchedule)
                                                         :navigationOptions #js {:tabBarLabel month}})))
    (reset! MainTab (create-tab-navigator @tab-infomation))
    (reset! TabViewMain (r/adapt-react-class @MainTab))
    (reset! is-tabview-timelineschedule true)))

(defn get-attendee-timeline [attendees-i]
  (-> @w/contract-classfactory .-methods
      (.getAttendance (get @selected-class :mapcode) (get-in @state/attendees [attendees-i "0"]))
      (.call #js {:from @state/address})
      (.then (fn [timestamps]
               (doseq [timestamp timestamps]
                 (swap! state/range-month conj (js/parseInt (-> c/moment (.unix timestamp) (.format "MM")))))
               (swap! state/attendances conj {:address (get-in @state/attendees [attendees-i "0"]) :timestamps (js->clj timestamps)})
               (if (< (inc attendees-i) (count @state/attendees))
                 (get-attendee-timeline (inc attendees-i)))))
      (.catch (fn [e] (prn "catch") (prn e)))
      (.finally (fn []
                  (if (empty? @state/range-month)
                    (do
                      (reset! is-tabview-timelineschedule false)
                      (reset! TabViewMain #(fn [] [NoDataScreen "md-today" "You have no data yet" "Create class for checking students at calendar icon"])))
                    (create-range-month))))))

(defn pre-get-attendee-timeline []
  (swap! state/range-month [] [])
  (swap! state/attendances [] [])
  (if-not (empty? @state/attendees)
    (get-attendee-timeline 0)
    (do
      (reset! is-tabview-timelineschedule false)
      (reset! TabViewMain #(fn [] [NoDataScreen "md-people" "You have no student yet" "Give your generated course code to your students"])))))

(comment
  (w/web3.utils.hexToString (get-in @selected-class [:group-start-time]))
  (w/web3.utils.hexToString (get-in @selected-class [:group-end-time]))
  (reset! TabViewMain (r/adapt-react-class @MainTab))
  (reset! TabViewMain #(fn [] [LoadingScreen]))
  ; add attendance
  (-> @w/contract-classfactory .-methods
      (.addAttendance "0x5445676a78480000000000000000000000000000000000000000000000000000"
                      "0x5545c5944078ce9270B7AD1e9c170F61adf8f216"
                      (-> (c/moment "2019-02-14 10:27" "YYYY-MM-DD HH:mm") (.unix)))
      (.send #js {:from @state/address :gas 3000000})
      (.then (fn [r] (prn r)))
      (.catch (fn [e] (prn "catch") (prn e)))))