(ns abcast.screens.host.main.timeline-schedule
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.states.global-state :as state]
            [abcast.constance :as constance]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; ---------- Atom ----------

(def classes-in-month (atom {}))
(def actived-tab (atom {}))
(def critical-standard 20)
(def bad-standard 50)
(def tab-listeners (atom []))
(def time-checking (atom []))
(def all-checkings (atom 0))
(def refreshing (atom false))

;; --------------------------
;; -------- Function --------

(defn sort-by-date []
  (reset! time-checking (sort-by :date @time-checking)))

(defn create-timeline-view-data []
  (swap! time-checking [] [])
  (doseq [class-in-month @classes-in-month]
    (if (= (-> (c/moment (name (first class-in-month))) (.format "MMMM")) (get @actived-tab :month))
      (do
        (swap! time-checking assoc-in [(count @time-checking)] {:date (-> (c/moment (name (first class-in-month))) (.format "YYYY-MM-DD"))})
        (doseq [class-time (get (last class-in-month) :times)]
          (let [time-inside-time-checking (get-in @time-checking [(dec (count @time-checking)) :times])]
            (if (nil? time-inside-time-checking)
              (swap! time-checking assoc-in [(dec (count @time-checking)) :times] [{:period (get class-time :period)
                                                                                    :numpresent (Math/ceil (* (count (get class-time :period)) 0.5))
                                                                                    :present (count (get class-time :attendances))
                                                                                    :attendance-period (get class-time :attendance-period)}])
              (swap! time-checking assoc-in [(dec (count @time-checking)) :times (count time-inside-time-checking)] {:period (get class-time :period)
                                                                                                                     :numpresent (Math/ceil (* (count (get class-time :period)) 0.5))
                                                                                                                     :present (count (get class-time :attendances))
                                                                                                                     :attendance-period (get class-time :attendance-period)})))))))
  (sort-by-date))

(defn create-attendance-per-time []
  (doseq [class-day @classes-in-month]
    (doseq [[class-time-i class-time] (map-indexed vector (get (last class-day) :times))]
      (doseq [[time-i time] (map-indexed vector (get class-time :period))]
        (doseq [attendance (get class-time :attendances)]
          (if (and (>= (-> (-> c/moment (.unix attendance))
                           (.diff (c/moment (str (name (first class-day)) time) "YYYY-MM-DD HH:mm"))) 0)
                   (< (-> (-> c/moment (.unix attendance))
                          (.diff (c/moment (str (name (first class-day)) (-> (c/moment time "HH:mm") (.add @state/period "minutes") (.format "HH:mm"))) "YYYY-MM-DD HH:mm"))) 0))
            (let [attendance-period (get-in @classes-in-month [(first class-day) :times class-time-i :attendance-period])]
              (if (nil? attendance-period)
                (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendance-period] [{time [attendance]}])
                (let [inside-attendance-period (last (last attendance-period))]
                  (if (= time (first inside-attendance-period))
                    (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendance-period time-i time (count (last inside-attendance-period))] attendance)
                    (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendance-period (count attendance-period)] {time [attendance]})))))
            (let [attendance-period (get-in @classes-in-month [(first class-day) :times class-time-i :attendance-period])]
              (if (nil? attendance-period)
                (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendance-period] [{time []}])
                (if (empty? (filter #(= (first (first %)) time) attendance-period))
                  (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendance-period (count attendance-period)] {time []})))))))))
  (create-timeline-view-data))

(defn attendances-into-class-in-month []
  (doseq [class-day @classes-in-month]
    (doseq [[class-time-i class-time] (map-indexed vector (get (last class-day) :times))]
      (let [moment-start (-> (c/moment (str (first class-day) " " (first (get class-time :period)))  "YYYY-MM-DD HH:mm"))
            moment-end (-> (c/moment (str (first class-day) " " (last (get class-time :period))) "YYYY-MM-DD HH:mm") (.add @state/period "minutes"))] ; Add one more period bcoz class-period is not keep really end time
        (doseq [attendee @state/attendances]
          (doseq [attendance (get attendee :timestamps)]
            (let [moment-attendance (-> c/moment (.unix attendance))]
              (if (and (<= (-> moment-attendance (.diff moment-end)) 0)
                       (>= (-> moment-attendance (.diff moment-start)) 0))
                (let [attendance-inside-class-time (get-in @classes-in-month [(first class-day) :times class-time-i :attendances])]
                  (if (nil? attendance-inside-class-time)
                    (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendances] [attendance])
                    (swap! classes-in-month assoc-in [(first class-day) :times class-time-i :attendances (count attendance-inside-class-time)] attendance))))))))))
  (create-attendance-per-time))

(defn class-day [today week]
  (-> (c/moment today "YYYY-MM")
      (.add week "day")
      (.startOf "week")
      (.add (inc (js/parseInt (get @selected-class :group-day))) "days")
      (.format "YYYY-MM-DD")))

(defn create-class-period [class-start-time class-end-time]
  (let [class-period (atom [])]
    (swap! class-period assoc 0 class-start-time)
    (while (< (-> (c/moment (last @class-period) "HH:mm") (.add @state/period "minutes"))
              (c/moment class-end-time "HH:mm"))
      (swap! class-period assoc (count @class-period) (-> (c/moment (last @class-period) "HH:mm") (.add @state/period "minutes") (.format "HH:mm"))))
    (reset! all-checkings (* (count @class-period) (count @state/attendees)))
    @class-period))

(defn update-special-class []
  (doseq [[i special-class] (map-indexed vector (get @selected-class :group-schedule-start))]
    (let [special-class-date (-> c/moment (.unix special-class) (.format "YYYY-MM-DD"))
          special-class-timestart (-> c/moment (.unix special-class) (.format "HH:mm"))
          special-class-timeend (-> c/moment (.unix (get-in @selected-class [:group-schedule-end i])) (.format "HH:mm"))]
      (if (< (-> (c/moment (str special-class-date " " special-class-timeend))
                 (.diff (c/moment))) 0) ; Special class must no more present date time
        (let [class-period (create-class-period special-class-timestart
                                                special-class-timeend)]
          (if (nil? (find @classes-in-month (keyword special-class-date)))
            (swap! classes-in-month assoc-in [(keyword special-class-date) :times] [{:period class-period}])
            (let [in-date (get-in @classes-in-month [(keyword special-class-date) :times])]
              (swap! classes-in-month assoc-in [(keyword special-class-date) :times (count in-date)] {:period class-period})))))))
  (attendances-into-class-in-month))

(defn update-classes-in-month [today]
  (swap! classes-in-month {} {})
  (let [this-month (js/parseInt (-> (c/moment today "YYYY-MM-DD") (.format "MM")))]
    (doseq [week (range 0 35 7)]
      (if (and (= (js/parseInt (-> (c/moment (class-day today week) "YYYY-MM-DD") (.format "MM"))) this-month)
               (-> (c/moment (class-day today week) "YYYY-MM-DD") (.isValid)))
        (if (< (-> (c/moment (str (class-day today week)  " " (w/web3.utils.hexToString (get @selected-class :group-end-time))))
                   (.diff (c/moment))) 0) ; Special class must no more present date time
          (let [class-period (create-class-period (w/web3.utils.hexToString (get @selected-class :group-start-time))
                                                  (w/web3.utils.hexToString (get @selected-class :group-end-time)))]
            (swap! classes-in-month assoc (keyword (class-day today week)) {:times [{:period class-period}]}))))))
  (update-special-class))

(defn get-percent [number amount]
  (str (* (/ number amount) 100) "%"))

;; --------------------------
;; ---------- View ----------

(defn ProgressBar []
  (fn [number amount]
    [c/View {:style {:width "90%" :padding-top 12}}
     [c/View {:style {:height 3 :width "100%" :background-color s/default}}]
     [c/View {:style {:height 3 :margin-top -3 :width (get-percent number amount) :background-color (cond (< (* (/ number amount) 100) 20) s/danger
                                                                                                          (< (* (/ number amount) 100) 50) s/warning
                                                                                                          :else s/success)}}]
     [c/View {:style {:width 25
                      :height 25
                      :border-radius 18
                      :margin-top -14
                      :margin-left (get-percent number amount)
                      :justify-content "center"
                      :align-items "center"
                      :background-color (cond (< (* (/ number amount) 100) 20) s/danger
                                              (< (* (/ number amount) 100) 50) s/warning
                                              :else s/success)}}
      [c/Text {:style {:color s/white :font-size s/txt-xsm}} number]]]))

(defn TimelineSchedule []
  (r/create-class
   {:component-will-mount
    (fn [this]
      ; Create listener tab changing
      (let [navigation (get-in this.props.argv [1 :navigation])]
        (swap! tab-listeners conj (-> navigation (.addListener "willFocus" (fn [active-tab]
                                                                             (swap! actived-tab assoc :month active-tab.state.key)
                                                                             (update-classes-in-month (-> (c/moment) (.month active-tab.state.key) (.format "YYYY-MM-DD")))))))))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (if-not (= (-> navigation .-state .-key) (get @actived-tab :month))
        [LoadingScreen]
        [c/ScrollView {:style {:flex 1}
                       :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(@state/refresh-tabview)}])}
         (doall (for [[i date] (map-indexed vector @time-checking)]
                  [c/View {:style {:flex-direction "row" :width "100%" :margin-bottom 10} :key i}
                   [c/View {:style {:width 150 :padding s/ml :justify-content "center" :align-items "center" :margin-right 3 :background-color s/white}}
                    [c/Text {:style {:font-size 42 :color s/primary}} (-> (c/moment (get date :date) "YYYY-MM-DD") (.format "DD"))]
                    [c/Text {:style {:font-size s/txt-xlg :color s/primary}} (-> (c/moment (get date :date) "YYYY-MM-DD") (.format "MMM"))]]
                   [c/View
                    (doall (for [[j times] (map-indexed vector (get date :times))]
                             [c/TouchableOpacity {:style {:flex-direction "column" :width (- constance/WIDTH 150) :margin-top 3}
                                                  :on-press #(.navigate @state/main-navigation "TimelineAdayScreen" #js {:date (get date :date) :class-time times})
                                                  :key j}
                              (doall (for [[k period-time] (map-indexed vector (get times :period))]
                                       [c/View {:style {:padding 10 :padding-right s/ml :width "100%" :background-color s/white :justify-content "space-around"}
                                                :key (str j "-" k)}
                                        [c/Text {:style {:font-size s/txt-md :color s/black :font-weight s/txt-bold}} period-time]
                                        (let [num-attendance-per-period-time (last (last (get-in times [:attendance-period k])))]
                                          [ProgressBar (count num-attendance-per-period-time) (count @state/attendees)])]))]))]]))]))}))
