(ns abcast.screens.host.schedule.move-schedule
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.screens.host.schedule.schedule :refer [selected-date init-marked-date loading update-init-marked-date]]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]
            [abcast.states.global-state :as state]))

;; --------------------------
;; -------- Atom --------

(def from-time (atom [(-> (c/moment) (.format "HH:mm"))]))
(def to-time (atom [(-> (c/moment) (.format "HH:mm"))]))
(def to-date (atom (-> (c/moment) (.format "YYYY-MM-DD"))))
(def from-datetime (atom [(-> (c/moment) (.unix))])) ; final timestamp
(def to-datetime (atom [(-> (c/moment) (.unix))])) ; final timestamp
(def date-error (atom ""))

;; --------------------------
;; -------- Function --------

(defn from-time-picker [date i]
  (->
    (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
    (.then (fn [t]
             (if (not= t.action c/time-picker-android.dismissedAction)
               (do
                 (if (> (-> (c/moment (str t.hour ":" t.minute) "HH:mm") .unix)
                        (-> (c/moment (get @to-time i) "HH:mm") .unix))
                   (do
                     (swap! to-time assoc (js/parseInt i) (str t.hour ":" t.minute))
                     (swap! to-datetime assoc (js/parseInt i) (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm") (.unix)))))
                 (swap! from-time assoc (js/parseInt i) (str t.hour ":" t.minute))
                 (if-not (nil? date)
                   (swap! from-datetime assoc (js/parseInt i)
                          (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix)))
                   (swap! from-datetime assoc (js/parseInt i)
                          (-> (c/moment (str (-> (c/moment) (.format "YYYY-MM-DD")) " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix))))))))
    (.catch (fn [e] (prn e) (js/alert "Cannot select date")))))

(defn to-date-picker [date i]
  (->
    (.open c/date-picker-android #js {:date (-> (c/moment date "YYYY-MM-DD") (.toDate)) :minDate (-> (c/moment) (.toDate))})
    (.then (fn [js-picked-date]
             (let [picked-date (str (.-year js-picked-date) "-" (inc (.-month js-picked-date)) "-" (.-day js-picked-date))]
               (reset! to-date picked-date)
               (swap! from-datetime assoc i (-> (c/moment (str @to-date " " (get @from-time i)) "YYYY-MM-DD HH:mm") (.unix)))
               (swap! to-datetime assoc i (-> (c/moment (str @to-date " " (get @to-time i)) "YYYY-MM-DD HH:mm") (.unix))))))
    (.catch (fn [e] (prn e)))))

(defn to-time-picker [date i]
  (->
    (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
    (.then (fn [t]
             (if (not= t.action c/time-picker-android.dismissedAction)
               (do
                 (if (< (-> (c/moment (str t.hour ":" t.minute) "HH:mm") .unix)
                        (-> (c/moment (get @from-time i) "HH:mm") .unix))
                   (do
                     (swap! from-time assoc (js/parseInt i) (str t.hour ":" t.minute))
                     (swap! from-datetime assoc (js/parseInt i) (-> (c/moment (str @to-date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm") (.unix)))))
                 (swap! to-time assoc (js/parseInt i) (str t.hour ":" t.minute))
                 (if-not (nil? date)
                   (swap! to-datetime assoc (js/parseInt i)
                          (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix)))
                   (swap! to-datetime assoc (js/parseInt i)
                          (-> (c/moment (str (-> (c/moment) (.format "YYYY-MM-DD")) " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix))))))))
    (.catch (fn [e] (prn e) (js/alert "Cannot select date")))))

(defn set-schedule [navigation today-schedules i]
  (reset! date-error "")
  (reset! loading true)
  (-> @w/contract-classfactory .-methods
      (.setSchedule (get @selected-class :mapcode)
                    (get-in @state/selected-class ["7" 0 "groupId"])
                    (js/parseInt (get-in @selected-class [:groups 0 "6" (get-in today-schedules [0 "index"])]))
                    (js/parseInt (get-in @selected-class [:groups 0 "7" (get-in today-schedules [0 "index"])]))
                    (get @from-datetime i)
                    (get @to-datetime i))
      (.send #js {:from @state/address :gas 3000000})
      (.then (fn []
               (update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD")))
               (reset! selected-date {})
               (.goBack navigation)
               (if (< (inc i) (count today-schedules))
                 (set-schedule today-schedules (inc i)))))
      (.catch (fn [e] (prn (str "E is " e))))
      (.finally #(reset! loading false))))

(defn move-schedule [navigation]
  (let [today-schedules (js->clj (.-times (get @init-marked-date (get (first @selected-date) 0))))]
    (let [selected-date (c/moment (str @to-date " " @from-time) "YYYY-MM-DD HH:mm")]
      (let [present-date (c/moment)]
        (if (neg? (-> present-date (.diff selected-date)))
          (set-schedule navigation today-schedules 0)
          (reset! date-error "This time passed"))))))

;; --------------------------
;; -------- View --------

(defn MoveClassSchedule [navigation]
  (r/create-class
   {:component-will-mount
    (fn []
      (doseq [[i times] (map-indexed vector (.-times (get @init-marked-date (get (first @selected-date) 0))))]
        (swap! from-time assoc (js/parseInt i) (.-from times))
        (swap! to-time assoc (js/parseInt i) (.-to times))
        (let [date (.-dateString (.getParam navigation "date"))]
          (swap! from-datetime assoc (js/parseInt i) (-> (c/moment (str date " " (get @from-time i)) "YYYY-MM-DD HH:mm") (.unix)))
          (swap! to-datetime assoc (js/parseInt i) (-> (c/moment (str date " " (get @to-time i)) "YYYY-MM-DD HH:mm") (.unix)))
          (reset! to-date  date))))
    :component-will-unmount
    (fn []
      (comment
        (reset! from-time [(-> (c/moment) (.format "HH:mm"))])
        (reset! to-time [(-> (c/moment) (.format "HH:mm"))])
        (reset! from-datetime [(-> (c/moment) (.unix))])
        (reset! to-datetime [(-> (c/moment) (.unix))])
        (reset! to-date (-> (c/moment) (.format "YYYY-MM-DD")))))
    :reagent-render
    (fn []
        [c/View
         (if-not (empty? (js->clj (get @init-marked-date (get (first @selected-date) 0))))
           (doall (for [[i times] (map-indexed vector (.-times (get @init-marked-date (get (first @selected-date) 0))))]
                    [c/View {:style {:margin-top 10 :padding s/ml :width "100%" :background-color s/white} :key i}
                     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
                      [c/Text {:style {:color s/black :font-size s/txt-sm}} "To date"]
                      [c/TouchableOpacity {:style {:flex-direction "row" :align-items "center"}
                                           :on-press #(to-date-picker
                                                        (if-not (nil? (.getParam navigation "date"))
                                                          (.-dateString (.getParam navigation "date"))) i)}
                       [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}} @to-date]
                       [c/Icon {:name "md-arrow-dropright" :color s/black :size s/txt-sm}]]]
                     [c/View {:style {:flex-direction "row" :justify-content "space-between" :margin-top 30}}
                      [c/Text {:style {:color s/black :font-size s/txt-sm}} "From time"]
                      [c/TouchableOpacity {:style {:flex-direction "row" :align-items "center"}
                                           :on-press #(from-time-picker
                                                        (if-not (nil? (.getParam navigation "date"))
                                                          (.-dateString (.getParam navigation "date"))) i)}
                       [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}} (get @from-time i)]
                       [c/Icon {:name "md-arrow-dropright" :color s/black :size s/txt-sm}]]]
                     [c/View {:style {:flex-direction "row" :justify-content "space-between" :margin-top 30}}
                      [c/Text {:style {:color s/black :font-size s/txt-sm}} "To time"]
                      [c/TouchableOpacity {:style {:flex-direction "row" :align-items "center"}
                                           :on-press #(to-time-picker
                                                        (if-not (nil? (.getParam navigation "date"))
                                                          (.-dateString (.getParam navigation "date"))) i)}
                       [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}} (get @to-time i)]
                       [c/Icon {:name "md-arrow-dropright" :color s/black :size s/txt-sm}]]]
                     (if-not (empty? @date-error)
                       [c/View {:style {:flex-direction "row" :justify-content "flex-end" :margin-top 30}}
                        [c/Text {:style {:color s/danger :font-size s/txt-sm}} @date-error]])])))
         [c/View {:style {:margin-top 10 :padding 10 :padding-left s/ml :padding-right s/ml :width "100%" :background-color s/white}}
          [c/View {:style {:flex-direction "row" :align-items "center" :justify-content "flex-end"}}
           [c/View {:style {:flex-direction "row" :align-items "center"}}
            [c/TouchableOpacity {:style (merge s/btn s/btn-success s/btn-md {:background-color s/success :width 80})
                                 :on-press #(move-schedule navigation)}
             [c/Text {:style {:color s/white :font-size s/txt-sm}} "Save"]]]]]])}))
