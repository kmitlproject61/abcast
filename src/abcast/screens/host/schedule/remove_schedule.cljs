(ns abcast.screens.host.schedule.remove-schedule
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.screens.host.schedule.schedule :refer [selected-date init-marked-date update-init-marked-date loading]]
            [abcast.utils.web3 :as w]
            [abcast.states.global-state :as state]
            [abcast.utils.class-service :refer [selected-class]]))

;; --------------------------
;; ---------- Atom ----------

(def status-remove (atom []))
(def from-time (atom [(-> (c/moment) (.format "HH:mm"))]))
(def to-time (atom [(-> (c/moment) (.format "HH:mm"))]))

(defn update-status-remove [navigation]
  (let [date (.-dateString (.getParam navigation "date"))]
    (let [present-time (-> (c/moment))]
      (doseq [[i times] (map-indexed vector (.-times (get @init-marked-date (get (first @selected-date) 0))))]
        (let [selected-datetime (-> (c/moment (str date " " (.-from times)) "YYYY-MM-DD HH:mm"))]
          (comment "If past timestamp will be negative")
          (if (neg? (-> selected-datetime (.diff present-time)))
            (swap! status-remove assoc i false)
            (swap! status-remove assoc i true)))))))

;; --------------------------
;; -------- Function --------

(defn remove-schedule [navigation times-i]
  (reset! loading true)
  (let [schedule-i (.-index (get (.-times (get @init-marked-date (get (first @selected-date) 0))) times-i))]
    (-> @w/contract-classfactory .-methods
        (.deleteSchedule (get @selected-class :mapcode)
                        (get-in @state/selected-class ["7" 0 "groupId"])
                        (js/parseInt (get-in @selected-class [:groups 0 "6" schedule-i]))
                        (js/parseInt (get-in @selected-class [:groups 0 "7" schedule-i])))
        (.send #js {:from @state/address :gas 3000000})
        (.then (fn []
                 (update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD")))
                 (reset! selected-date {})
                 (.goBack navigation)))
        (.catch (fn [e] (prn e) (js/alert "Remove class schedule failed!")))
        (.finally #(reset! loading false)))))

;; --------------------------
;; ---------- View ----------

(defn RemoveClassSchedule [navigation]
  (r/create-class
    {:component-will-mount
     (fn []
       (update-status-remove navigation)

       (doseq [[i times] (map-indexed vector (.-times (get @init-marked-date (get (first @selected-date) 0))))]
         (swap! from-time assoc (js/parseInt i) (.-from times))
         (swap! to-time assoc (js/parseInt i) (.-to times))))
     :component-will-unmount
     #(reset! status-remove [])
     :reagent-render
     (fn []
       [c/View {:margin-top 10}
        (if-not (empty? (js->clj (get @init-marked-date (get (first @selected-date) 0))))
          (doall (for [[i times] (map-indexed vector (.-times (get @init-marked-date (get (first @selected-date) 0))))]
                  [c/View {:style {:margin-top 1 :padding s/ml :width "100%" :background-color s/white} :key i}
                    [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
                     [c/Text {:style {:color s/black :font-size s/txt-sm}}
                      (str (subs (str @selected-date) 12 2) " " (get @from-time i) " - " (get @to-time i))]
                     [c/View {:style {:flex-direction "row" :align-items "center"}}
                      (if (= (get @status-remove i) true)
                        [c/TouchableOpacity {:style (merge s/btn s/btn-sm s/btn-danger {:flex-direction "row"
                                                                                        :justify-content "center"
                                                                                        :align-items "center"
                                                                                        :width 90})
                                             :on-press #(remove-schedule navigation i)}
                          [c/Icon {:name "md-trash" :color s/danger :size s/txt-xsm :style {:margin-right 10}}]
                          [c/Text {:style {:color s/danger :font-size s/txt-xsm}} "Remove"]]
                        [c/Text {:style {:color s/danger :font-size s/txt-sm}} "Class is already past"])]]])))])}))
