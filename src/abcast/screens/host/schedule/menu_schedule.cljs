(ns abcast.screens.host.schedule.menu-schedule
  (:require [reagent.core :as r :refer [atom]]
            [abcast.constance :as constance]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.states.global-state :as state]
            [abcast.screens.host.schedule.schedule :refer [selected-date init-marked-date]]
            [abcast.screens.host.schedule.create-schedule :refer [CreateClassSchedule]]
            [abcast.screens.host.schedule.move-schedule :refer [MoveClassSchedule]]
            [abcast.screens.host.schedule.remove-schedule :refer [RemoveClassSchedule]]
            [abcast.utils.class-service :refer [selected-class]]))

;; --------------------------
;; -------- Atom --------

(def header-menu (atom "add"))
(def is-marked (atom false))

;; --------------------------
;; -------- View --------

(defn HeaderMenu []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :background-color s/white :flex-direction "row" :justify-content "space-between"}}
     [c/TouchableOpacity {:style {:width "25%" :align-items "center"} :on-press (if (= @is-marked true) #(reset! header-menu "remove"))}
      [c/Icon {:name "md-remove-circle-outline"
               :size 40
               :color (if (= @header-menu "remove") s/primary s/black)
               :style {:opacity (if (= @is-marked true) 1 0.4)}}]
      [c/Text {:style {:font-size s/txt-sm
                       :width 100
                       :text-align "center"
                       :color (if (= @header-menu "remove") s/primary s/black)
                       :opacity (if (= @is-marked true) 1 0.4)}} "Cancel Class"]]
     [c/TouchableOpacity {:style {:width "25%" :align-items "center"} :on-press (if (= @is-marked true) #(reset! header-menu "move"))}
      [c/Icon {:name "ios-redo"
               :size 40
               :color (if (= @header-menu "move") s/primary s/black)
               :style {:opacity (if (= @is-marked true) 1 0.4)}}]
      [c/Text {:style {:font-size s/txt-sm
                       :color (if (= @header-menu "move") s/primary s/black)
                       :opacity (if (= @is-marked true) 1 0.4)}} "Move Class"]]
     [c/TouchableOpacity {:style {:width "25%" :align-items "center"} :on-press #(reset! header-menu "add")}
      [c/Icon {:name "md-add" :size 40 :color (if (= @header-menu "add") s/primary s/black)}]
      [c/Text {:style {:font-size s/txt-sm :color (if (= @header-menu "add") s/primary s/black)}} "Add Class"]]]))


(defn MenuScheduleScreen []
  (r/create-class
   {:component-will-mount
    (fn []
      (let [selected-date (subs (str @selected-date) 12 2)]
        (if (nil? (get @init-marked-date (keyword selected-date)))
          (reset! is-marked false)
          (if (= (get (js->clj (get @init-marked-date (keyword selected-date))) "selected") true)
            (reset! is-marked true)
            (reset! is-marked false)))))
    :component-will-unmount
    (fn []
      (reset! header-menu "add"))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/menu-schedule-navigation navigation)
      [c/ScrollView {:style {:flex 1}}
       [HeaderMenu]
       [c/View
        (if (= @header-menu "add")
          [CreateClassSchedule navigation])
        (if (= @header-menu "move")
          [MoveClassSchedule navigation])
        (if (= @header-menu "remove")
          [RemoveClassSchedule navigation])]])}))
