(ns abcast.screens.host.schedule.create-schedule
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.screens.host.schedule.schedule :refer [selected-date update-init-marked-date selected-date loading]]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]
            [abcast.states.global-state :as state]))

;; --------------------------
;; ---------- Atom ----------

(def from-time (atom (-> (c/moment) (.format "HH:mm"))))
(def to-time (atom (-> (c/moment) (.format "HH:mm"))))
(def from-datetime (atom (-> (c/moment) (.unix)))) ; final timestamp
(def to-datetime (atom (-> (c/moment) (.unix)))) ; final timestamp
(def create-error (atom ""))

;; --------------------------
;; -------- Function --------

(defn from-time-picker [date]
  (->
   (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
   (.then (fn [t]
            (if (not= t.action c/time-picker-android.dismissedAction)
              (do
                (if (> (-> (c/moment (str t.hour ":" t.minute) "HH:mm") .unix)
                       (-> (c/moment @to-time "HH:mm") .unix))
                  (do
                    (reset! to-time (str t.hour ":" t.minute))
                    (reset! to-datetime (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm") (.unix)))))
                (reset! from-time (str t.hour ":" t.minute))
                (if-not (nil? date)
                  (reset! from-datetime
                          (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix)))
                  (reset! from-datetime
                          (-> (c/moment (str (-> (c/moment) (.format "YYYY-MM-DD")) " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix))))))))))

(defn to-time-picker [date]
  (->
   (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
   (.then (fn [t]
            (if (not= t.action c/time-picker-android.dismissedAction)
              (do
                (if (< (-> (c/moment (str t.hour ":" t.minute) "HH:mm") .unix)
                       (-> (c/moment @from-time "HH:mm") .unix))
                  (do
                    (reset! from-time (str t.hour ":" t.minute))
                    (reset! from-datetime (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm") (.unix)))))
                (reset! to-time (str t.hour ":" t.minute))
                (if-not (nil? date)
                  (reset! to-datetime
                          (-> (c/moment (str date " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix)))
                  (reset! to-datetime
                          (-> (c/moment (str (-> (c/moment) (.format "YYYY-MM-DD")) " " (str t.hour ":" t.minute)) "YYYY-MM-DD HH:mm")
                              (.unix))))))))))

(defn add-schedule [navigation]
  (reset! create-error "")
  (reset! loading true)
  (-> @w/contract-classfactory .-methods
      (.addSchedule (get @selected-class :mapcode) (get-in @state/selected-class ["7" 0 "groupId"]) @from-datetime @to-datetime)
      (.send #js {:from @state/address :gas 3000000})
      (.then (fn []
               (update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD")))
               (reset! selected-date {})
               (.goBack navigation)))
      (.catch (fn [e] (prn e) (js/alert "Create schedule failed!")))
      (.finally #(reset! loading false))))

(defn create-schedule [navigation]
  (let [present-time (c/moment)]
    (comment "Negative is future")
    (if (neg? (-> present-time (.diff (-> c/moment (.unix @from-datetime)))))
      (add-schedule navigation)
      (reset! create-error "This time passed"))))

;; --------------------------
;; -------- View --------

(defn CreateClassSchedule [navigation]
  (r/create-class
   {:component-will-mount
    (fn []
      (reset! from-datetime (-> (c/moment (str (.-dateString (.getParam navigation "date")) @from-time) "YYYY-MM-DD HH:mm") (.unix)))
      (reset! to-datetime (-> (c/moment (str (.-dateString (.getParam navigation "date")) @to-time) "YYYY-MM-DD HH:mm") (.unix))))
    :component-will-unmount
    (fn []
      (reset! create-error "")
      (reset! from-time (-> (c/moment) (.format "HH:mm")))
      (reset! to-time (-> (c/moment) (.format "HH:mm")))
      (reset! from-datetime (-> (c/moment) (.unix)))
      (reset! to-datetime (-> (c/moment) (.unix))))
    :reagent-render
    (fn []
      [c/View
       [c/View {:style {:margin-top 10 :padding s/ml :width "100%" :background-color s/white}}
        [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm}} "From"]
         [c/TouchableOpacity {:style {:flex-direction "row" :align-items "center"}
                              :on-press #(from-time-picker
                                          (if-not (nil? (.getParam navigation "date"))
                                            (.-dateString (.getParam navigation "date"))))}
          [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}}
           (if (nil? (.getParam navigation "date"))
             (-> (c/moment
                  (str (-> (c/moment) (.format "YYYY-MM-DD")) " " @from-time) "YYYY-MM-DD HH:mm")
                 (.format "ddd, MMM DD, YYYY HH:mm"))
             (-> (c/moment
                  (str (.-dateString (.getParam navigation "date")) " " @from-time) "YYYY-MM-DD HH:mm")
                 (.format "ddd, MMM DD, YYYY HH:mm")))]
          [c/Icon {:name "md-arrow-dropright" :color s/black :size s/txt-sm}]]]
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :margin-top 30}}
         [c/Text {:style {:color s/black :font-size s/txt-sm}} "To"]
         [c/TouchableOpacity {:style {:flex-direction "row" :align-items "center"}
                              :on-press #(to-time-picker
                                          (if-not (nil? (.getParam navigation "date"))
                                            (.-dateString (.getParam navigation "date"))))}
          [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}}
           (if (nil? (.getParam navigation "date"))
             (-> (c/moment
                  (str (-> (c/moment) (.format "YYYY-MM-DD")) " " @to-time) "YYYY-MM-DD HH:mm")
                 (.format "ddd, MMM DD, YYYY HH:mm"))
             (-> (c/moment
                  (str (.-dateString (.getParam navigation "date")) " " @to-time) "YYYY-MM-DD HH:mm")
                 (.format "ddd, MMM DD, YYYY HH:mm")))]
          [c/Icon {:name "md-arrow-dropright" :color s/black :size s/txt-sm}]]]]
       [c/View {:style {:padding 10 :padding-left s/ml :padding-right s/ml :width "100%" :background-color s/white}}
        [c/View {:style {:flex-direction "row" :align-items "center" :justify-content "space-between"}}
         [c/Text {:style {:color s/danger :font-size s/txt-sm}} @create-error]]]
       [c/View {:style {:margin-top 10 :padding 10 :padding-left s/ml :padding-right s/ml :width "100%" :background-color s/white}}
        [c/View {:style {:flex-direction "row" :align-items "center" :justify-content "flex-end"}}
         [c/View {:style {:flex-direction "row" :align-items "center"}}
          [c/TouchableOpacity {:style (merge s/btn s/btn-success s/btn-md {:background-color s/success :width 80})
                               :on-press #(create-schedule navigation)}
           [c/Text {:style {:color s/white :font-size s/txt-sm}} "Save"]]]]]])}))
