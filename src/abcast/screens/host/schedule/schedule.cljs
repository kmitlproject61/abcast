(ns abcast.screens.host.schedule.schedule
  (:require [reagent.core :as r :refer [atom]]
            [clojure.walk :refer [keywordize-keys]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.states.global-state :as state]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.components.modal :as modal]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; -------- Atom --------

(def init-marked-date (atom {}))
(def selected-date (atom {}))
(def loading (atom false))
(def clicked-date (atom {}))
(def selected-date-details (atom []))
(def refreshing (atom false))

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:y 2
                                                                         :opacity 1
                                                                         :transition #js {:ease "easeOut" :duration 500}}
                                                           :hidden #js {:y 30 :opacity 0 :transition #js {:duration 0}}})))

;; --------------------------
;; -------- Function --------


(defn class-day [today week]
  (-> (c/moment today "YYYY-MM")
      (.add week "day")
      (.startOf "week")
      (.add (inc (js/parseInt (get @selected-class :group-day))) "days")
      (.format "YYYY-MM-DD")))

(defn abs [n]
  (max n (- n)))

(defn update-special-class []
  (doseq [[i special-class] (map-indexed vector (get @selected-class :group-schedule-start))]
    (let [special-class-date (-> c/moment (.unix special-class) (.format "YYYY-MM-DD"))
          special-class-timestart (-> c/moment (.unix special-class) (.format "HH:mm"))
          special-class-timeend (-> c/moment (.unix (get-in @selected-class [:group-schedule-end i])) (.format "HH:mm"))]
      (if (nil? (find @init-marked-date (keyword special-class-date)))
        (swap! init-marked-date assoc (keyword special-class-date) (clj->js {:dots [{:color s/white :selectedDotColor s/white}]
                                                                             :selected true
                                                                             :selectedColor s/danger
                                                                             :times [{:from special-class-timestart :to special-class-timeend :index i}]
                                                                             :specialClass true}))
        (try
          (let [in-date (get @init-marked-date (keyword special-class-date))]
            (let [dot-pushed (js/Object.assign in-date (-> in-date .-dots (.push #js {:color (if (= (.-specialClass in-date) false) s/danger s/white)
                                                                                      :selectedDotColor s/white})))]
              (let [time-pushed (js/Object.assign in-date (-> dot-pushed .-times (.push #js {:from special-class-timestart :to special-class-timeend :index i})))]
                (swap! init-marked-date assoc (keyword special-class-date) time-pushed))))
          (catch js/Error e (prn e) (prn "Error push array")))))))

(defn update-init-marked-date [today]
   (-> @w/contract-classfactory .-methods
       (.getGroup (get @selected-class :mapcode) (js/parseInt (get-in @state/selected-class ["7" 0 "groupId"])))
       (.call)
       (.then (fn [newGroup]
                (doseq [[i classroom] (map-indexed vector @state/classes)]
                  (if (= (get classroom "0") (get @selected-class :mapcode))
                    (do
                      (swap! state/classes assoc-in [i "7" 0 "6"] (js->clj newGroup.exStart))
                      (swap! state/classes assoc-in [i "7" 0 "7"] (js->clj newGroup.exEnd)))))
                (swap! init-marked-date {} {})
                (let [this-month (js/parseInt (-> (c/moment today "YYYY-MM-DD") (.format "MM")))]
                  (doseq [week (range 0 35 7)]
                    (if (and (= (js/parseInt (-> (c/moment (class-day today week) "YYYY-MM-DD") (.format "MM"))) this-month)
                             (-> (c/moment (class-day today week) "YYYY-MM-DD") (.isValid)))
                      (swap! init-marked-date assoc (keyword (class-day today week)) (clj->js {:dots [{:color s/danger :selectedDotColor s/success}] :times #js [] :specialClass false}))))
                  (update-special-class))))
       (.catch (fn [e] (prn e)))))

;; --------------------------
;; ---------- View ----------

(defn Loading []
  (fn []
    [modal/Loading
     @loading
     "Loading..."]))

(defn Header []
  (fn [navigation]
    [c/View {:style {:padding s/ml :width "100%" :height (- 150 constance/TABBAR-HEIGHT) :background-color s/white :flex-direction "row" :justify-content "space-between" :align-items "center"}}
     [c/View
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Course\nTimetable"]]
     (if-not (empty? (js->clj @clicked-date))
       [c/TouchableOpacity {:style {:width 50
                                    :height 50
                                    :border-radius 25
                                    :align-items "center"
                                    :justify-content "center"
                                    :background-color s/success}
                            :on-press #(.navigate navigation "MenuScheduleScreen" #js {:date @clicked-date})}
        [c/Icon {:name "md-add" :size s/txt-lg :color s/white}]])]))

(defn SubHeader []
  (fn []
    [c/ScrollView {:style {:margin-top 5}}
     [ViewAnimated {:pose (if-not (or (empty? @selected-date-details)
                                      (empty? @selected-date)) "visible" "hidden")}
      (if (and (= (get @selected-date-details 0) "INITIAL")
               (if-not (empty? @selected-date)
                 (= (.-specialClass (get (find @init-marked-date (get (first @selected-date) 0)) 1)) false)))
        [ItemAnimated {:style {:background-color s/white :flex-direction "row" :align-items "center" :padding s/ml :margin-top 2}}
         [c/View {:style {:width 15 :height 15 :border-radius 10 :background-color s/danger :margin-right 15}}]
         [c/Text {:style {:color s/black :font-size s/txt-sm :opacity 0.7}}
          (str "Class is between " (w/web3.utils.hexToString (get @selected-class :group-start-time)) " - " (w/web3.utils.hexToString (get @selected-class :group-end-time)))]])
      (doall (for [[i times] (map-indexed vector @selected-date-details)]
               (if (not= i 0)
                 [ItemAnimated {:style {:background-color s/white :flex-direction "row" :align-items "center" :padding s/ml :margin-top 2} :key i}
                  [c/View {:style {:width 15 :height 15 :border-radius 10 :background-color s/danger :margin-right 15}}]
                  [c/Text {:style {:color s/black :font-size s/txt-sm :opacity 0.7}}
                   (str "Class is between " (get times "from") " - " (get times "to"))]])))]]))

(defn on-date-click [date]
  (swap! selected-date-details [] [])
  (js/setTimeout
   #(do
      (swap! selected-date {} (keywordize-keys {(.-dateString date) (clj->js {:selected true :selectedColor s/primary})}))
      (if-not (nil? (find @init-marked-date (get (first @selected-date) 0)))
        (if-not (= (.-special-class (get (find @init-marked-date (get (first @selected-date) 0)) 1)) false)
          (swap! selected-date-details [] (vec (concat ["INITIAL"] (js->clj (.-times (get (find @init-marked-date (get (first @selected-date) 0)) 1))))))
          (swap! selected-date-details [] ["INITIAL"]))
        (swap! selected-date-details [] [])))
   90))

(defn Calendar []
  (fn [navigation]
    [c/View {:style {:margin-top 5}}
     [c/Calendar
      {:horizontal true
       :pagingEnabled true
       :showScrollIndicator true
       :hideArrows false
       :onDayPress (fn [date] (swap! clicked-date {} date) (on-date-click date))
       :theme {:todayTextColor s/primary
               :textSectionTitleColor s/primary
               :textMonthFontSize s/txt-lg
               :textMonthFontWeight "bold"}
       :markedDates (clj->js (merge @init-marked-date @selected-date))
       :markingType "multi-dot"
       :onPressArrowLeft (fn [prvMonth] (prvMonth))
       :onPressArrowRight (fn [nxtMonth] (nxtMonth))
       :onMonthChange (fn [today] (update-init-marked-date (.-dateString today)))}]]))

(defn ScheduleScreen []
  (r/create-class
   {:component-will-mount
    (fn []
      (update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD"))))
    :reagent-render
    (fn [{:keys [navigation]} props]
      [c/ScrollView {:style {:flex 1}
                     :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD")))}])}
       [Loading]
       [Header navigation]
       [Calendar navigation]
       [SubHeader]])}))