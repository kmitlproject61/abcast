(ns abcast.screens.host.attendercheckingconclude.attenders-checking-conclude
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.components.modal :as modal]
            [abcast.states.global-state :as state]
            [abcast.screens.host.attenderchecking.attender-checking :refer [checking-attendees all-connection]]))

;; --------------------------
;; ---------- Atom ----------

(def sortby-tmp (atom nil))
(def sortby (atom nil))
(def num-present (atom 0))
(def tab (atom 0))
(def rendered (atom false))
(def numpresent (atom 3))

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:y 0
                                                                         :opacity 1
                                                                         :transition #js {:ease "easeOut" :duration 500}}
                                                           :hidden #js {:y 150 :opacity 0 :transition #js {:duration 1}}})))

;; --------------------------
;; -------- Function --------

(defn get-num-present-absent []
  (reset! numpresent (Math/ceil (* (count (get-in @checking-attendees [0 :time])) 0.5)))
  (let [presents (atom [])]
    (doseq [attender @checking-attendees]
      (if (>= (get (frequencies (-> attender :attention)) true) @numpresent)
        (swap! presents assoc (count @presents) (get (frequencies (-> attender :attention)) true))))
    (reset! num-present (count @presents))))

(comment
  (reset! checking-attendees
          [{:studentid "58010176"
            :name "jitiwattana robru"
            :time ["14:40" "14:41" "14:42" "14:43"]
            :img
            "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
            :bt-address "AA:AA:AA:AA:AA:AA"
            :student-addr "0x7A188132182bf3CeE160A16E26f71c20cC341643"
            :attention [true true true true]}
           {:studentid "58010176"
            :name "jitiwattana robru"
            :time ["14:40" "14:41" "14:42" "14:43"]
            :img "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
            :bt-address "AA:AA:AA:AA:AA:AA"
            :student-addr "0x4cf7B94A3320F6e4215a7463919148B8C9c3Ef65"
            :attention [nil nil true false]}
           {:studentid "58010176"
            :name "jitiwattana robru"
            :time ["14:40" "14:41" "14:42" "14:43"]
            :img "https://lh4.googleusercontent.com/-Z682VCHpI4o/AAAAAAAAAAI/AAAAAAAAACU/jHOPSBkCBBw/s96-c/photo.jpg"
            :bt-address "AA:AA:AA:AA:AA:AA"
            :student-addr "0x4cf7B94A3320F6e4215a7463919148B8C9c3Ef65"
            :attention [nil nil true false]}]))

(defn switch-tab [tab-num]
  (reset! tab tab-num)
  (reset! rendered false)
  (js/setTimeout #(reset! rendered true) 200))

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn []
    [c/View {:style {:justify-content "space-between" :padding s/ml :width "100%" :height 180 :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Attendance\nConclusion"]
      [c/View {:style {:margin-right 20 :align-items "center"}}
       [c/Text {:style {:color s/primary :font-size s/txt-xlg :font-weight s/txt-bold}} (-> (c/moment) (.format "DD"))]
       [c/Text {:style {:color s/primary :font-size s/txt-lg :font-weight s/txt-bold}} (-> (c/moment) (.format "MMM"))]]]
     [c/View {:style {:width "100%" :justify-content "center" :flex-direction "row"}}
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-top-right-radius 0
                                                                       :border-bottom-right-radius 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= @tab 0) s/primary s/white)})
                           :on-press #(switch-tab 0)}
       [c/Text {:style {:color (if (= @tab 0) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Both (" (count @state/attendees) ")")]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= @tab 1) s/primary s/white)})
                           :on-press #(switch-tab 1)}
       [c/Text {:style {:color (if (= @tab 1) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Present (" @num-present ")")]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-bottom-left-radius 0
                                                                       :border-top-left-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"
                                                                       :background-color (if (= @tab 2) s/primary s/white)})
                           :on-press #(switch-tab 2)}
       [c/Text {:style {:color (if (= @tab 2) s/white s/primary) :font-size s/txt-xsm :font-weight s/txt-bold}} (str "Absent (" (- (count @state/attendees) @num-present) ")")]]]]))

(defn AttenderLists [attender]
  [c/View {:style {:justify-content "space-between" :flex-direction "row"}}
   [c/View {:style {:flex-direction "row"}}
    [c/Image {:source {:uri (-> attender :img)}
              :style {:margin-right s/ml
                      :width 50
                      :height 50
                      :border-radius 25}}]
    [c/View {:style {:max-width (- constance/WIDTH 160) :flex-direction "column"}}
     [c/Text {:style {:font-size s/txt-md :color s/black}} (-> attender :name)]
     [c/Text {:style {:font-size s/txt-xsm :color s/black :padding 2}} (-> attender :studentid)]
     [c/View {:style {:flex-wrap "wrap" :flex-direction "row"}}
      (for [[i t] (map-indexed vector (-> attender :time))]
        [c/Text {:key t :style {:font-size s/txt-xsm
                                :color (if (= (get (-> attender :attention) i) true)
                                         s/success
                                         (if (= (get (-> attender :attention) i) nil)
                                           s/black s/danger))}}
         (str t "   ")])]]]
   [c/Text {:style {:text-align "center"
                    :width 70
                    :font-weight s/txt-bold
                    :color (if (>= (get (frequencies (-> attender :attention)) true) @numpresent) s/success s/danger)}}
    (if (>= (get (frequencies (-> attender :attention)) true) @numpresent) "Present" "Absent")]])

(defn AttenderList []
  (fn []
    [c/ScrollView
     [ViewAnimated {:pose (if (= @rendered true) "visible" "hidden")
                    :style {:align-items "center"
                            :width "100%"
                            :height (- constance/HEIGHT constance/STATUSBAR-HEIGHT constance/TABBAR-HEIGHT 180)
                            :background-color s/white :padding-top 10}}
      (if (= @tab 1)
        (doall
         (for [[i attender] (map-indexed vector @checking-attendees)]
           (if (>= (get (frequencies (-> attender :attention)) true) @numpresent)
             [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
              [AttenderLists attender]]))))
      (if (= @tab 2)
        (doall
         (for [[i attender] (map-indexed vector @checking-attendees)]
           (if (< (get (frequencies (-> attender :attention)) true) @numpresent)
             [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
              [AttenderLists attender]]))))
      (if (= @tab 0)
        (doall
         (for [[i attender] (map-indexed vector @checking-attendees)]
           [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
            [AttenderLists attender]])))]]))

(def sortby-menu-studentid [{:name "ANY" :value "studentid-any"}
                            {:name "A-Z" :value "studentid-az"}
                            {:name "Z-A" :value "studentid-za"}])
(def sortby-menu-name [{:name "ANY" :value "name-any"}
                       {:name "A-Z" :value "name-az"}
                       {:name "Z-A" :value "name-za"}])

(defn FilterAttender []
  (fn []
    [modal/Custom
     ""
     "FILTER ATTENDER"
     "Cancel"
     "Enter"
     @state/filter-modal-conclude
     (fn [] (reset! state/filter-modal-conclude false)
       (reset! sortby-tmp @sortby))
     (fn [] (reset! state/filter-modal-conclude false)
       (if (or (= @sortby-tmp "studentid-any") (= @sortby-tmp "name-any"))
         (doall
          (reset! sortby nil)
          (reset! sortby-tmp nil))
         (reset! sortby @sortby-tmp)))
     [c/View
      [c/View {:style {:width "100%" :align-items "center" :padding 20}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight s/txt-bold}} "SORT BY"]]
      [c/View
                    ; STUDENT ID
       [c/View {:style {:flex-direction "row" :justify-content "center" :margin-bottom 20}}
        [c/View {:style {:width 70 :margin-right 20 :justify-content "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight s/txt-bold}} "Student ID"]]
        [c/View {:style {:width 200 :flex-direction "row" :justify-content "space-between"}}
         (doall
          (for [[i menu] (map-indexed vector sortby-menu-studentid)]
            [c/TouchableOpacity {:style (merge {:flex-direction "row"
                                                :align-items "center"
                                                :width 58
                                                :justify-content "space-around"
                                                :background-color s/default
                                                :padding 5
                                                :border-radius 18
                                                :border-width 2
                                                :border-color (if (= @sortby-tmp (-> menu :value)) s/success "transparent")})
                                 :on-press #(reset! sortby-tmp (-> menu :value))
                                 :key i}
             (if (= @sortby-tmp (-> menu :value))
               [c/Icon {:name "md-checkmark" :color s/success :size s/txt-sm}])
             [c/Text {:style {:color (if (= @sortby-tmp (-> menu :value)) s/success s/black) :font-size s/txt-xsm}} (-> menu :name)]]))]]
                    ; STUDENT NAME
       [c/View {:style {:flex-direction "row" :justify-content "center"}}
        [c/View {:style {:width 70 :margin-right 20 :justify-content "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :font-weight s/txt-bold}} "Name"]]
        [c/View {:style {:width 200 :flex-direction "row" :justify-content "space-between"}}
         (doall
          (for [[i menu] (map-indexed vector sortby-menu-name)]
            [c/TouchableOpacity {:style (merge {:flex-direction "row"
                                                :align-items "center"
                                                :width 58
                                                :justify-content "space-around"
                                                :background-color s/default
                                                :padding 5
                                                :border-radius 18
                                                :border-width 2
                                                :border-color (if (= @sortby-tmp (-> menu :value)) s/success "transparent")})
                                 :on-press #(reset! sortby-tmp (-> menu :value))
                                 :key i}
             (if (= @sortby-tmp (-> menu :value))
               [c/Icon {:name "md-checkmark" :color s/success :size s/txt-sm}])
             [c/Text {:style {:color (if (= @sortby-tmp (-> menu :value)) s/success s/black) :font-size s/txt-xsm}} (-> menu :name)]]))]]]]]))

(defn AttenderCheckingConcludeScreen []
  (r/create-class
   {:component-will-mount
    #(get-num-present-absent)
    :component-did-mount
    (fn [] (js/setTimeout #(reset! rendered true) 500)) 
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/checking-conclude-navigation navigation)
      [c/View {:style {:flex 1 :background-color s/white}}
       [Header]
       [AttenderList]
       [FilterAttender]])}))