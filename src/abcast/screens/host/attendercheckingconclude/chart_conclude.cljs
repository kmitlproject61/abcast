(ns abcast.screens.host.attendercheckingconclude.chart-conclude
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.states.global-state :as state]
            [abcast.screens.host.attenderchecking.attender-checking :refer [checking-attendees all-connection]]
            [abcast.screens.host.attendercheckingconclude.attenders-checking-conclude :refer [num-present]]))

;; --------------------------
;; ---------- Atom ----------

(def chart-type (atom "Bar"))
(def chart-x-data (atom []))

;; --------------------------
;; -------- Function --------

(defn set-num-attender-per-time []
  (reset! chart-x-data [])
  (let [checked-per-time (atom [])]
    (doseq [[i attedance] (map-indexed vector (get (first @checking-attendees) :attention))]
      (swap! checked-per-time assoc (count @checked-per-time) [])
      (doseq [[j attender] (map-indexed vector @checking-attendees)]
        (swap! checked-per-time assoc-in [i j] (get-in @checking-attendees [j :attention i])))
      (swap! chart-x-data assoc (count @chart-x-data) (count (vec (filter #(= % true) (get @checked-per-time i))))))))

;; --------------------------
;; ---------- View ----------


(def chart-data (atom {:data {:labels (if-not (empty? (-> (first @checking-attendees) :time))
                                        (-> (first @checking-attendees) :time)
                                        [])
                              :datasets [{:data @chart-x-data}]}
                       :width constance/WIDTH
                       :height 220
                       :chartConfig {:backgroundGradientFrom s/white
                                     :backgroundGradientTo s/white
                                     :color (fn [] s/primary)
                                     :decimalPlaces 0}}))

(defn update-char-data []
  (set-num-attender-per-time)
  (reset! chart-type "Line")
  (reset! chart-type "Bar")
  (reset! chart-data {:data {:labels (if-not (empty? (-> (first @checking-attendees) :time))
                                       (-> (first @checking-attendees) :time)
                                       [])
                             :datasets [{:data @chart-x-data}]}
                      :width constance/WIDTH
                      :height 220
                      :chartConfig {:backgroundGradientFrom s/white
                                    :backgroundGradientTo s/white
                                    :color (fn [] s/primary)
                                    :decimalPlaces 0}}))

(add-watch checking-attendees :watcher
           (fn [] (update-char-data)))

(comment
  ; Refresh by change tab
  (swap! chart-x-data assoc 0 0)
  (reset! chart-data {:data {:labels (if-not (empty? (-> (first @checking-attendees) :time))
                                       (-> (first @checking-attendees) :time)
                                       [])
                             :datasets [{:data @chart-x-data}]}
                      :width constance/WIDTH
                      :height 220
                      :chartConfig {:backgroundGradientFrom s/white
                                    :backgroundGradientTo s/white
                                    :color (fn [] s/primary)
                                    :decimalPlaces 0}}))

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn [navigation]
    [c/View {:style {:justify-content "space-between" :padding s/ml :width "100%" :height 180 :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       (get @state/selected-class "3")]
      [c/View {:style {:margin-right 20 :align-items "center"}}
       [c/Text {:style {:color s/primary :font-size s/txt-xlg :font-weight s/txt-bold}} (-> (c/moment) (.format "DD"))]
       [c/Text {:style {:color s/primary :font-size s/txt-lg :font-weight s/txt-bold}} (-> (c/moment) (.format "MMM"))]]]
     [c/Text {:style {:font-weight s/txt-bold :color s/black :font-size s/txt-md :margin-top -16.5}} (get @state/selected-class "2")]
     [c/View {:style {:width "100%" :justify-content "center" :flex-direction "row"}}
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-top-right-radius 0
                                                                       :border-bottom-right-radius 0
                                                                       :justify-content "center"}
                                         (if (= @chart-type "SmoothLine")
                                           {:background-color s/primary}))
                           :on-press #(reset!  chart-type "SmoothLine")}
       [c/Text {:style {:color (if (= @chart-type "SmoothLine")  s/white s/primary)
                        :font-size s/txt-xsm
                        :font-weight s/txt-bold}} "Line Smooth"]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"}
                                         (if (= @chart-type "Bar")
                                           {:background-color s/primary}))
                           :on-press #(reset! chart-type "Bar")}
       [c/Text {:style {:color (if (= @chart-type "Bar")  s/white s/primary)
                        :font-size s/txt-xsm
                        :font-weight s/txt-bold}} "Bar"]]
      [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-sm {:width 100
                                                                       :margin-top 40
                                                                       :border-bottom-left-radius 0
                                                                       :border-top-left-radius 0
                                                                       :border-left-width 0
                                                                       :justify-content "center"}
                                         (if (= @chart-type "Line")
                                           {:background-color s/primary}))
                           :on-press #(reset! chart-type "Line")}
       [c/Text {:style {:color (if (= @chart-type "Line")  s/white s/primary)
                        :font-size s/txt-xsm
                        :font-weight s/txt-bold}} "Line"]]]]))

(defn Conclusion []
  (fn []
    [c/View {:style {:width "100%" :background-color s/white :padding s/ml :margin-top 1}}
     [c/View {:style {:flex-direction "row"}}
      [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold :width 50}} "Total:"]
      [c/Text {:style {:color s/success :font-size s/txt-md :font-weight s/txt-bold}} @num-present " Present"]
      [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold :margin-right 10}} ","]
      [c/Text {:style {:color s/danger :font-size s/txt-md :font-weight s/txt-bold :margin-right 10}} (- (count @state/attendees) @num-present) " Absent"]
      [c/Text {:style {:color s/black :font-size s/txt-md :margin-right 10}} "in"]
      [c/Text {:style {:color s/primary :font-size s/txt-md :font-weight s/txt-bold}} (count @state/attendees) " Attendees"]]]))


(defn ChartConcludeScreen []
  (r/create-class
   {:commponent-will-mount
    #(update-char-data)
    :commponent-did-mount
    #(prn @checking-attendees)
    :reagent-render
    (fn []
      [c/View {:style {:flex 1}}
       [Header]
       (if (= @chart-type "Line")
         [c/LineChart @chart-data])
       (if (= @chart-type "Bar")
         [c/BarChart @chart-data])
       (if (= @chart-type "SmoothLine")
         [c/LineChart (merge @chart-data {:bezier true})])
       [Conclusion]])}))
