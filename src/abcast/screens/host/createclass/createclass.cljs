(ns abcast.screens.host.createclass.createclass
  (:require [reagent.core :refer [atom] :as r]
            [abcast.styles :as s]
            [abcast.states.global-state :as state]
            [abcast.requires.library :as c]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]
            [abcast.components.modal :as modal]))

;; --------------------------
;; -------- Atom --------

(def from-time-template #js {:minute (-> (c/moment) (.format "mm")) :hour (-> (c/moment) (.format "HH"))})
(def from-time (atom [from-time-template]))
(def to-time-template #js {:minute (-> (c/moment) (.format "mm")) :hour (-> (c/moment) (.format "HH"))})
(def to-time (atom [to-time-template]))
(def academic-years ["-------- Select Term --------"
                     (str (- (js/parseInt (-> (c/moment) (.format "YYYY"))) 1))
                     (str (-> (c/moment) (.format "YYYY")))
                     (str (+ (js/parseInt (-> (c/moment) (.format "YYYY"))) 1))])
(def days (concat ["-------- Select Day --------"] constance/DAYS))
(def terms ["-------- Select Term --------" "1" "2" "3 (Summer)"]) ; NOTE bug react native picker, can't select which on selected item so init with '--select item--'
(def class-form (atom {:name nil :code nil :academic_year (nth academic-years 2) :term 0}))
(def group-form-template {:group nil :day 0 :time {:from (nth @from-time 0) :to (nth @to-time 0) :room nil}})
(def group-forms (atom [group-form-template]))
(def creating-class (atom false))
(def form-error (atom {:class-code "" :class-name "" :groups [{:group-code "" :group-room ""}]}))

;; --------------------------
;; -------- Function --------

(defn from-time-picker [group-i]
  (->
   (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
   (.then (fn [t]
            (if (not= t.action c/time-picker-android.dismissedAction)
              (do
                (def hour (-> (c/moment (.-hour t) "HH") (.format "HH")))
                (def minute (-> (c/moment (.-minute t) "mm") (.format "mm")))
                (set! (.-minute t) minute)
                (set! (.-hour t) hour)
                (reset! from-time (assoc @from-time group-i t))
                (if (< (js/parseInt (.-hour (get @to-time group-i))) (js/parseInt hour)) (do (set! (.-hour (get @to-time group-i)) hour)
                                                                                             (set! (.-minute (get @to-time group-i)) minute))
                    (if (< (js/parseInt (.-minute (get @to-time group-i))) (js/parseInt minute)) (set! (.-minute (get @to-time group-i)) minute)))
                (reset! group-forms (assoc-in @group-forms [group-i :time :from] (get @from-time group-i)))))))))

(defn to-time-picker [group-i]
  (->
   (.open c/time-picker-android {:hour 12 :minute 0 :is24Hour false})
   (.then (fn [t]
            (if (not= t.action c/time-picker-android.dismissedAction)
              (do
                (def hour (-> (c/moment (.-hour t) "HH") (.format "HH")))
                (def minute (-> (c/moment (.-minute t) "mm") (.format "mm")))
                (set! (.-minute t) minute)
                (set! (.-hour t) hour)
                (reset! to-time (assoc @to-time group-i t))
                (if (> (js/parseInt (.-hour (get @from-time group-i))) (js/parseInt (.-hour t))) (do (set! (.-hour (get @from-time group-i)) hour)
                                                                                                     (set! (.-minute (get @from-time group-i)) minute))
                    (if (> (js/parseInt (.-minute (get @from-time group-i)) (js/parseInt (.-minute t)))) (set! (.-minute (get @from-time group-i)) minute)))
                (reset! group-forms (assoc-in @group-forms [group-i :time :to] (get @to-time group-i)))))))))

(defn time-format [group-i time]
  "INPUT: #js {:minute mm :hour HH}
  OUTPUT: 'HH:mm'"
  (str (.-hour time) ":" (.-minute time)))

(defn create-group [class-created group-i]
  (let [class-address (get-in (js->clj (.-events class-created)) ["0" "address"])]
    (-> @w/contract-classfactory .-methods
        (.createGroup (-> class-created .-events .-NewClass .-returnValues .-mapCode)
                      (get-in @group-forms [group-i :group])
                      (get-in @group-forms [group-i :room])
                      (get-in @group-forms [group-i :day])
                      (w/web3.utils.stringToHex (time-format group-i (get-in @group-forms [group-i :time :from])))
                      (w/web3.utils.stringToHex (time-format group-i (get-in @group-forms [group-i :time :to]))))
        (.send #js {:from @state/address :gas 5000000 :gasPrice (w/web3.utils.toWei "10" "Gwei")})
        (.then (fn [group-created]
                 (prn (str "Group done " group-i))
                 (if (< (inc group-i) (count @group-forms))
                   (create-group class-created (inc group-i)))))
        (.catch (fn [e] (prn e) (js/alert "Cannot create group"))))))

(defn validate-all []
  (if (nil? (re-find #"^[0-9a-zA-Z][a-zA-Z0-9()\[\]_\-\.@\/\+ ]{0,255}$" (str (get @class-form :code))))
    (swap! form-error assoc :class-code "Invalid input!")
    (swap! form-error assoc :class-code ""))
  (if (nil? (re-find #"^[0-9a-zA-Z][a-zA-Z0-9()\[\]_\-\.@\/\+ ]{0,255}$" (str (get @class-form :name))))
    (swap! form-error assoc :class-name "Invalid input!")
    (swap! form-error assoc :class-name ""))
  (doseq [[group-i group-form] (map-indexed vector (get @form-error :groups))]
    (if (nil? (re-find #"^[0-9a-zA-Z][a-zA-Z0-9()\[\]_\-\.@\/\+ ]{0,255}$" (str (get-in @group-forms [group-i :group]))))
      (swap! form-error assoc-in [:groups group-i :group-code] "Invalid input!")
      (swap! form-error assoc-in [:groups group-i :group-code] ""))
    (if (nil? (re-find #"^[0-9a-zA-Z][a-zA-Z0-9()\[\]_\-\.@\/\+ ]{0,255}$" (str (get-in @group-forms [group-i :room]))))
      (swap! form-error assoc-in [:groups group-i :group-room] "Invalid input!")
      (swap! form-error assoc-in [:groups group-i :group-room] "")))
  (let [is-validate-valid? (atom {:result true})]
    (doseq [error @form-error]
      (if (and (not (vector? (val error)))
               (not (empty? (val error))))
        (swap! is-validate-valid? assoc :result false))
      (doseq [group-e (val error)]
        (if (or (not (empty? (get group-e :group-code)))
                (not (empty? (get group-e :group-room))))
          (swap! is-validate-valid? assoc :result false))))
    (get @is-validate-valid? :result)))

(defn create-class []
  (cond (validate-all)
        (do
          (reset! creating-class true)
          (-> @w/contract-classfactory .-methods
              (.createClass (get @class-form :code)
                            (get @class-form :name)
                            (js/parseInt (get @class-form :academic_year))
                            (get @class-form :term))
              (.send #js {:from @state/address :gas 3000000 :gasPrice (w/web3.utils.toWei "10" "Gwei")})
              (.then (fn [class-created]
                       (create-group class-created 0)))
              (.catch (fn [e] (prn e) (js/alert "Cannot create course")))
              (.finally (fn []
                          (-> @state/create-class-navigation .-state .-params (.getAllClass))
                          (reset! creating-class false)
                          (.goBack @state/create-class-navigation)))))))

(defn group-append []
  (reset! group-forms (conj @group-forms group-form-template))
  (reset! from-time (conj @from-time from-time-template))
  (reset! to-time (conj @to-time to-time-template))
  (swap! form-error assoc :groups (conj (get @form-error :groups) {:group-code "" :group-room ""})))

(defn group-pop []
  (swap! group-forms pop)
  (swap! from-time pop)
  (swap! to-time pop)
  (swap! form-error assoc :groups (pop (get @form-error :groups))))

;; --------------------------
;; -------- View --------

(defn Loading []
  (fn []
    [modal/Loading
     @creating-class
     "Loading..."]))

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 130 constance/TABBAR-HEIGHT) :background-color s/white :flex-direction "row" :justify-content "space-between" :align-items "center"}}
     [c/View
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Create course"]]
     [c/TouchableOpacity {:style {:width 50
                                  :height 50
                                  :border-radius 25
                                  :align-items "center"
                                  :justify-content "center"
                                  :background-color s/success}
                          :disabled false
                          :on-press #(create-class)}
      [c/Icon {:name "ios-save" :size s/txt-lg :color s/white}]]]))

(defn ClassForm []
  (fn []
    (let [input2 (atom nil)]
      [c/View
       [c/View {:style {:background-color s/white :width "100%" :padding s/ml :margin-top 10}}
        ; Name
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Course Name"]
         [c/View
          [c/TextInput {:style {:padding 10 :width 190 :color s/black :font-size s/txt-sm}
                        :underlineColorAndroid s/primary
                        :on-change-text (fn [value] (reset! class-form (assoc @class-form :name value)))
                        :return-key-type "next"
                        :on-submit-editing #(.focus @input2)
                        :placeholder "Course name Ex. English 1"}]
          [c/Text {:style {:font-size s/txt-sm :color s/danger}} (get @form-error :class-name)]]]
        ; Code
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Course ID"]
         [c/View
          [c/TextInput {:style {:padding 10 :width 190 :color s/black :font-size s/txt-sm}
                        :underlineColorAndroid s/primary
                        :on-change-text (fn [value] (reset! class-form (assoc @class-form :code value)))
                        :placeholder "Course ID Ex. 01076311"
                        :ref (fn [input] (reset! input2 input))}]
          [c/Text {:style {:font-size s/txt-sm :color s/danger}} (get @form-error :class-code)]]]
        ; Academic-years
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Academic year"]
         [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :width 170}}
          [c/Text {:style {:color s/black :font-size s/txt-sm}} (get @class-form :academic_year)]
          [c/Icon {:name "md-arrow-dropdown" :color s/black :size s/txt-md}]]
         [c/Picker {:style {:margin-left -200 :opacity 0 :width 190 :background-color "red"}
                    :mode "dropdown"
                    :on-value-change (fn [value] (reset! class-form (assoc @class-form :academic_year value)))}
          (doall (for [[i year] (map-indexed vector academic-years)]
                   [c/PickerItem {:key (dec i) :label year :value year}]))]]
        ; Terms
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Term"]
         [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :width 170}}
          [c/Text {:style {:color s/black :font-size s/txt-sm}} (nth terms (inc (get @class-form :term)))]
          [c/Icon {:name "md-arrow-dropdown" :color s/black :size s/txt-md}]]
         [c/Picker {:style {:margin-left -200 :opacity 0 :width 190}
                    :mode "dropdown"
                    :on-value-change (fn [value] (reset! class-form (assoc @class-form :term value)))}
          (doall (for [[i term] (map-indexed vector terms)]
                   [c/PickerItem {:key (dec i) :label term :value (dec i)}]))]]]])))

(defn AddGroup []
  (fn []
    [c/TouchableOpacity {:style (merge s/btn s/btn-primary s/btn-md {:align-self "flex-end" :flex-direction "row" :width 80 :margin-top 10 :background-color s/primary :margin-right s/ml})
                         :on-press #(group-append)}
     [c/Icon {:name "md-add" :color s/white :font-size s/txt-sm}]
     [c/Text {:style {:color s/white :font-size s/txt-sm :margin-left 10}} "Group"]]))

(defn DelGroup []
  (fn []
    [c/TouchableOpacity {:style (merge s/btn s/btn-danger s/btn-md {:align-self "flex-end" :flex-direction "row" :width 80 :margin-top 10 :background-color s/danger :margin-right s/ml})
                         :on-press #(group-pop)}
     [c/Icon {:name "md-trash" :color s/white :font-size s/txt-sm}]
     [c/Text {:style {:color s/white :font-size s/txt-sm :margin-left 10}} "Group"]]))

(defn GroupForm []
  (fn []
    [c/View
     (doall (for [[i group-form] (map-indexed vector @group-forms)]
              [c/View {:style {:background-color s/white :width "100%" :padding s/ml :margin-top 10} :key i}
               ; Group
               [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
                [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Group"]
                [c/View
                 [c/TextInput {:style {:padding 10 :width 190 :color s/black :font-size s/txt-sm}
                               :underlineColorAndroid s/primary
                               :on-change-text (fn [value] (reset! group-forms (assoc-in @group-forms [i :group] value)))
                               :placeholder "Group Ex. 101"}]
                 [c/Text {:style {:font-size s/txt-sm :color s/danger}} (get-in @form-error [:groups i :group-code])]]]
               ;Date
               [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
                [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Day"]
                [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :width 170}}
                 [c/Text {:style {:color s/black :font-size s/txt-sm}} (nth days (inc (get-in @group-forms [i :day])))]
                 [c/Icon {:name "md-arrow-dropdown" :color s/black :size s/txt-md}]]
                [c/Picker {:style {:margin-left -200 :opacity 0 :width 190}
                           :mode "dropdown"
                           :on-value-change (fn [value] (reset! group-forms (assoc-in @group-forms [i :day] value)))}
                 (doall (for [[i day] (map-indexed vector days)]
                          [c/PickerItem {:key (dec i) :label day :value (dec i)}]))]]
               ;Time
               [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :margin-top 10}}
                [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Time"]
                [c/View {:style {:flex-direction "row" :align-items "center" :width 190}}
                 [c/View {:style {:flex-direction "row" :margin-right 5}}
                  [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 5}} "From"]
                  [c/TouchableOpacity {:style {:width 55 :align-items "center" :padding-bottom 4 :border-bottom-width 1 :border-color s/primary}
                                       :on-press #(from-time-picker i)}
                   [c/Text {:color s/black :font-size s/txt-sm} (time-format i (get @from-time i))]]]
                 [c/View {:style {:flex-direction "row"}}
                  [c/Text {:style {:color s/black :font-size s/txt-sm :margin-right 10}} "To"]
                  [c/TouchableOpacity {:style {:width 55 :align-items "center" :padding-bottom 4 :border-bottom-width 1 :border-color s/primary}
                                       :on-press #(to-time-picker i)}
                   [c/Text {:color s/black :font-size s/txt-sm} (time-format i (get @to-time i))]]]]]
               ; Room
               [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :margin-top 10}}
                [c/Text {:style {:color s/black :font-size s/txt-sm :width (- constance/WIDTH 250)}} "Location"]
                [c/View
                 [c/TextInput {:style {:padding 10 :width 190 :color s/black :font-size s/txt-sm}
                               :underlineColorAndroid s/primary
                               :on-change-text (fn [value] (reset! group-forms (assoc-in @group-forms [i :room] value)))
                               :placeholder "Location Ex. E12-502"}]
                 [c/Text {:style {:font-size s/txt-sm :color s/danger}} (get-in @form-error [:groups i :group-room])]]]]))]))

(defn CreateClass []
  (fn [{:keys [navigation]} props]
    (reset! state/create-class-navigation navigation)
    [c/ScrollView {:style {:flex 1}}
     [Loading]
     [Header]
     [ClassForm]
     [c/View {:style {:flex-direction "row" :justify-content "flex-end"}}
      (if (> (count @group-forms) 1)
        [DelGroup])
      [AddGroup]]
     [GroupForm]]))
