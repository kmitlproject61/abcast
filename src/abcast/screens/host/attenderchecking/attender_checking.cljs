(ns abcast.screens.host.attenderchecking.attender-checking
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.components.modal :as modal]
            [abcast.states.global-state :as state]
            [abcast.utils.web3 :as w]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.utils.bluetooth-service :refer [init-bluetooth-config bluetooth-on on-state-change switch-bluetooth]]))

;; --------------------------
;; ---------- Atom ----------

(def percentage (atom 0)) ;100
(def isScan (atom false))
(def stopModalState (atom false))
(def show-help (atom false))
(def margin-top-animate 400)
(def getting-attendees (atom false))
(def checking-attendees (atom []))
(def class-period (atom []))
(def interval (atom nil))
(def num-checked (atom 0))
(def has-stopped (atom []))
(def all-connection (atom 0))
(def class-times (atom []))
(def thismoment-period (atom []))
(def present-time (atom (c/moment)))
(def is-check-finish (atom false))
(def show-no-attendee-modal (atom false))
(def show-no-classtime-modal (atom false))
(def show-cant-change-class-modal (atom false))
(def attendee-i-scaning (atom 0))

(def StatIconAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:x 9 :opacity 1 :delay 600 :transition #js {:duration 1000 :type "spring" :stiffness 900}}
                                                               :hidden #js {:x 0 :opacity 0}})))

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:y (- margin-top-animate)
                                                                         :opacity 1
                                                                         :transition #js {:ease "easeOut" :duration 500}}
                                                           :hidden #js {:y 0 :opacity 0}})))

(defn init-animation-values [this]
  (.setValue (.. this -state -startBtnBGSize) 180)
  (.setValue (.. this -state -startBtnSize) 150)
  (.setValue (.. this -state -startBtnBorder) 10)
  (.setValue (.. this -state -startBtnFontStart) 30)
  (.setValue (.. this -state -startBtnFontTime) 28)
  (.setValue (.. this -state -startBtnOpacity) 1))

;; --------------------------
;; -------- Function --------

(defn start-zoomin-prestartscan-animation [this]
  (let [duration 500]
    (-> (.parallel c/animated
                   #js [(-> (.timing c/animated (.. this -state -startBtnSize) #js {:toValue 150 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnBGSize) #js {:toValue 180 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnBorder) #js {:toValue 10 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnFontStart) #js {:toValue 30 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnFontTime) #js {:toValue 28 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnOpacity) #js {:toValue 1 :duration duration :easing (.-ease c/Easing)}))])
        (.start))))

(defn reset-to-prescan [this]
  (reset! percentage 0) ;100
  (reset! isScan false)
  (reset! attendee-i-scaning 0)
  (reset! is-check-finish false)
  (reset! stopModalState false)
  (reset! show-help false)
  (reset! getting-attendees false)
  (reset! checking-attendees [])
  (reset! show-cant-change-class-modal false)
  (reset! class-period [])
  (reset! interval nil)
  (reset! num-checked 0)
  (reset! all-connection 0)
  (reset! thismoment-period [])
  (reset! present-time (c/moment))
  (start-zoomin-prestartscan-animation this))

(defn add-attendance [attendee-i round]
  (-> @w/contract-classfactory .-methods
      (.addAttendance (get @selected-class :mapcode) (get-in @checking-attendees [attendee-i :student-addr]) (-> (c/moment) (.unix)))
      (.send #js {:from @state/address :gas 3000000})
      (.then (fn []
               (swap! checking-attendees assoc-in [attendee-i :attention round] true)))
      (.catch (fn [e] (prn e) (js/alert "Cannot check student-id ")))))

(defn bluetooth-scan [attendee-i round & {:keys [rechecking] :or {rechecking false}}]
  "@NOTE force stop recursive by check realtime after bt connection done, if match ignore recursive else keep looping bt connect"
  (reset! attendee-i-scaning attendee-i)
  (-> (.connect c/bluetooth-classic #js {:name "" :address (get-in @checking-attendees [attendee-i :bt-address])})
      (.then (fn []
               (add-attendance attendee-i round)))
      (.catch (fn []
                (swap! checking-attendees assoc-in [attendee-i :attention round] false)))
      (.finally (fn []
                  (comment (prn (str "Checked => attendee-i: " attendee-i " round: " round)))
                  (if (not rechecking)
                    (swap! num-checked inc))
                  (reset! percentage (* (/ @num-checked @all-connection) 100))
                  (.setTimeout c/background-timer (fn []
                                                    (if (= @is-check-finish true)
                                                      (.clearInterval c/background-timer @interval)
                                                      (cond (and (not (nil? (get-in @checking-attendees [attendee-i :time (inc round)])))
                                                                 (>= (.diff @present-time (-> (c/moment (get-in @checking-attendees [attendee-i :time (inc round)]) "HH:mm"))) 0))
                                                            (do ; Interrupt checking this round because of time's up and go to next round
                                                                     ; Before go to next round set disconnected user remaining to nil
                                                              (doseq [remain-i (range (inc attendee-i) (count @checking-attendees))]
                                                                (if (not= (get-in @checking-attendees [remain-i :attention round]) nil)
                                                                  (swap! num-checked inc)
                                                                  (swap! checking-attendees assoc-in [remain-i :attention round] nil)))
                                                              (bluetooth-scan 0 (inc round)))
                                                            (and (< (inc attendee-i) (count @checking-attendees))
                                                                 (nil? (get-in @checking-attendees [(inc attendee-i) :attention round]))) ; Normal loop checking between attendees
                                                            (bluetooth-scan (inc attendee-i) round)
                                                            :else (reset! interval (.setInterval c/background-timer ; Waiting for next round time
                                                                                                 #(do (cond (>= (.diff @present-time (-> (c/moment (get-in @checking-attendees [attendee-i :time (inc round)]) "HH:mm"))) 0)
                                                                                                            (do ; Time passed this round, go next round
                                                                                                              (.clearInterval c/background-timer @interval)
                                                                                                              (bluetooth-scan 0 (inc round)))
                                                                                                            (not (-> (c/moment (get-in @checking-attendees [attendee-i :time (inc round)]) "HH:mm") (.isValid))) ; Out of round, End of checking
                                                                                                            (do
                                                                                                              (reset! is-check-finish true)
                                                                                                              (.clearInterval c/background-timer @interval))
                                                                                                            :else (do
                                                                                                                    (prn "Waiting for next round")
                                                                                                                    (reset! present-time (-> (c/moment) (.seconds 0))) ; Always update present time
                                                                                                                    (let [recheck-attendee-i (atom [])] ; Recheck while time is still remaining by random check all false status
                                                                                                                      (doseq [attendee-i (range (count @checking-attendees))]
                                                                                                                        (if (= (get-in @checking-attendees [attendee-i :attention round]) false)
                                                                                                                          (swap! recheck-attendee-i assoc (count @recheck-attendee-i) attendee-i)))
                                                                                                                      (if-not (empty? @recheck-attendee-i)
                                                                                                                        (do
                                                                                                                          (.clearInterval c/background-timer @interval)
                                                                                                                          (bluetooth-scan (first (shuffle @recheck-attendee-i)) round :rechecking true)))))))
                                                                                                 3000)))))
                               2000)))))

(comment
  (comment ; Command for test recheck
    (swap! checking-attendees assoc-in [0 :attention 2] true)
    (swap! class-times assoc (count @class-times) ["15:55" "15:59"])))

(defn interval-scan []
  (reset! all-connection (* (count @checking-attendees) (count (get (last @checking-attendees) :time))))
  ; Loop for send latest round time after pressed time
  (let [i (atom 1)
        past-time-arr (atom [])]
    ; Loop set all attendees to nil for past time
    (while (and (< @i (count @class-period))
                (>= (.diff @present-time (c/moment (get-in @checking-attendees [0 :time @i]) "HH:mm")) 0))
      (swap! past-time-arr assoc (count @past-time-arr) (dec @i))
      (swap! i inc))
    (doseq [past-time-i @past-time-arr]
      (doseq [past-time-attendee (range (count @checking-attendees))]
        (do
          (swap! num-checked inc)
          (swap! checking-attendees assoc-in [past-time-attendee :attention past-time-i] nil))))
    (if (and (not (empty? @class-times))
             (= (last @has-stopped) (last @class-times)))
      (-> @w/contract-classfactory .-methods
          (.deleteAttendance (get @selected-class :mapcode)
                             (get-in @selected-class [:groups 0 "groupId"])
                             (-> (c/moment (first (last @class-times)) "HH:mm") (.unix))
                             (-> (c/moment (last (last @class-times)) "HH:mm") (.unix)))
          (.send #js {:from @state/address :gas 3000000})
          (.then (fn [r]
                   (prn "Cleared last attendances")
                   (if (empty? @past-time-arr)
                     (.setTimeout c/background-timer #(bluetooth-scan 0 0) 2000)
                     (.setTimeout c/background-timer #(bluetooth-scan 0 (inc (last @past-time-arr))) 2000))))
          (.catch (fn [e] (prn e) (js/alert "Cannot clear last attendances")))
          (.finally #(reset! has-stopped [])))
      (if (empty? @past-time-arr)
        (.setTimeout c/background-timer #(bluetooth-scan 0 0) 2000)
        (.setTimeout c/background-timer #(bluetooth-scan 0 (inc (last @past-time-arr))) 2000)))))

(defn create-class-period []
  (let [class-start-time (get @thismoment-period 0)
        class-end-time (get @thismoment-period 1)]
    (swap! class-period assoc 0 class-start-time)
    (while (< (-> (c/moment (last @class-period) "HH:mm") (.add @state/period "minutes"))
              (c/moment class-end-time "HH:mm"))
      (swap! class-period assoc (count @class-period) (-> (c/moment (last @class-period) "HH:mm") (.add @state/period "minutes") (.format "HH:mm")))))
  (doseq [[i _] (map-indexed vector @state/attendees)]
    (swap! checking-attendees assoc i {:studentid (get-in @state/attendees [i "1"])
                                       :name (get-in @state/attendees [i "2"])
                                       :time @class-period
                                       :img (get-in @state/attendees [i "4"])
                                       :bt-address (w/web3.utils.hexToString (get-in @state/attendees [i "5"]))
                                       :student-addr (get-in @state/attendees [i "0"])
                                       :attention []})))

(defn get-user [addresses attendees-i]
  (-> @w/contract-user .-methods
      (.getUser (get addresses attendees-i))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee]
               (let [attendee (js->clj (js/Object.assign #js {} js-attendee))]
                 (swap! state/attendees assoc (count @state/attendees) attendee))
               (if (< (inc attendees-i) (count addresses))
                 (get-user addresses (inc attendees-i)))))
      (.catch (fn [e] (prn e)))))

(defn start-zoomout-prestartscan-animation [this]
  (let [duration 500]
    (-> (.parallel c/animated
                   #js [(-> (.timing c/animated (.. this -state -startBtnSize) #js {:toValue 50 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnBGSize) #js {:toValue 80 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnBorder) #js {:toValue 2 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnFontStart) #js {:toValue 10 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnFontTime) #js {:toValue 10 :duration duration :easing (.-ease c/Easing)}))
                        (-> (.timing c/animated (.. this -state -startBtnOpacity) #js {:toValue 0 :duration duration :easing (.-ease c/Easing)}))])
        (.start (fn [] (swap! isScan {} true) (interval-scan))))))

(defn get-all-attendee [this]
  (reset! state/attendees [])
  (-> @w/contract-classfactory .-methods
      (.getClassAttendees (get @state/selected-class "0") (js/parseInt (get-in @state/selected-class ["7" 0 "groupId"])))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee-addresses]
               (let [attendee-addresses (js->clj js-attendee-addresses)]
                 (if-not (empty? attendee-addresses)
                   (get-user attendee-addresses 0)))))
      (.catch (fn [e] (prn e)))
      (.finally (fn []
                  (create-class-period)
                  (swap! getting-attendees {} false)
                  (start-zoomout-prestartscan-animation this)))))

(defn set-period-this-moment []
  (doseq [class-time @class-times]
    (if (and (>= (.diff @present-time (-> (c/moment (get class-time 0) "HH:mm") (.seconds 0))) 0)
             (<= (.diff @present-time (-> (c/moment (get class-time 1) "HH:mm") (.seconds 0))) 0))
      (reset! thismoment-period class-time))))

(defn prescan []
  (swap! class-times [] [])
  (let [classday (js/parseInt (get-in @selected-class [:groups 0 "3"]))]
    (if (= (-> (c/moment) (.day (inc classday)) (.format "ddd")) (-> (c/moment) (.format "ddd")))
      (swap! class-times [] (vec (conj @class-times [(w/web3.utils.hexToString (get-in @selected-class [:groups 0 "4"]))
                                                     (w/web3.utils.hexToString (get-in @selected-class [:groups 0 "5"]))])))))
  (if-not (empty? (get-in @selected-class [:groups 0 "6"]))
    (let [today-class-start-time (atom [])
          today-class-end-time (atom [])]
      (doseq [[i class-time] (map-indexed vector (get-in @selected-class [:groups 0 "6"]))]
        (if (and (= (-> @present-time (.diff (-> c/moment (.unix class-time)) "day")) 0))
          (do
            (swap! today-class-start-time assoc (count @today-class-start-time) class-time)
            (swap! today-class-end-time assoc (count @today-class-end-time) (get-in @selected-class [:groups 0 "7" i]))
            (let [group-start-timestamp (vec (map #(-> c/moment (.unix %) (.format "HH:mm")) @today-class-start-time))
                  group-end-timestamp (vec (map #(-> c/moment (.unix %) (.format "HH:mm")) @today-class-end-time))]
              (swap! class-times [] (vec (concat @class-times (vec (apply map vector [group-start-timestamp group-end-timestamp]))))))))))))

(defn start-scan [this]
  (prescan)
  (if-not (empty? @state/attendees)
    (do
      (reset! present-time (-> (c/moment) (.seconds 0)))
      (set-period-this-moment)
      (if-not (empty? @thismoment-period)
        (do
          (if (= @bluetooth-on false)
            (switch-bluetooth))
          (reset! is-check-finish false)
          (swap! getting-attendees {} true)
          (get-all-attendee this))
        (reset! show-no-classtime-modal true)))
    (reset! show-no-attendee-modal true)))

(comment ;comment for test prescan 
  (swap! selected-class assoc-in [:groups 0 "6" (count (get-in @selected-class [:groups 0 "6"]))]
         (str (-> (c/moment) (.subtract 2 "minutes") (.unix))))
  (swap! selected-class assoc-in [:groups 0 "7" (count (get-in @selected-class [:groups 0 "7"]))]
         (str (-> (c/moment) (.add 3 "minutes") (.unix))))
  (swap! selected-class assoc-in [:groups] "A")
  (swap! selected-class assoc :groups "A")
  (reset! present-time (-> (c/moment "15:40" "HH:mm") (.seconds 0))))

;; --------------------------
;; -------- View --------

(defn Header []
  (fn []
    [c/View {:style {:justify-content "space-between" :flex-direction "row" :padding s/ml :width "100%" :height 120 :background-color s/white}}
     [c/View
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Attendance\nChecking"]]
     [c/View {:style {:margin-right 20 :align-items "center"}}
      [c/Text {:style {:color s/primary :font-size s/txt-xlg :font-weight s/txt-bold}} (-> (c/moment) (.format "DD"))]
      [c/Text {:style {:color s/primary :font-size s/txt-lg :font-weight s/txt-bold}} (-> (c/moment) (.format "MMM"))]]]))

(defn Percentage []
  (fn [navigation this]
    [c/View {:style {:background-color s/white :padding-bottom s/ml :align-items "center" :height 180}}
     [c/AnimatedCircularProgress {:size 150
                                  :width 10
                                  :fill @percentage
                                  :prefill 100
                                  :easing (.-ease c/Easing)
                                  :tintColor (if (>= @percentage 100) s/success s/primary)
                                  :background-color s/secondary
                                  :arc-sweep-angle 270
                                  :rotation 225}
      (fn [fill]
        (r/as-element
         [c/Text {:style {:font-size s/txt-xxxlg :color (if (>= @percentage 100) s/success s/primary) :font-weight s/txt-bold}} (if (>= @percentage 100) "DONE" (str (int fill) "%"))]))]
     [c/View {:style {:padding-left 40 :padding-right 40 :align-items "center" :margin-top -30}}
      [c/Text {:style {:font-size s/txt-sm :color s/black}} "Completed"]
      [c/Text {:style {:font-size s/txt-sm :font-weight s/txt-bold}} (if (> @num-checked @all-connection)
                                                                       (str @all-connection "/" @all-connection)
                                                                       (str @num-checked "/" @all-connection))]
      (if (= @is-check-finish false)
        [c/TouchableOpacity {:on-press #(reset! stopModalState true)
                             :style {:margin-top -16 :margin-right 150 :margin-left s/ml :width 25 :height 25}}
         [c/Icon {:name "ios-square" :color s/black :size s/txt-md :style {:opacity 0.7}}]])
      (if (= @is-check-finish true)
        [c/TouchableOpacity {:on-press #(reset-to-prescan this)
                             :style {:margin-top -16 :margin-right 150 :margin-left s/ml :width 25 :height 25}}
         [c/Icon {:name "md-refresh" :color s/black :size s/txt-md :style {:opacity 0.7}}]])
      [c/TouchableOpacity {:on-press #(.navigate navigation "AttenderCheckingConcludeScreen")
                           :style {:width 35 :margin-top -16 :margin-left 150}}
       [StatIconAnimated {:pose (if (= @is-check-finish true) "visible" "hidden")}
        [c/Icon {:name "ios-stats" :color s/primary :size s/txt-xlg :style {:opacity 0.7}}]]]]]))

(defn AttenderList []
  (fn []
    [c/ScrollView
     [ViewAnimated {:pose (if (= @isScan true) "visible" "hidden") :style {:margin-top margin-top-animate :margin-bottom -100}}
      (if-not @getting-attendees
        (doall (for [[i attender] (map-indexed vector @checking-attendees)]
                 [ItemAnimated {:style {:width "100%" :padding 10 :padding-left s/ml :padding-right s/ml :background-color s/white} :key i}
                  [c/View {:style {:justify-content "space-between" :flex-direction "row"}}
                   [c/View {:style {:flex-direction "row"}}
                    [c/Image {:source {:uri (-> attender :img)}
                              :style {:margin-right s/ml
                                      :width 50
                                      :height 50
                                      :border-radius 25}}]
                    [c/View {:style {:max-width (- constance/WIDTH 180) :flex-direction "column"}}
                     [c/Text {:style {:font-size s/txt-md :color s/black}} (-> attender :name)]
                     [c/Text {:style {:font-size s/txt-xsm :color s/black :padding 2}} (-> attender :studentid)]
                     [c/View {:style {:flex-wrap "wrap" :flex-direction "row"}}
                      (doall (for [[i t] (map-indexed vector (-> attender :time))]
                               [c/Text {:key t :style {:font-size s/txt-xsm
                                                       :color (if (= (get (-> attender :attention) i) true)
                                                                s/success
                                                                (if (= (get (-> attender :attention) i) nil)
                                                                  s/black s/danger))}}
                                (str t "   ")]))]]]
                   [c/Text {:style {:text-align "right"}
                            :width 70
                            :font-weight s/txt-bold
                            :color s/black}
                    (if (= @is-check-finish true) "Finish"
                        (if (= i @attendee-i-scaning) "Scanning" "Waiting"))]]])))]]))
;Play animate with dots


(defn PreStartScan []
  (fn [this]
    (let [startBtnSize (.. this -state -startBtnSize)
          startBtnBGSize (.. this -state -startBtnBGSize)
          startBtnFontStart (.. this -state -startBtnFontStart)
          startBtnFontTime (.. this -state -startBtnFontTime)
          startBtnBGSize (.. this -state -startBtnBGSize)
          startBtnBorder (.. this -state -startBtnBorder)
          startBtnBorder (.. this -state -startBtnBorder)
          startBtnOpacity (.. this -state -startBtnOpacity)]
      [c/AnimatedView {:style {:flex 1 :background-color s/white :justify-content "center" :align-items "center" :opacity startBtnOpacity}}
       [c/AnimatedView {:style {:width startBtnBGSize :height startBtnBGSize :border-radius 90 :background-color s/default :justify-content "center" :align-items "center"}}
        [c/TouchableOpacity {:on-press (fn [] (start-scan this))}
         [c/AnimatedView {:style {:width startBtnSize :height startBtnSize :border-radius 75 :border-width startBtnBorder :border-color s/primary :justify-content "center" :align-items "center"}}
          [c/AnimatedText {:style {:font-size startBtnFontStart :font-weight s/txt-bold :color s/primary}} "START"]]]]
       [c/TouchableOpacity {:style {:width 200 :align-items "flex-end"} :on-press #(reset! show-help (not @show-help))}
        [c/Icon {:name "md-help-circle-outline" :size s/txt-lg :color s/black :style {:opacity 0.6}}]]])))

(defn CantChangeClassDrawerModal []
  (fn []
    [modal/Simple
     "warning"
     "Warning!"
     "Get it"
     @show-cant-change-class-modal
     (fn [] (reset! show-cant-change-class-modal false))
     "Cannot change class while checking attendeehat"]))

(defn NoAttendeeModal []
  (fn []
    [modal/Simple
     ""
     "You have no student yet"
     "Get it"
     @show-no-attendee-modal
     (fn [] (reset! show-no-attendee-modal false))
     "Give your generated course code to your students"]))

(defn NoClasstimeModal []
  (fn []
    [modal/Simple
     ""
     "You have no class yet"
     "Get it"
     @show-no-classtime-modal
     (fn [] (reset! show-no-classtime-modal false))
     "Create class for checking students at calendar icon"]))

(defn Help []
  (fn []
    [modal/Simple
     ""
     "Help"
     "Get it"
     @show-help
     (fn [] (reset! show-help (not @show-help)))
     "- System will check students in every 30 minutes.\n\n- If checking is finish before 30 minutes, System will wait for next round and repeat checking remain absent student.\n\n- Else if checking isn't finish yet but time's up, System will check for next round and remain student will be unchecked"]))

(defn ShowStopQuestion []
  (fn [this navigation]
    [modal/Question
     "warning"
     "Warning!"
     "Cancel"
     "Stop checking"
     @stopModalState
     (fn [] (reset! stopModalState false))
     (fn []
       (reset! is-check-finish true)
       (reset! has-stopped @class-times)
       (reset! stopModalState false))
     "You will not able to check again. We advise you to stop checking when you finish your class only"]))

(defn AttenderCheckingScreen []
  (r/create-class
   {:get-initial-state
    (fn []
      (clj->js {:startBtnBGSize (new c/animated-value 180)
                :startBtnSize (new c/animated-value 150)
                :startBtnBorder (new c/animated-value 10)
                :startBtnFontStart (new c/animated-value 30)
                :startBtnFontTime (new c/animated-value 28)
                :startBtnOpacity (new c/animated-value 1)}))
    :component-will-mount
    (fn []
      (init-bluetooth-config)
      (prescan)
      (.addEventListener c/RNBluetoothInfo "change" on-state-change))
    :component-did-mount
    (fn [this]
      (init-animation-values this))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (let [this (r/current-component)]
        [c/View {:style {:flex 1 :background-color s/white}}
         [Header]
         [Help]
         [NoAttendeeModal]
         [NoClasstimeModal]
         [CantChangeClassDrawerModal]
         [ShowStopQuestion this navigation]
         (if (= @isScan false)
           [PreStartScan this]
           [Percentage navigation this])
         [c/View {:style (merge (if (= @isScan false) {:display "none"}))}
          [AttenderList]]]))}))