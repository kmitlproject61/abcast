(ns abcast.screens.selectclass.select-class
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.states.global-state :as state]
            [abcast.components.modal :as modal]
            [abcast.screens.host.main.tabview-main :refer [TabViewMain pre-get-attendee-timeline]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.components.no-data :refer [NoDataScreen]]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]))

;; --------------------------
;; ---------- Atom ----------

(def loading (atom true))
(def refreshing (atom false))
(def classes (atom []))
(def backup-is-copied (atom "Copy"))
(def prikey (atom ""))
(def app-first-times (atom false))

;; --------------------------
;; -------- Function --------

(defn get-group [classroom class-i mapCode groups-id group-i]
  (let [group-id (get groups-id group-i)]
    (-> @w/contract-classfactory .-methods
        (.getGroup mapCode group-id)
        (.call #js {:from @state/address})
        (.then (fn [js-group]
                 (let [group (js->clj (js/Object.assign #js {} js-group))]
                   (swap! classes assoc-in [class-i "7"] (conj (get-in @classes [class-i "7"]) group)))
                 (if (< (inc group-i) (count groups-id))
                   (get-group classroom class-i mapCode groups-id (inc group-i)))))
        (.catch (fn [e] (prn e) (prn "ERROR"))))))

(defn get-groups-id [classroom class-i mapCode]
  (-> @w/contract-classfactory .-methods
      (.getAllGroupId mapCode)
      (.call #js {:from @state/address})
      (.then (fn [groups-id]
               (get-group classroom class-i mapCode groups-id 0)))
      (.catch (fn [e] (prn e) (prn "ERROR")))))

(defn get-class [mapCodes class-i]
  (let [mapCode (get mapCodes class-i)]
    (-> @w/contract-classfactory .-methods
        (.getClass mapCode)
        (.call #js {:from @state/address})
        (.then (fn [js-classroom]
                 (let [classroom (js->clj (js/Object.assign #js {} js-classroom))]
                   (swap! classes conj (conj classroom {"7" []}))
                   (get-groups-id classroom class-i mapCode))
                 (if (< (inc class-i) (count mapCodes))
                   (get-class (js->clj mapCodes) (inc class-i)))))
        (.catch (fn [e] (prn e) (prn "ERROR"))))))

(defn get-own-classes []
  (-> @w/contract-classfactory .-methods
      (.getOwnClasses @state/address)
      (.call #js {:from @state/address})
      (.then (fn [mapCodes]
               (get-class (js->clj mapCodes) 0)))
      (.catch (fn [e] (prn e) (prn "ERROR")))
      (.finally (fn []
                  (js/setTimeout #(do
                                    (try
                                      (reset! state/classes @classes)
                                      (catch js/Error e (prn e)))
                                    (reset! loading false))
                                 1200)))))

(defn get-all-class []
  (reset! loading true)
  (swap! classes [] [])
  (get-own-classes))

(defn get-user [addresses attendees-i]
  (-> @w/contract-user .-methods
      (.getUser (get addresses attendees-i))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee]
               (let [attendee (js->clj (js/Object.assign #js {} js-attendee))]
                 (swap! state/attendees assoc (count @state/attendees) attendee))
               (if (< (inc attendees-i) (count addresses))
                 (get-user addresses (inc attendees-i)))))
      (.catch (fn [e] (prn e)))))

(defn get-all-attendee [navigation]
  (swap! state/attendees [] [])
  (if (empty? @state/attendees)
    (-> @w/contract-classfactory .-methods
        (.getClassAttendees (get @state/selected-class "0") (js/parseInt (get-in @state/selected-class ["7" 0 "groupId"])))
        (.call #js {:from @state/address})
        (.then (fn [js-attendee-addresses]
                 (let [attendee-addresses (js->clj js-attendee-addresses)]
                   (if-not (empty? attendee-addresses)
                     (get-user attendee-addresses 0)))))
        (.catch (fn [e] (prn e) (js/alert "Cannot get attendees")))
        (.finally (fn []
                    (.navigate navigation "HostDrawer")
                    (reset! TabViewMain #(fn [] [LoadingScreen]))
                    (pre-get-attendee-timeline)
                    (.closeDrawer navigation))))))

(defn selected-class [navigation classroom selected-group-i]
  (swap! state/selected-class {} classroom)
  (swap! state/selected-class assoc "7" (vec (keep-indexed #(if (= %1 selected-group-i) %2) (get classroom "7"))))
  (get-all-attendee navigation))

(defn backup-prikey []
  (reset! backup-is-copied "Copy")
  (-> (.getItem c/AsyncStorage "pk")
      (.then (fn [result]
               (reset! prikey result)))
      (.catch (fn [e] (js/alert "Cannot get private key, Try again")))
      (.finally #(reset! app-first-times true))))

(defn check-is-first-times []
  (-> (.getItem c/AsyncStorage "firsttimes")
      (.then (fn [result]
               (if (nil? result)
                 (do
                   (reset! app-first-times true)
                   (.setItem c/AsyncStorage "firsttimes" "true")))))
      (.catch (fn []
                (reset! app-first-times true)
                (prn "Can't get firsttimes storage")
                (.setItem c/AsyncStorage "firsttimes" "true")))))

;; --------------------------
;; --------- View -----------

(defn BackupQuestionModal []
  (fn [this navigation]
    [modal/Question
     "primary"
     "Copy your private key and save in safe place"
     "Later"
     @backup-is-copied
     @app-first-times
     (fn [] (reset! app-first-times false))
     (fn [] (.setString c/Clipboard @prikey) (reset! backup-is-copied "Coppied") (js/setTimeout #(reset! app-first-times false) 500))
     "Copy your private key and save in the some safe place. Private key will use again in sign in.\n\nYou can not restore your account if you lose your private key.\n\nYou can do this later in setting."]))


(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 130 constance/TABBAR-HEIGHT) :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Select course"]]]))

(defn CreateClass []
  (fn [navigation]
    [c/TouchableOpacity {:style {:position "absolute"
                                 :width 50
                                 :height 50
                                 :right 30
                                 :bottom 30
                                 :align-items "center"
                                 :justify-content "center"
                                 :border-radius 25
                                 :z-index 1
                                 :background-color s/success}
                         :on-press #(.navigate navigation "CreateClassScreen" #js {:getAllClass get-all-class})}
     [c/Icon {:name "md-add" :size s/txt-lg :color s/white}]]))

(defn ClassList []
  (fn [navigation]
    [c/View
     (if-not (empty? @state/classes)
       (doall (for [[i classroom] (map-indexed vector @state/classes)]
                (let [groups (get classroom "7")]
                  (if-not (empty? groups)
                    (doall (for [[j group] (map-indexed vector groups)]
                             [c/TouchableOpacity {:style {:background-color s/white :width "100%" :min-height 150 :padding s/ml :margin-top 10 :flex-direction "row" :justify-content "space-between"}
                                                  :on-press #(selected-class navigation classroom j)
                                                  :key (str i "-" j)}
                              [c/View {:style {:justify-content "space-between"}}
                               [c/View
                                [c/View {:style {:flex-direction "row" :align-items "flex-end"}}
                                 [c/Text {:style {:font-size s/txt-lg :font-weight s/txt-bold :color s/black}} (get classroom "3")]
                                 [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} (str " (" (inc (js/parseInt (get classroom "5"))) "/" (get classroom "4") ") ")]
                                 [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} (str " - " (get group "1"))]]
                                [c/Text {:style {:font-size s/txt-sm :color s/black :margin-top 10}} (get classroom "2")]]
                               [c/View {:style {:flex-direction "row" :align-items "flex-end"}}
                                [c/Text {:style {:color s/black :font-size s/txt-md}} (.-name @state/account)]]]
                              [c/View {:style {:justify-content "space-between" :align-items "flex-end"}}
                               [c/View]
                               [c/Text {:style {:color s/success :font-weight "bold" :font-size s/txt-md}} (try (w/web3.utils.hexToString (get classroom "0"))
                                                                                                                (catch js/Error e (prn e)))]]]))))))
       [c/View {:style {:height (- constance/HEIGHT constance/STATUSBAR-HEIGHT 130) :justify-content "center" :align-items "center"}}
        [NoDataScreen "md-today" "You have no course yet" "Create course with plus button"]])]))

(defn SelectClassScreen []
  (r/create-class
   {:component-will-mount
    (fn []
      (get-all-class)
      (check-is-first-times))
    :component-did-mount
    (fn []
      (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION))
      (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/drawer-toggle-button navigation)
      [c/View {:style {:flex 1}}
       [BackupQuestionModal]
       [Header]
       [CreateClass navigation]
       (if (= @loading true)
         [c/View {:style {:height (- constance/HEIGHT constance/STATUSBAR-HEIGHT 130) :justify-content "center" :align-items "center"}}
          [LoadingScreen]])
       [c/ScrollView {:style {:flex 1}
                      :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(get-all-class)}])}
        [ClassList navigation]]])}))
