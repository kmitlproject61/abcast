(ns abcast.screens.selectclass.attender-stack-selectclass
  (:require [reagent.core :as r]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.constance :as constance]
            [abcast.styles :as s]
            [abcast.screens.selectclass.attender-select-class :refer [SelectClassScreen]]
            [abcast.navigation.attender.drawer-button :refer [AttenderDrawerButton]]))

(def SelectClassStack (createStackNavigator
                       #js {:SelectClassScreen #js {:screen (r/reactify-component SelectClassScreen)
                                                    :navigationOptions #js {:headerLeft (fn []
                                                                                          (r/as-element
                                                                                           [c/View {:style {:width constance/WIDTH
                                                                                                            :padding-right s/ml
                                                                                                            :flex-direction "row"
                                                                                                            :justify-content "space-between"}}
                                                                                            [AttenderDrawerButton]]))
                                                                            :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))
