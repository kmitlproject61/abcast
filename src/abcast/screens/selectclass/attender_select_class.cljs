(ns abcast.screens.selectclass.attender-select-class
  (:require [reagent.core :as r :refer [atom]]
            [abcast.styles :as s]
            [abcast.requires.library :as c]
            [abcast.states.attender-global-state :as state]
            [abcast.screens.attender.main.tabview-main :refer [TabViewMain pre-get-attendance-timeline]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.components.no-data :refer [NoDataScreen]]
            [abcast.constance :as constance]
            [abcast.utils.web3 :as w]
            [abcast.components.modal :as modal]))

;; --------------------------
;; ---------- Atom ----------

(def loading (atom true))
(def popup-joinclass (atom false))
(def join-disabled (atom true))
(def join-class-loading (atom false))
(def map-code (atom ""))
(def inputed-classcode (atom 2))
(def interval (atom nil))
(def groups (atom []))
(def selected-group (atom "Select group"))
(defonce duration 3)
(def group-error (atom ""))
(def error-join-group (atom ""))
(def refreshing (atom false))
(def backup-is-copied (atom "Copy"))
(def prikey (atom ""))
(def app-first-times (atom false))

;; --------------------------
;; -------- Function --------

(defn cancel-popup []
  (reset! group-error "")
  (reset! join-disabled true)
  (reset! groups [])
  (reset! map-code "")
  (reset! error-join-group "")
  (reset! popup-joinclass false)
  (reset! selected-group "Select group")
  (reset! join-class-loading false))

(defn get-group [class-i mapCode groups-id]
  (let [group-id (get groups-id class-i)]
    (-> @w/contract-classfactory .-methods
        (.getGroup mapCode group-id)
        (.call #js {:from @state/address})
        (.then (fn [js-group]
                 (let [group (js->clj (js/Object.assign #js {} js-group))]
                   (comment "Convert teacher name")
                   (-> @w/contract-classfactory .-methods
                       (.getUser (get-in @state/classes [class-i "owner"]))
                       (.call)
                       (.then (fn [userinfo]
                                (swap! state/classes assoc-in [class-i "teacher"] (get (js->clj (js/Object.assign #js {} userinfo)) "name"))))
                       (.catch (fn [e] (prn e) (prn "Cannot set lecturer name"))))
                   (swap! state/classes assoc-in [class-i "7"] (conj (get-in @state/classes [class-i "7"]) group)))))
        (.catch (fn [error] (prn error) (prn "Cannot set course data"))))))

(defn get-class [mapCodes groups-id class-i]
  (let [mapCode (get mapCodes class-i)]
    (-> @w/contract-classfactory .-methods
        (.getClass mapCode)
        (.call #js {:from @state/address})
        (.then (fn [js-classroom]
                 (let [classroom (js->clj (js/Object.assign #js {} js-classroom))]
                   (swap! state/classes assoc-in [class-i] (conj classroom {"7" []}))
                   (get-group class-i mapCode groups-id))
                 (if (< (inc class-i) (count mapCodes))
                   (get-class mapCodes groups-id (inc class-i))))))))

(defn get-attend-classes []
  (-> @w/contract-classfactory .-methods
      (.getAttendClasses @state/address)
      (.call #js {:from @state/address})
      (.then (fn [js-joined-class]
               (let [joined-class (js->clj (js/Object.assign #js {} js-joined-class))]
                 (let [mapCodes (get joined-class "0")
                       groups-id (get joined-class "1")]
                   (get-class mapCodes groups-id 0)))))
      (.catch (fn [e] (prn e) (prn "ERROR")))
      (.finally (js/setTimeout #(reset! loading false) 800))))

(defn get-all-joined-group []
  (reset! state/classes [])
  (get-attend-classes))

(defn join-class []
  (reset! join-class-loading true)
  (reset! join-disabled true)
  (-> @w/contract-classfactory .-methods
      (.addAttendee (w/web3.utils.stringToHex @map-code) (js/parseInt (get @selected-group 0)) @state/address)
      (.send #js {:from @state/address :gas 3000000})
      (.then (fn [result]
               (if (empty? (js->clj (.-events result)))
                 (do
                   (throw "Join group failed"))
                 (do
                   (get-all-joined-group)))
               (cancel-popup)))
      (.catch (fn [error]
                (js/console.log (js/JSON.stringify error))
                (reset! join-disabled false)
                (reset! join-class-loading false)
                (reset! error-join-group "Join group failed")))))

(defn get-select-group [value]
  (let [mapCode (w/web3.utils.stringToHex value)]
    (-> @w/contract-classfactory .-methods
        (.getAllGroupId mapCode)
        (.call #js {:from @state/address})
        (.then (fn [js-groups-id]
                 (let [groups-id (js->clj js-groups-id)]
                   (if-not (empty? groups-id)
                     (do
                       (reset! group-error "")
                       (reset! groups [])
                       (doseq [group-id groups-id]
                         (-> @w/contract-classfactory .-methods
                             (.getGroup mapCode group-id)
                             (.call #js {:from @state/address})
                             (.then (fn [js-group-info]
                                      (let [group-info (js->clj (js/Object.assign #js {} js-group-info))]
                                        (reset! groups (conj @groups [(get group-info "0") (get group-info "1")])))))
                             (.catch (fn [err] (reset! group-error "Group is not found"))))))
                     (do
                       (reset! group-error "Groups is not found"))))))
        (.catch (fn [err] (prn err) (js/alert "Class is not found"))))))

(comment
  "4r4DAC"
  (-> @w/contract-classfactory .-methods
      (.getAllMapCodes)
      (.call #js {:from @state/address})
      (.then (fn [mapCodes]
               (doall (for [mapCode mapCodes]
                        (prn (w/web3.utils.hexToString mapCode))))))
      (.catch (fn [e] (prn e)))))


(defn handle-classcode-change [value]
  (js/clearInterval @interval)
  (reset! inputed-classcode duration) ; Change input delay here
  (reset! groups [])
  (reset! group-error "")
  (reset! map-code value)
  (reset! interval (js/setInterval (fn []
                                     (swap! inputed-classcode dec)
                                     (if (<= @inputed-classcode 0)
                                       (js/clearInterval @interval)
                                       (get-select-group value)))
                                   1000)))

(defn selected-class [navigation classroom]
  (reset! state/selected-class classroom)
  (.navigate navigation "AttenderDrawer")
  (reset! TabViewMain #(fn [] [LoadingScreen]))
  (pre-get-attendance-timeline))

(defn backup-prikey []
  (reset! backup-is-copied "Copy")
  (-> (.getItem c/AsyncStorage "pk")
      (.then (fn [result]
               (reset! prikey result)))
      (.catch (fn [e] (js/alert "Cannot get private key, Try again")))
      (.finally #(reset! app-first-times true))))

(defn check-is-first-times []
  (-> (.getItem c/AsyncStorage "firsttimes")
      (.then (fn [result]
               (if (nil? result)
                 (do
                   (reset! app-first-times true)
                   (.setItem c/AsyncStorage "firsttimes" "true")))))
      (.catch (fn []
                (reset! app-first-times true)
                (prn "Can't get firsttimes storage")
                (.setItem c/AsyncStorage "firsttimes" "true")))))

;; --------------------------
;; ---------- View ----------

(defn BackupQuestionModal []
  (fn [this navigation]
    [modal/Question
     "primary"
     "Copy your private key and save in safe place"
     "Later"
     @backup-is-copied
     @app-first-times
     (fn [] (reset! app-first-times false))
     (fn [] (.setString c/Clipboard @prikey) (reset! backup-is-copied "Coppied") (js/setTimeout #(reset! app-first-times false) 500))
     "Copy your private key and save in the some safe place. Private key will use again in sign in.\n\nYou can not restore your account if you lose your private key.\n\nYou can do this later in setting."]))

(defn PopupJoinclass []
  (fn []
    [modal/Custom
     ""
     "Join course"
     "Cancel"
     "Join"
     @popup-joinclass
     #(cancel-popup)
     #(join-class)
     [c/View
      [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} "Course Code"]
       [c/TextInput {:style {:padding 10 :width 220 :color s/black :font-size s/txt-sm}
                     :underlineColorAndroid s/primary
                     :on-change-text (fn [value] (handle-classcode-change value))
                     :placeholder "Receive generated course code from lecturer"
                     :editable (not @join-class-loading)}]]
      (if-not (empty? @group-error)
        [c/Text {:style {:align-self "center" :font-size s/txt-sm :color s/danger}} @group-error])
      (if-not (empty? @groups)
        [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
         [c/Text {:style {:color s/black :font-size s/txt-sm :padding 10}} "Group"]
         [c/View {:style {:width 220 :flex-direction "row"}}
          [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center" :width 220 :padding-right 7}}
           [c/Text {:style {:color s/black :font-size s/txt-sm}} (if (string? @selected-group) @selected-group (get @selected-group 1))]
           [c/Icon {:name "md-arrow-dropdown" :color s/black :size s/txt-md}]]
          [c/Picker {:style {:margin-left -270 :opacity 0 :width 283}
                     :mode "dropdown"
                     :on-value-change (fn [value]
                                        (if-not (string? value)
                                          (do
                                            (reset! selected-group (get @groups value))
                                            (reset! join-disabled false))
                                          (reset! join-disabled true)))
                     :selected-value @selected-group
                     :enabled (not @join-class-loading)}
           [c/PickerItem {:key -1 :label "Select group" :value "Select group"}]
           (for [[i group] (map-indexed vector @groups)]
             [c/PickerItem {:key i :label (get group 1) :value i}])]]])
      (if-not (empty? @error-join-group)
        [c/Text {:style {:align-self "center" :font-size s/txt-sm :color s/danger}} @error-join-group])]
     @join-disabled
     @join-class-loading]))

(defn Header []
  (fn []
    [c/View {:style {:padding s/ml :width "100%" :height (- 130 constance/TABBAR-HEIGHT) :background-color s/white}}
     [c/View {:style {:flex-direction "row" :justify-content "space-between"}}
      [c/Text {:style {:font-size s/txt-xlg
                       :font-weight s/txt-bold
                       :color s/black
                       :margin-bottom 10}}
       "Select course"]]]))

(defn JoinClass []
  (fn [navigation]
    [c/TouchableOpacity {:style {:position "absolute"
                                 :width 50
                                 :height 50
                                 :right 30
                                 :bottom 30
                                 :align-items "center"
                                 :justify-content "center"
                                 :border-radius 25
                                 :z-index 1
                                 :background-color s/success}
                         :on-press #(reset! popup-joinclass true)}
     [c/Icon {:name "md-add" :size s/txt-lg :color s/white}]]))

(defn ClassList []
  (fn [navigation]
    [c/View
     (if-not (empty? @state/classes)
       (doall (for [[i classroom] (map-indexed vector @state/classes)]
                (let [groups (get classroom "7")]
                  (if-not (empty? groups)
                    (doall (for [[j group] (map-indexed vector groups)]
                             [c/TouchableOpacity {:style {:background-color s/white :width "100%" :min-height 150 :padding s/ml :margin-top 10 :flex-direction "row" :justify-content "space-between"}
                                                  :on-press (fn [] (selected-class navigation classroom))
                                                  :key j}
                              [c/View {:style {:justify-content "space-between"}}
                               [c/View
                                [c/View {:style {:flex-direction "row" :align-items "flex-end"}}
                                 [c/Text {:style {:font-size s/txt-lg :font-weight s/txt-bold :color s/black}} (get classroom "3")]
                                 [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} (str " (" (inc (js/parseInt (get classroom "5"))) "/" (get classroom "4") ") ")]
                                 [c/Text {:style {:color s/black :font-size s/txt-md :font-weight s/txt-bold}} (str " - " (get group "1"))]]
                                [c/Text {:style {:font-size s/txt-sm :color s/black :margin-top 10}} (get classroom "2")]]
                               [c/View {:style {:flex-direction "row" :align-items "flex-end"}}
                                [c/Text {:style {:color s/black :font-size s/txt-md}} (get classroom "teacher")]]]
                              [c/View {:style {:justify-content "space-between" :align-items "flex-end"}}
                               [c/View]
                               [c/Text {:style {:color s/success :font-weight "bold" :font-size s/txt-md}} (w/web3.utils.hexToString (get classroom "0"))]]]))))))
       [c/View {:style {:height (- constance/HEIGHT constance/STATUSBAR-HEIGHT 130) :justify-content "center" :align-items "center"}}
        [NoDataScreen "md-today" "You haven't join course yet" "Join course with plus button"]])]))

(defn SelectClassScreen []
  (r/create-class
   {:component-will-mount
    (fn []
      (get-all-joined-group)
      (check-is-first-times))
    :component-did-mount
    (fn [] (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION))
      (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)))
    :reagent-render
    (fn [{:keys [navigation]} props]
      (reset! state/drawer-toggle-button navigation)
      [c/View {:style {:flex 1}}
       [BackupQuestionModal]
       [Header]
       [JoinClass]
       (if (= @loading true)
         [c/View {:style {:height (- constance/HEIGHT constance/STATUSBAR-HEIGHT 130) :justify-content "center" :align-items "center"}}
          [LoadingScreen]])
       [c/ScrollView {:style {:flex 1}
                      :refreshControl (r/as-element [c/RefreshControl {:refreshing @refreshing :on-refresh #(get-all-joined-group)}])}
        [PopupJoinclass]
        [ClassList navigation]]])}))