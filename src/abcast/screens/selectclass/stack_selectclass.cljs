(ns abcast.screens.selectclass.stack-selectclass
  (:require [reagent.core :as r]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.constance :as constance]
            [abcast.styles :as s]
            [abcast.states.global-state :as state]
            [abcast.screens.selectclass.select-class :refer [SelectClassScreen]]
            [abcast.screens.host.createclass.createclass :refer [CreateClass]]
            [abcast.navigation.host.drawer-button :refer [HostDrawerButton]]))

(def SelectClassStack (createStackNavigator
                       #js {:SelectClassScreen #js {:screen (r/reactify-component SelectClassScreen)
                                                    :navigationOptions #js {:headerLeft (fn []
                                                                                          (r/as-element
                                                                                           [c/View {:style {:width constance/WIDTH
                                                                                                            :padding-right s/ml
                                                                                                            :flex-direction "row"
                                                                                                            :justify-content "space-between"}}
                                                                                            [HostDrawerButton]]))
                                                                            :headerStyle #js {:elevation 0 :backgroundColor s/white}}}
                            :CreateClassScreen #js {:screen (r/reactify-component CreateClass)
                                                    :navigationOptions #js {:headerLeft (r/as-element [c/TouchableOpacity {:style {:margin-left s/ml}
                                                                                                                           :on-press #(.goBack @state/create-class-navigation)}
                                                                                                       [c/Icon {:name "md-close" :size s/txt-lg :color s/black}]])
                                                                            :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))
