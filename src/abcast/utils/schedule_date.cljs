(ns abcast.utils.schedule-date
  (:require [reagent.core :as r]
            [abcast.constance :as constance]))

(defn get-month-shortname [date]
  (nth constance/MONTH (- (js/parseInt (apply str (nth (split-at 5 date) 1))) 1)))

(defn get-date [date]
  (apply str (nth (split-at 8 date) 1)))
