(ns abcast.utils.authenticate
  (:require [abcast.requires.library :as c]
            [abcast.states.global-state :as state]))


(defn signout-google []
  (-> (.revokeAccess c/google-signin)
      (.then (fn [r] (prn r)))
      (.catch (fn [e] (prn e))))
  (.signOut c/google-signin))

(defn google-config []
  (.configure c/google-signin #js {:webClientId "785312740812-brosqqb1jotm3tgicr598c51da2nonap.apps.googleusercontent.com"
                                   :offlineAccess true}))

(defn init-google-idToken []
  (google-config)
  (-> (.signInSilently c/google-signin)
      (.then (fn [gmail] (reset! state/idToken gmail.idToken)))
      (.catch (fn [error]
                (prn error)
                (if (= error.code c/RNGoogleSignin.statusCodes.SIGN_IN_REQUIRED)
                  (prn "Haven't sign in yet")
                  (prn "Cannot sign in silently"))))))