(ns abcast.utils.bluetooth-service
  (:require [reagent.core :as r :refer [atom]]
            [abcast.requires.library :as c]))

;; --------------------------
;; ---------- Atom ----------

(def bluetooth-on (atom nil))
(def pairedDevices (atom []))

;; --------------------------
;; -------- Function --------

(defn switch-bluetooth []
  (.switchBluetooth c/AbcastBluetooth (fn [r] (prn r))))

(defn getPairedDevices []
  (.getPairedDevices c/AbcastBluetooth (fn [r] (reset! pairedDevices (.parse js/JSON r)))))

(defn on-state-change [resp]
  (cond (= (.. resp -type -connectionState) "on")
        (do
          (reset! bluetooth-on true)
          (getPairedDevices))
        (= (.. resp -type -connectionState) "off")
        (reset! bluetooth-on false)))

; Init blutooth current state
(-> (.getCurrentState c/RNBluetoothInfo)
    (.then on-state-change))

(def bluetooth-config #js {:uuid "00001112-0000-1000-8000-00805f9b34fb"
                           :deviceName "babyjazz"
                           :bufferSize 1024
                           :characterDelimiter "\n"})

(defn init-bluetooth-config []
  (-> (.init c/bluetooth-classic bluetooth-config)
      (.then (fn [config]
               (prn "Bluetooth successfully init configuration")
               (prn config)))
      (.catch (fn [err]
                (prn "Bluetooth failed init configuration")
                (prn err)))))

(comment
    ; Using for append device list for test icon color with regex
  (reset! pairedDevices [])
  (reset! pairedDevices (clj->js (concat (js->clj @pairedDevices) [#js {:name "tototo.js"}])))
  (reset! pairedDevices (clj->js (concat (js->clj @pairedDevices) [#js {:name "58010176"}])))
  ())
