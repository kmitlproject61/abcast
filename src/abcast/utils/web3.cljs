(ns abcast.utils.web3
  (:require [reagent.core]
            [abcast.requires.library :as c]
            [abcast.constance :as constance]
            [abcast.states.global-state :as host-state]
            [abcast.states.attender-global-state :as state]))


(def web-url "http://rpc.z3n.pw")

(defonce web3 (new c/Web3 web-url))

(def contract-user (atom (new web3.eth.Contract c/abi-classfactory "0x5Bd6018962Dc69e4B76b34A9b64a2E58aD9dFE71"))) ; Initialed value for protect error
(def contract-classfactory (atom (new web3.eth.Contract c/abi-classfactory "0x5Bd6018962Dc69e4B76b34A9b64a2E58aD9dFE71"))) ; Initialed value for protect error

(comment
  ; Get address from keystore
  (-> (.getItem c/AsyncStorage "pk")
      (.then (fn [r]
               (prn r)))
      (.catch (fn [e] (js/alert "Cannot save item in localstorage"))))
  (-> @contract-classfactory .-methods
      (.getUser @host-state/address)
      (.call)
      (.then (fn [r] (prn (js->clj (js/Object.assign #js {} r))))))
  (-> web3 .-eth (.getBalance "0xe88E5fBD140a5E874c63b292d5F9152ecEC6461B")
      (.then #(prn %))
      (.catch #(prn %))))