(ns abcast.utils.import-attendees
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.constance :as constance]
            [abcast.states.global-state :as state]
            [abcast.utils.web3 :as w]
            [abcast.utils.authenticate :refer [google-config]]
            [abcast.utils.class-service :refer [selected-class]]
            [abcast.screens.host.main.timeline-schedule :refer [update-classes-in-month actived-tab time-checking]]
            [abcast.screens.host.main.timeline-aday :refer [period-times class-date create-attendees-conclude]]))

;; --------------------------
;; ---------- Atom ----------

(def time-checking-all-months (atom []))
(def attentions-checking-all (atom []))

;; --------------------------
;; -------- Function --------

(defn set-state-attendees-joining []
  (doseq [[attendee-i attendee] (map-indexed vector @state/attendees)]
    (let [student-id (get attendee "1")]
      (doseq [attendee-template @state/attendees-template]
        (if (and (= student-id (get attendee-template "studentId"))
                 (= (get attendee-template "joined") true))
          (swap! state/attendees assoc-in [attendee-i :joined] (get attendee-template "joined")))))))

(defn get-defined-list [& {:keys [loading]}]
  (if-not (nil? loading)
    (reset! loading true))
  (-> @w/contract-classfactory .-methods
      (.getDefinedList (get @selected-class :mapcode)
                       (get-in @selected-class [:groups 0 "groupId"]))
      (.call)
      (.then (fn [attendees-template-js]
               (swap! state/attendees-template [] (js->clj (js/JSON.parse attendees-template-js)))
               (set-state-attendees-joining)))
      (.catch (fn [err] (prn "Cannot get attendees template") (prn err)))
      (.finally #(if-not (nil? loading) (reset! loading false)))))

(defn set-defined-list [attendees-template]
  (swap! state/attendees-template [] [])
  (let [imported-attendees (JSON.stringify (clj->js {:studentId attendees-template}))]
    (-> @w/contract-classfactory .-methods
        (.setDefinedList (get @selected-class :mapcode)
                         (get-in @selected-class [:groups 0 "groupId"])
                         imported-attendees
                         (count attendees-template))
        (.send #js {:from @state/address :gas 7200000})
        (.then (fn [] (get-defined-list)))
        (.catch (fn [err] (prn "Cannot set attendees template") (prn err))))))

(defn import-students [& {:keys [loading]}]
  (if-not (nil? loading)
    (reset! loading true))
  (let [attendees-template (atom [])]
    (.show c/document-picker #js {:filetype #js [(c/document-picker-util.allFiles)]}
           (fn [err res]
             (let [file (clj->js {:name res.fileName :uri res.uri :type res.type})]
               (let [formdata (new js/FormData)]
                 (.append formdata "excel" file)
                 (-> (js/fetch constance/api-excel #js {:method "post"
                                                        :headers #js {:Content-Type "multipart/form-data"
                                                                      :Authorization (str "bearer " @state/idToken)}
                                                        :body formdata})
                     (.then #(.json %))
                     (.then (fn [response]
                              (reset! attendees-template (vec (map #(str %) (js->clj response.data))))
                              (set-defined-list @attendees-template)))
                     (.catch (fn [error] (js/alert "Cannot read excel file") (prn (js/Object.assign #js {} error))))
                     (.finally #(reset! loading false)))))))))

(defn do-write-excel []
  (reset! state/exporting true)
  (swap! attentions-checking-all [] [])
  (swap! time-checking-all-months [] [])
  (comment "Concat time-checking all month from timeline-schedule")
  (let [current-actived-tab (get @actived-tab :month)]
    (doseq [month (range (apply min @state/range-month) (inc (apply max @state/range-month)))]
      (swap! actived-tab assoc :month (-> (c/moment) (.month (dec month)) (.format "MMMM")))
      (update-classes-in-month (-> (c/moment) (.month (dec month)) (.format "YYYY-MM-DD")))
      (swap! time-checking-all-months concat @time-checking))
     ; Set tab back to default
    (swap! time-checking-all-months [] (vec @time-checking-all-months))
    (comment "Get attention number per student from timeline-aday")
    (doseq [attendee @state/attendees]
      (swap! attentions-checking-all conj {(keyword " Student Id") (get attendee "1")}))
    (doseq [attention-date @time-checking-all-months]
      (doseq [attention-time (get attention-date :times)]
        (swap! period-times [] (get attention-time :period))
        (swap! class-date assoc :date (get attention-date :date))
        (create-attendees-conclude)
        (doseq [[student-i student] (map-indexed vector (get-in @state/timeline-aday [:attendees-conclude]))]
          (swap! attentions-checking-all assoc-in [student-i (keyword (str (get attention-date :date)
                                                                           "["
                                                                           (get-in attention-time [:period 0])
                                                                           "]"))]
                 (if (>= (count (filter true? (get student :attention)))
                         (get attention-time :numpresent))
                   "1" "-"))
          (swap! attentions-checking-all assoc-in [student-i :SUM]
                 (str (count (filter #(= % "1") (vals (get-in @attentions-checking-all [student-i]))))
                      "/"
                      (count (filter #(or (= % "1") (= % "-")) (vals (get-in @attentions-checking-all [student-i]))))))
          (swap! attentions-checking-all assoc-in [student-i :Percent]
                 (Math/ceil (* 100 (/ (count (filter #(= % "1") (vals (get-in @attentions-checking-all [student-i]))))
                                      (count (filter #(or (= % "1") (= % "-")) (vals (get-in @attentions-checking-all [student-i])))))))))))
    (doseq [[i attention-checking-all] (map-indexed vector @attentions-checking-all)]
      (swap! attentions-checking-all assoc i (into (sorted-map) attention-checking-all)))
    (comment "Send to backend")
    (-> (js/fetch constance/api-export-excel #js {:method "post"
                                                  :headers #js {:Content-Type "application/json"
                                                                :Authorization (str "bearer " @state/idToken)}
                                                  :body (js/JSON.stringify (clj->js {:data @attentions-checking-all}))})
        (.then #(.json %))
        (.then (fn [response]
                 (if (= response.success true)
                   (-> (c/fetch-blob.config #js {:addAndroidDownloads #js {:useDownloadManager true
                                                                           :notification true
                                                                           :appendExt "xlsx"
                                                                           :path (str c/fetch-blob.fs.dirs.DownloadDir "/ABCAS-" (get @selected-class :class-name) "-sec" (get-in @selected-class [:groups 0 "groupCode"]) ".xlsx")}
                                                 :fileCache true})
                       (.fetch "GET" (str constance/api "/" response.file))
                       (.then (fn [res]
                                (js/alert "Download successfull")
                                (prn (js/Object.assign #js {} res))))
                       (.catch (fn [err] (prn err))))
                   (js/alert "Cannot export excel file"))))
        (.catch (fn [error] (js/alert "Cannot export excel") (prn (js/Object.assign #js {} error))))
        (.finally (fn []
                    (swap! actived-tab assoc :month (-> (c/moment) (.month current-actived-tab) (.format "MMMM")))
                    (update-classes-in-month (-> (c/moment) (.month current-actived-tab) (.format "YYYY-MM-DD")))
                    (reset! state/exporting false))))))

(defn write-excel []
  (-> c/PermissionsAndroid (.request c/PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
      (.then (fn [granted]
               (if (= granted c/PermissionsAndroid.RESULTS.GRANTED)
                 (do-write-excel)
                 (prn "Permission Denied"))))
      (.catch (fn [error] (js/alert "Request permission error") (prn error)))))

; Auto run
(reset! state/export-excel #(write-excel))

; test
(comment
  (get @state/timeline-aday :attendees-conclude)
  (reset! time-checking-all-months [])
  @time-checking-all-months
  (reset! attentions-checking-all [])
  @attentions-checking-all
  (get @state/timeline-aday :attendees-conclude)
  (swap! state/timeline-aday assoc-in [:attendees-conclude 1 :name] "nattapat"))