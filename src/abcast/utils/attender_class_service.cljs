(ns abcast.utils.attender-class-service
  (:require [reagent.core :as r :refer [atom]]
            [abcast.states.attender-global-state :as state]
            [abcast.requires.library :as c]))

(defonce selected-class (atom {:mapcode (get @state/selected-class "0")
                               :class-address (get @state/selected-class "1")
                               :class-code (get @state/selected-class "2")
                               :class-name (get @state/selected-class "3")
                               :academic-year (get @state/selected-class "4")
                               :term (get @state/selected-class "5")
                               :is-close (get @state/selected-class "6")
                               :groups (get @state/selected-class "7")
                               :group-id (get-in @state/selected-class ["7" 0 "0"])
                               :group-code (get-in @state/selected-class ["7" 0 "1"])
                               :group-room (get-in @state/selected-class ["7" 0 "2"])
                               :group-day (get-in @state/selected-class ["7" 0 "3"])
                               :group-start-time (get-in @state/selected-class ["7" 0 "4"])
                               :group-end-time (get-in @state/selected-class ["7" 0 "5"])
                               :group-schedule-start (get-in @state/selected-class ["7" 0 "6"])
                               :group-schedule-end (get-in @state/selected-class ["7" 0 "7"])}))

(if js/goog.DEBUG
  (-> (.getItem c/AsyncStorage "selected-class")
      (.then (fn [selected-class] (swap! state/selected-class [] (js->clj (js/JSON.parse selected-class)))))))

(add-watch state/selected-class :watcher
           (fn []
             (reset! selected-class {:mapcode (get @state/selected-class "0")
                                     :class-address (get @state/selected-class "1")
                                     :class-code (get @state/selected-class "2")
                                     :class-name (get @state/selected-class "3")
                                     :academic-year (get @state/selected-class "4")
                                     :term (get @state/selected-class "5")
                                     :is-close (get @state/selected-class "6")
                                     :groups (get @state/selected-class "7")
                                     :group-id (get-in @state/selected-class ["7" 0 "0"])
                                     :group-code (get-in @state/selected-class ["7" 0 "1"])
                                     :group-room (get-in @state/selected-class ["7" 0 "2"])
                                     :group-day (get-in @state/selected-class ["7" 0 "3"])
                                     :group-start-time (get-in @state/selected-class ["7" 0 "4"])
                                     :group-end-time (get-in @state/selected-class ["7" 0 "5"])
                                     :group-schedule-start (get-in @state/selected-class ["7" 0 "6"])
                                     :group-schedule-end (get-in @state/selected-class ["7" 0 "7"])})
             (if js/goog.DEBUG
              (-> (.setItem c/AsyncStorage "selected-class" (js/JSON.stringify (clj->js @state/selected-class)))))))

(add-watch state/classes :watcher
           (fn []
             (doseq [[i classroom] (map-indexed vector @state/classes)]
               (if (= (get classroom "0") (get @selected-class :mapcode))
                 (reset! selected-class {:mapcode (get classroom "0")
                                         :class-address (get classroom "1")
                                         :class-code (get classroom "2")
                                         :class-name (get classroom "3")
                                         :academic-year (get classroom "4")
                                         :term (get classroom "5")
                                         :is-close (get classroom "6")
                                         :groups (get classroom "7")
                                         :group-id (get-in classroom ["7" 0 "0"])
                                         :group-code (get-in classroom ["7" 0 "1"])
                                         :group-room (get-in classroom ["7" 0 "2"])
                                         :group-day (get-in classroom ["7" 0 "3"])
                                         :group-start-time (get-in classroom ["7" 0 "4"])
                                         :group-end-time (get-in classroom ["7" 0 "5"])
                                         :group-schedule-start (get-in classroom ["7" 0 "6"])
                                         :group-schedule-end (get-in classroom ["7" 0 "7"])})))))
