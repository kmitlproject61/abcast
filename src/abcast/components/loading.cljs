(ns abcast.components.loading
  (:require [reagent.core :as r]
            [abcast.requires.images :as image]
            [abcast.styles :as s]
            [abcast.requires.library :as c]))

(defn LoadingScreen []
  [c/View {:style {:flex 1 :justify-content "center" :align-items "center"}}
   [c/Image {:source image/logo-large :style {:width 30 :height 30 :margin-bottom 10}}]
   [c/Text {:style {:color s/black :font-size s/txt-sm}} "Loading..."]])