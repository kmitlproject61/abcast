(ns abcast.components.no-data
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.styles :as s]))

(defn NoDataScreen [icon title description]
  (fn []
    [c/View {:style {:flex 1 :justify-content "center" :align-items "center" :margin s/ml}}
     [c/Icon {:name icon :color s/danger :size 60 :style {:opacity 0.7}}]
     [c/Text {:style {:color s/black :font-size s/txt-xlg :text-align "center" :opacity 0.7 :margin-bottom 10}} title]
     [c/Text {:style {:color s/black :font-size s/txt-md :text-align "center" :opacity 0.5}} description]]))