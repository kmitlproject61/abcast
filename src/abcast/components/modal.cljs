(ns abcast.components.modal
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.requires.images :as image]
            [abcast.constance :as constance]
            [abcast.styles :as s]))

(defn Simple []
  (fn [modal-type title actions-title visible on-touch-outside contents]
    [c/Dialog {:visible visible
               :dialogTitle (r/as-element [c/DialogTitle {:title title
                                                          :textStyle {:font-size s/txt-md :color (cond (= modal-type "warning") s/warning
                                                                                                       (= modal-type "danger") s/danger
                                                                                                       (= modal-type "success") s/success
                                                                                                       (= modal-type "primary") s/primary
                                                                                                       :else s/black)}
                                                          :color s/black :hasTitleBar false}])
               :actions (array (r/as-element [c/DialogButton {:text actions-title
                                                              :textStyle {:font-size s/txt-md :color (cond (= modal-type "warning") s/warning
                                                                                                           (= modal-type "danger") s/danger
                                                                                                           (= modal-type "success") s/success
                                                                                                           (= modal-type "primary") s/primary
                                                                                                           :else s/black)}
                                                              :on-press on-touch-outside :key 0}]))
               :onTouchOutside on-touch-outside
               :width (- constance/WIDTH (* s/ml 2))}
     [c/DialogContent
      [c/Text contents]]]))

(defn Question []
  (fn [modal-type title actions-title-no actions-title-yes visible on-touch-outside-no on-touch-outside-yes contents]
    [c/Dialog {:visible visible
               :dialogTitle (r/as-element [c/DialogTitle {:title title
                                                          :textStyle {:font-size s/txt-md :color (cond (= modal-type "warning") s/warning
                                                                                                       (= modal-type "danger") s/danger
                                                                                                       (= modal-type "success") s/success
                                                                                                       (= modal-type "primary") s/primary
                                                                                                       :else s/black)}
                                                          :color s/black
                                                          :hasTitleBar false}])
               :actions (array (r/as-element [c/DialogButton {:text actions-title-no
                                                              :textStyle {:font-size s/txt-md :color s/black}
                                                              :on-press on-touch-outside-no :key 0}])
                               (r/as-element [c/DialogButton {:text actions-title-yes
                                                              :textStyle {:font-size s/txt-md :color (cond (= modal-type "warning") s/warning
                                                                                                           (= modal-type "danger") s/danger
                                                                                                           (= modal-type "success") s/success
                                                                                                           (= modal-type "primary") s/primary
                                                                                                           :else s/black)}
                                                              :on-press on-touch-outside-yes :key 1}]))
               :onTouchOutside on-touch-outside-no
               :width (- constance/WIDTH (* s/ml 2))}
     (if (not-empty contents)
       [c/DialogContent
        [c/Text contents]])]))

(defn Custom []
  (fn [modal-type title actions-title-no actions-title-yes visible on-touch-outside-no on-touch-outside-yes contents yes-btn-disabled no-btn-disabled]
    [c/Dialog {:visible visible
               :dialogTitle (r/as-element [c/DialogTitle {:title title
                                                          :textStyle {:font-size s/txt-md :color (cond (= modal-type "warning") s/warning
                                                                                                       (= modal-type "danger") s/danger
                                                                                                       (= modal-type "success") s/success
                                                                                                       (= modal-type "primary") s/primary
                                                                                                       :else s/black)}
                                                          :color s/black
                                                          :hasTitleBar false}])
               :actions (array (r/as-element [c/DialogButton {:text actions-title-no
                                                              :style {:background-color (if (= no-btn-disabled true) s/default s/white)}
                                                              :textStyle {:font-size s/txt-md
                                                                          :color (if (= no-btn-disabled true)
                                                                                   s/black
                                                                                   s/primary)}
                                                              :on-press on-touch-outside-no
                                                              :key 0
                                                              :disabled no-btn-disabled}])
                               (r/as-element [c/DialogButton {:text actions-title-yes
                                                              :style {:background-color (if (= no-btn-disabled true) s/default s/white)}
                                                              :textStyle {:font-size s/txt-md
                                                                          :color (if (= modal-type "warning")
                                                                                   s/warning
                                                                                   (if (= yes-btn-disabled true)
                                                                                     s/black
                                                                                     s/primary))}
                                                              :on-press on-touch-outside-yes
                                                              :key 1
                                                              :disabled yes-btn-disabled}]))
               :onTouchOutside on-touch-outside-no
               :width (- constance/WIDTH (* s/ml 2))}
     (if (not-empty contents)
       [c/DialogContent {:style {:max-height (- constance/HEIGHT 200)}} contents])]))

(defn Loading []
  (fn [visible contents]
    [c/Dialog {:visible visible
               :width (- constance/WIDTH (* s/ml 4))
               :dialogStyle {:background-color "transparent"}}
     [c/DialogContent {:style {:height 150 :align-items "center" :justify-content "center" :margin-top 20}}
      [c/Image {:source image/logo-large :style {:width 80 :height 80}}]
      [c/Text {:style {:color s/white :font-size s/txt-md :margin-top 28}} contents]]]))
