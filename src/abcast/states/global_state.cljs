(ns abcast.states.global-state
  (:require [reagent.core :as r :refer [atom]]))

(def drawer-toggle-button
  "State of navigation's drawer toggle button
    NOTE dispatch this navigation state in first stack in first tab in drawer navigator"
  (atom {}))

(def main-navigation (atom {}))

(def filter-modal-conclude (atom false))

(def menu-schedule-navigation (atom {}))

(def timeline-aday-navigation (atom {}))

(def checking-conclude-navigation (atom {}))

(def create-class-navigation (atom {}))

(def drawer-screen-visible (atom false)) ; Animation

(def timeline-aday (atom {:num-present 0 :tab 0 :rendered false :show-xlsx-modal false}))

(def refresh-tabview (atom #(prn "Function update tavview screen not working")))

(def export-excel (atom #()))
(def exporting (atom false))

(def period (atom 30))
(def numpresent (atom 4))

(defonce range-month (atom []))
(defonce attendances (atom []))

(defonce account (atom {}))

(defonce idToken (atom ""))

(defonce address (atom nil))

(defonce classes (atom []))

(defonce selected-class (atom []))

(defonce attendees (atom []))

(defonce attendees-template (atom []))