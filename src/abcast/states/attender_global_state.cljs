(ns abcast.states.attender-global-state
  (:require [reagent.core :as r :refer [atom]]))

(def drawer-toggle-button
  "State of navigation's drawer toggle button
    NOTE dispatch this navigation state in first stack in first tab in drawer navigator"
  (atom {}))

(def main-navigation (atom {}))

(def filter-modal-conclude (atom false))

(def menu-schedule-navigation (atom {}))

(def timeline-aday-navigation (atom {}))

(def drawer-screen-visible (atom false)) ; Animation

(def refresh-tabview (atom #(prn "Function update tavview screen not working")))

(def range-month (atom []))
(def attendances (atom []))

(defonce account (atom {}))

(defonce address (atom nil))

(defonce selected-class (atom []))

(defonce classes (atom []))
