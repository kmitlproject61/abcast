(ns abcast.android.core
  (:require [reagent.core :as r :refer [atom]]
            [re-frame.core :refer [subscribe dispatch dispatch-sync]]
            [abcast.events]
            [abcast.subs]
            [abcast.screens.login.login :refer [LoginScreen LoginModal] :as login-state]
            [abcast.navigation.host.drawer :refer [HostMain]]
            [abcast.navigation.attender.drawer :refer [AttenderMain]]
            [abcast.screens.tutorial.tutorial :refer [HostTutorial AttenderTutorial]]
            [abcast.requires.library :as c]
            [abcast.utils.authenticate :refer [init-google-idToken]]
            [abcast.components.modal :as modal]
            [abcast.utils.web3 :as w]
            [abcast.constance :as constance]
            [abcast.states.attender-global-state :as attender-state]
            [abcast.states.global-state :as host-state]))

;; --------------------------
;; -------- Atom --------

(defonce screen (atom LoginScreen))

;; --------------------------
;; -------- Function --------

(defn create-wallet [prikey]
  (let [myAccount (-> w/web3 .-eth .-accounts (.privateKeyToAccount prikey))]
    (-> w/web3 .-eth .-accounts .-wallet .clear)
    (-> w/web3 .-eth .-accounts .-wallet (.create 0))
    (-> w/web3 .-eth .-accounts .-wallet (.add myAccount))))

(defn check-is-first-times [role]
  (-> (.getItem c/AsyncStorage "firsttimes")
      (.then (fn [result]
               (if (nil? result)
                 (if (= role "teacher")
                   (reset! screen (fn [] (HostTutorial #(reset! screen HostMain))))
                   (reset! screen (fn [] (AttenderTutorial #(reset! screen AttenderMain)))))
                 (if (= role "teacher")
                   (reset! screen HostMain)
                   (reset! screen AttenderMain)))))
      (.catch (fn []
                (if (= role "teacher")
                  (reset! screen HostMain)
                  (reset! screen AttenderMain))
                (prn "Can't get firsttimes storage")))
      (.finally #(.setItem c/AsyncStorage "firsttimes" "true"))))

(defn check-login []
  ; Set contract address from backend
  (-> (js/fetch constance/get-address #js {:method "GET"})
      (.then #(.json %))
      (.then (fn [res]
               (reset! w/contract-user (new w/web3.eth.Contract res.abi res.address))
               (reset! w/contract-classfactory (new w/web3.eth.Contract res.abi res.address))))
      (.catch (fn [err]
                (prn err)
                (js/alert "Cannot set contract address")))
      (.finally (fn []
                  ; Get storage for check login
                  (-> (.getItem c/AsyncStorage "account")
                      (.then (fn [account]
                               (-> (.getItem c/AsyncStorage "address")
                                   (.then (fn [address]
                                            (let [role (.-role (js/JSON.parse account))]
                                              (-> (.getItem c/AsyncStorage "pk")
                                                  (.then (fn [prikey]
                                                           (create-wallet prikey)
                                                           (init-google-idToken)
                                                           (cond (= role "student") (do
                                                                                      (reset! attender-state/address address)
                                                                                      (reset! attender-state/account (js/JSON.parse account))
                                                                                      (check-is-first-times "student"))
                                                                 (= role "teacher") (do
                                                                                      (reset! host-state/address address)
                                                                                      (reset! host-state/account (js/JSON.parse account))
                                                                                      (check-is-first-times "teacher")))))
                                                  (.catch (fn [e] (prn e) (prn "No pk stored")))))))
                                   (.catch (fn [e] (prn e) (prn "No address stored"))))))
                      (.catch (fn [e] (prn "No account stored")))
                      (.finally (fn [] (js/setTimeout #(.hide c/splash-screen) 300))))))))

;; --------------------------
;; ---------- View ----------

(defn Loading []
  (fn []
    [modal/Loading
     @login-state/signing-in
     "Loading..."]))

(defn app-root []
  (r/create-class
   {:component-will-mount
    (fn []
      (check-login)
      (add-watch login-state/logged-in :watcher (fn [] (if (not= @login-state/logged-in false) (check-is-first-times @login-state/logged-in)))))
    :reagent-render
    (fn []
      [c/View {:flex 1}
       [Loading]
       [LoginModal]
       [@screen]])}))



; Ignore react-navigation warning message
(.ignoreWarnings c/yellowBox #js ["Warning: isMounted(...) is deprecated"
                                  "Warning: Can't call setState (or forceUpdate) on an unmounted component"])

(defn init []
  (dispatch-sync [:initialize-db])
  (.registerComponent c/app-registry "ABCast" #(r/reactify-component app-root)))
