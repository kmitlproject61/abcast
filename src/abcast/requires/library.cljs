(ns abcast.requires.library
  (:require [reagent.core :as r]))

(defonce ReactNative (js/require "react-native"))
(defonce app-registry (.-AppRegistry ReactNative))
(defonce Text (r/adapt-react-class (.-Text ReactNative)))
(defonce TextInput (r/adapt-react-class (.-TextInput ReactNative)))
(defonce View (r/adapt-react-class (.-View ReactNative)))
(defonce ScrollView (r/adapt-react-class (.-ScrollView ReactNative)))
(defonce Image (r/adapt-react-class (.-Image ReactNative)))
(defonce TouchableHighlight (r/adapt-react-class (.-TouchableHighlight ReactNative)))
(defonce TouchableOpacity (r/adapt-react-class (.-TouchableOpacity ReactNative)))
(defonce TextInput (r/adapt-react-class (.-TextInput ReactNative)))
(defonce dimensions (.-Dimensions ReactNative))
(defonce yellowBox (.-YellowBox ReactNative))
(defonce statusbar (.-StatusBar ReactNative))
(defonce time-picker-android (.-TimePickerAndroid ReactNative))
(defonce date-picker-android (.-DatePickerAndroid ReactNative))
(defonce Picker (r/adapt-react-class (.-Picker ReactNative)))
(defonce PickerItem (r/adapt-react-class (.-Item (.-Picker ReactNative))))
(defonce animated (.-Animated ReactNative))
(defonce animated-value (.-Value animated))
(defonce AnimatedView (r/adapt-react-class (.-View animated)))
(defonce AnimatedText (r/adapt-react-class (.-Text animated)))
(defonce AnimatedImage (r/adapt-react-class (.-Image animated)))
(defonce Easing (.-Easing ReactNative))
(defonce NativeModules (.-NativeModules ReactNative))
(defonce AsyncStorage (.-AsyncStorage ReactNative))
(defonce RefreshControl (r/adapt-react-class (.-RefreshControl ReactNative)))
(defonce Clipboard (.-Clipboard ReactNative))
(def PermissionsAndroid (.-PermissionsAndroid ReactNative))
(def alert (.-Alert ReactNative))

(defonce ReactNavigation (js/require "react-navigation"))
(defonce createDrawerNavigator (.-createDrawerNavigator ReactNavigation))
(defonce createBottomTabNavigator (.-createBottomTabNavigator ReactNavigation))
(defonce createMaterialTopTabNavigator (.-createMaterialTopTabNavigator ReactNavigation))
(defonce navigationActions (.-NavigationActions ReactNavigation))
(defonce createStackNavigator (.-createStackNavigator ReactNavigation))
(defonce drawerActions (.-DrawerActions ReactNavigation))

(defonce ReactVectorIcon (js/require "react-native-vector-icons/Ionicons"))
(defonce Icon (r/adapt-react-class (.-default ReactVectorIcon)))

(defonce ReactCircularProgress (js/require "react-native-circular-progress"))
(defonce AnimatedCircularProgress (r/adapt-react-class (.-AnimatedCircularProgress ReactCircularProgress)))

(defonce ReactPopDialog (js/require "react-native-popup-dialog"))
(defonce Dialog (r/adapt-react-class (.-default ReactPopDialog)))
(defonce DialogTitle (r/adapt-react-class (.-DialogTitle ReactPopDialog)))
(defonce DialogContent (r/adapt-react-class (.-DialogContent ReactPopDialog)))
(defonce DialogButton (r/adapt-react-class (.-DialogButton ReactPopDialog)))

(defonce ReactCalendars (js/require "react-native-calendars"))
(defonce CalendarList (r/adapt-react-class (.-CalendarList ReactCalendars)))
(defonce Calendar (r/adapt-react-class (.-Calendar ReactCalendars)))

(defonce moment (js/require "moment"))

(defonce ReactChartKit (js/require "react-native-chart-kit"))
(defonce LineChart (r/adapt-react-class (.-LineChart ReactChartKit)))
(defonce BarChart (r/adapt-react-class (.-BarChart ReactChartKit)))
(defonce ProgressChart (r/adapt-react-class (.-ProgressChart ReactChartKit)))

(defonce ReactSwiper (js/require "react-native-swiper"))
(defonce Swiper (r/adapt-react-class ReactSwiper))

(defonce ReactSwitchToggle (js/require "react-native-switch-toggle"))
(defonce SwitchToggle (r/adapt-react-class (.-default ReactSwitchToggle)))

(defonce ReactNativePose (js/require "react-native-pose"))
(defonce posed (.-default ReactNativePose))

(defonce RNGoogleSignin (js/require "react-native-google-signin"))
(defonce google-signin (.-GoogleSignin RNGoogleSignin))

(defonce ReactClassicBluetooth (js/require "easy-bluetooth-classic"))
(defonce bluetooth-classic ReactClassicBluetooth)

(defonce AbcastBluetooth (.-AbcastBluetooth NativeModules))

(defonce ReactNativeBluetoothInfo (js/require "react-native-bluetooth-info"))
(defonce RNBluetoothInfo (.-default ReactNativeBluetoothInfo))

(defonce background-timer (.-default (js/require "react-native-background-timer")))

(defonce SplashScreen (js/require "react-native-splash-screen"))
(defonce splash-screen (.-default SplashScreen))

(defonce DocumentPicker (js/require "react-native-document-picker"))
(defonce document-picker (.-DocumentPicker DocumentPicker))
(defonce document-picker-util (.-DocumentPickerUtil DocumentPicker))

(defonce RNRestart (js/require "react-native-restart"))
(defonce rn-restart (.-default RNRestart))

(defonce RNFetchBlob (js/require "react-native-fetch-blob"))
(defonce fetch-blob (.-default RNFetchBlob))

(defonce RNresponseFontSize (js/require "react-native-responsive-fontsize"))
(defonce rf (.-default RNresponseFontSize))

(js/require "./shim.js")
(js/require "./global.js")
(js/require "crypto")
(defonce Web3 (js/require "web3"))

;ABI
(def abi-user (js/require "./abi/user.json"))
(def abi-classfactory (js/require "./abi/classfactory.json"))
