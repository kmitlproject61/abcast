(ns abcast.navigation.host.drawer-button
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.states.global-state :as state]))

(defn HostDrawerButton []
  (fn []
    [c/TouchableOpacity {:on-press (fn []
                                     (.openDrawer @state/drawer-toggle-button)
                                     (reset! state/drawer-screen-visible false)
                                     (js/setTimeout #(reset! state/drawer-screen-visible true) 20))}
     [c/Icon {:name "ios-menu" :size s/txt-xlg :color s/black :style {:margin-left s/ml}}]]))
