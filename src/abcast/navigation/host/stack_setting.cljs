(ns abcast.navigation.host.stack-setting
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.screens.host.setting.setting :refer [SettingScreen]]
            [abcast.navigation.host.drawer-button :refer [HostDrawerButton]]))

(def SettingStack (createStackNavigator
                   #js {:SettingScreen #js {:screen (r/reactify-component SettingScreen)
                                            :navigationOptions #js {:headerLeft (fn []
                                                                                  (r/as-element
                                                                                   [c/View {:style {:width constance/WIDTH
                                                                                                    :padding-right s/ml
                                                                                                    :flex-direction "row"
                                                                                                    :justify-content "space-between"}}
                                                                                    [HostDrawerButton]]))
                                                                    :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))