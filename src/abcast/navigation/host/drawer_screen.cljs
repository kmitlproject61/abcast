(ns abcast.navigation.host.drawer-screen
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.navigation.host.drawer-routing :refer [host-drawer-routing]]
            [abcast.requires.images :as image]
            [abcast.screens.host.main.tabview-main :refer [pre-get-attendee-timeline  TabViewMain]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.screens.host.attenderchecking.attender-checking :refer [isScan show-cant-change-class-modal]]
            [abcast.states.global-state :as state]
            [abcast.components.modal :as modal]
            [abcast.states.global-state :as state]
            [abcast.screens.host.schedule.schedule :refer [update-init-marked-date]]
            [abcast.utils.web3 :as w]))


;; --------------------------
;; ---------- Atom ----------

(def colors [:darkblue
             :darkcyan
             :darkgreen
             :darkkhaki
             :darkmagenta
             :darkolivegreen
             :darkorange
             :darkorchid
             :darkred
             :darksalmon
             :darkviolet])

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:x 0 :transition #js {:ease "easeOut" :duration 200}}
                                                           :hidden #js {:x -220 :transition #js {:duration 10}}})))

;; --------------------------
;; -------- Function --------

(defn get-user [addresses attendees-i]
  (-> @w/contract-user .-methods
      (.getUser (get addresses attendees-i))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee]
               (let [attendee (js->clj (js/Object.assign #js {} js-attendee))]
                 (swap! state/attendees assoc (count @state/attendees) attendee))
               (if (< (inc attendees-i) (count addresses))
                 (get-user addresses (inc attendees-i)))))
      (.catch (fn [e] (prn e)))))

(defn get-all-attendee [navigation]
  (swap! state/attendees [] [])
  (-> @w/contract-classfactory .-methods
      (.getClassAttendees (get @state/selected-class "0") (js/parseInt (get-in @state/selected-class ["7" 0 "groupId"])))
      (.call #js {:from @state/address})
      (.then (fn [js-attendee-addresses]
               (let [attendee-addresses (js->clj js-attendee-addresses)]
                 (if-not (empty? attendee-addresses)
                   (get-user attendee-addresses 0)))))
      (.catch (fn [e] (prn e) (js/alert "Cannot get attendees")))
      (.finally (fn []
                  (.navigate navigation "HostDrawer")
                  (reset! TabViewMain #(fn [] [LoadingScreen]))
                  (pre-get-attendee-timeline)
                  (.closeDrawer navigation)))))

(defn selected-class [navigation classroom selected-group-i]
  (if (= @isScan true)
    (reset! show-cant-change-class-modal true)
    (do
      (swap! state/selected-class {} classroom)
      (swap! state/selected-class assoc "7" (vec (keep-indexed #(if (= %1 selected-group-i) %2) (get classroom "7"))))
      (get-all-attendee navigation)
      (update-init-marked-date (-> (c/moment) (.format "YYYY-MM-DD"))))))

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn [navigation]
    [c/View {:style {:width "100%" :height 110 :background-color s/default :padding 10 :justify-content "space-between"}}
     [c/Image {:source {:uri (.-photo @state/account)}
               :style {:width 50 :height 50 :border-radius 25}}]
     [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
      [c/Text {:style {:color s/white :font-size s/txt-sm}} (.-name @state/account)]
      [c/TouchableOpacity {:style {:width 30 :height 30 :justify-content "center" :align-items "center"}
                           :on-press #(if (= @isScan true)
                                        (reset! show-cant-change-class-modal true)
                                        (.navigate navigation "SettingDrawer"))}
       [c/Icon {:name "ios-settings" :size s/txt-md :color s/white}]]]
     [c/View {:width "120%" :height 110 :position "absolute" :opacity 0.6 :right 0 :background-color s/black :zIndex -1}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 90 :height 90 :zIndex -1 :left (rand-nth [10 125 250])}}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 30 :height 30 :zIndex -1 :left (rand-nth [10 125 250])}}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 190 :height 90 :zIndex -1 :left (rand-nth [10 125 250])}}]]))

(defn HostDrawerScreen []
  (fn [{:keys [navigation]} props]
    [c/ScrollView
     [ViewAnimated {:style {:background-color s/white} :pose (if (= @state/drawer-screen-visible true) "visible" "hidden")}
      [Header navigation]
      [c/View
       [ItemAnimated {:style {:padding 10 :background-color s/white :margin-top 2}}
        [c/View {:style {:padding 10 :flex-direction "row" :justify-content "space-between"}}
         [c/View {:style {:width 50}}
          [c/Icon {:name "ios-book" :color s/black :size s/txt-md}]]
         [c/TouchableOpacity {:style {:width 200} :on-press #(if (= @isScan true)
                                                               (reset! show-cant-change-class-modal true)
                                                               (.navigate navigation "SelectClassDrawer"))}
          [c/Text {:style {:color s/black :font-size s/txt-sm}} "All Courses"]]]]]
      [c/View {:style {:background-color s/default :height 1}}]
      [c/View {:style {:padding 10 :background-color s/white}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :padding-bottom 20}} "Courses"]
       (if (empty? @state/classes)
         [ItemAnimated {:style {:padding 10 :flex-direction "row" :padding-bottom 20}}
          [c/View {:style {:width 200}}
           [c/Text {:style {:color s/black :font-size s/txt-sm :opacity 0.5}} "No Course"]]]
         (doall (for [[i classroom] (map-indexed vector @state/classes)]
                  (let [groups (get classroom "7")]
                    (if-not (empty? groups)
                      (doall (for [[j group] (map-indexed vector groups)]
                               [ItemAnimated {:key (str i "-" j)}
                                [c/TouchableOpacity {:style {:padding 10 :flex-direction "row" :padding-bottom 20}
                                                     :on-press #(selected-class navigation classroom j)}
                                 [c/View {:style {:width 50}}
                                  [c/View {:width s/txt-lg :height s/txt-lg :border-radius (/ s/txt-lg 2) :justify-content "center" :align-items "center" :background-color (rand-nth colors)}
                                   [c/Text {:style {:color s/white :font-size s/txt-xsm}} (first (get classroom "3"))]]]
                                 [c/View {:style {:width 200}}
                                  [c/Text {:style {:color s/black :font-size s/txt-sm}} (str (get classroom "3") " - " (get-in classroom ["7" j "1"]))]]]])))))))]]]))