(ns abcast.navigation.host.tabbar-navigation
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.navigation.host.stack-navigation :refer [MainStack AttenderCheckingStack ScheduleStack JoinedAttendersStack]]
            [abcast.requires.library :refer [createBottomTabNavigator Icon]]
            [abcast.constance :as constance]))

(def HostTabs (createBottomTabNavigator
               #js {:MainTab #js {:screen (r/reactify-component MainStack)
                                  :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                              :style #js {:height constance/TABBAR-HEIGHT}}
                                                          :tabBarIcon (fn [props]
                                                                        (r/as-element
                                                                         [Icon {:name "ios-home"
                                                                                :size s/txt-xlg
                                                                                :color (if props.focused s/primary s/black)}]))}}
                    :AttenderCheckingTab #js {:screen (r/reactify-component AttenderCheckingStack)
                                              :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                          :style #js {:height constance/TABBAR-HEIGHT}}
                                                                      :tabBarIcon (fn [props]
                                                                                    (r/as-element
                                                                                     [Icon {:name (if props.focused "ios-checkbox" "ios-checkbox-outline")
                                                                                            :size s/txt-xlg
                                                                                            :color (if props.focused s/primary s/black)}]))}}
                    :ScheduleTab #js {:screen (r/reactify-component ScheduleStack)
                                      :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                  :style #js {:height constance/TABBAR-HEIGHT}}
                                                              :tabBarIcon (fn [props]
                                                                            (r/as-element
                                                                             [Icon {:name (if props.focused "md-calendar" "ios-calendar")
                                                                                    :size s/txt-xlg
                                                                                    :color (if props.focused s/primary s/black)}]))}}
                    :JoinedAttendersTab #js {:screen (r/reactify-component JoinedAttendersStack)
                                             :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                         :style #js {:height constance/TABBAR-HEIGHT}}
                                                                     :tabBarIcon (fn [props]
                                                                                   (r/as-element
                                                                                     [Icon {:name (if props.focused "md-people" "ios-people")
                                                                                            :size s/txt-xlg
                                                                                            :color (if props.focused s/primary s/black)}]))}}}))
