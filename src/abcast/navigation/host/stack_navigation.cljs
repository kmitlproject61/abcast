(ns abcast.navigation.host.stack-navigation
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.states.global-state :as state]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.navigation.host.drawer-button :refer [HostDrawerButton]]
            [abcast.screens.host.main.main :refer [MainScreen]]
            [abcast.screens.host.attenderchecking.attender-checking :refer [AttenderCheckingScreen]]
            [abcast.screens.host.attendercheckingconclude.attenders-checking-conclude :refer [AttenderCheckingConcludeScreen]]
            [abcast.screens.host.main.timeline-aday :refer [TimelineAday]]
            [abcast.screens.host.schedule.schedule :refer [ScheduleScreen]]
            [abcast.screens.host.schedule.menu-schedule :refer [MenuScheduleScreen]]
            [abcast.screens.host.main.chart-schedule :refer [ChartScheduleScreen]]
            [abcast.screens.host.attendercheckingconclude.chart-conclude :refer [ChartConcludeScreen]]
            [abcast.screens.host.joinedattenders.joinedattenders :refer [JoinedAttenders]]))

(def MainStack (createStackNavigator
                #js {:MainScreen #js {:screen (r/reactify-component MainScreen)
                                      :navigationOptions #js {:headerLeft (fn []
                                                                            (r/as-element
                                                                             [c/View {:style {:width constance/WIDTH
                                                                                              :padding-right s/ml
                                                                                              :flex-direction "row"
                                                                                              :justify-content "space-between"}}
                                                                              [HostDrawerButton]]))
                                                              :headerStyle #js {:elevation 0 :backgroundColor s/white}}}
                     :TimelineAdayScreen #js {:screen (r/reactify-component TimelineAday)
                                              :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}
                                                                      :headerRight (r/as-element [c/View {:style {:flex-direction "row"}}
                                                                                                  [c/TouchableOpacity {:style {:margin-right 20} :on-press #(.navigate @state/timeline-aday-navigation "ChartScheduleScreen")}
                                                                                                   [c/Icon {:name "md-trending-up" :size s/txt-lg :color s/black}]]])}}
                                                                                                ;   [c/TouchableOpacity {:on-press #(js/alert "Alert filter")}
                                                                                                ;    [c/Icon {:name "md-funnel" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]]])}}
                     :ChartScheduleScreen #js {:screen (r/reactify-component ChartScheduleScreen)
                                               :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))

(def AttenderCheckingStack (createStackNavigator
                            #js {:AttenderCheckingScreen #js {:screen (r/reactify-component AttenderCheckingScreen)
                                                              :navigationOptions #js {:headerLeft (fn []
                                                                                                    (r/as-element
                                                                                                     [HostDrawerButton]))
                                                                                      :headerStyle #js {:elevation 0 :backgroundColor s/white}}}
                                 :AttenderCheckingConcludeScreen #js {:screen (r/reactify-component AttenderCheckingConcludeScreen)
                                                                      :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}
                                                                                              :headerRight (r/as-element [c/View {:style {:flex-direction "row"}}
                                                                                                                          [c/TouchableOpacity {:style {:margin-right 20} :on-press #(.navigate @state/checking-conclude-navigation "ChartConcludeScreen")}
                                                                                                                           [c/Icon {:name "md-trending-up" :size s/txt-lg :color s/black}]]])}}
                                                                                                                        ;   [c/TouchableOpacity {:on-press #(reset! state/filter-modal-conclude true)}
                                                                                                                        ;    [c/Icon {:name "md-funnel" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]]])}}
                                 :ChartConcludeScreen #js {:screen (r/reactify-component ChartConcludeScreen)
                                                           :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))

(def ScheduleStack (createStackNavigator
                    #js {:ScheduleScreen #js {:screen (r/reactify-component ScheduleScreen)
                                              :navigationOptions #js {:headerLeft (fn []
                                                                                    (r/as-element
                                                                                     [HostDrawerButton]))
                                                                      :headerStyle #js {:elevation 0 :backgroundColor s/white}}}
                         :MenuScheduleScreen #js {:screen (r/reactify-component MenuScheduleScreen)
                                                  :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}
                                                                          :headerLeft (r/as-element [c/TouchableOpacity {:style {:padding-left s/ml}
                                                                                                                         :on-press #(.goBack @state/menu-schedule-navigation)}
                                                                                                     [c/Icon {:name "md-close" :color s/black :size s/txt-lg :style {:margin-right s/ml}}]])}}}))

(def JoinedAttendersStack (createStackNavigator
                            #js {:JoinedAttendersScreen #js {:screen (r/reactify-component JoinedAttenders)
                                                             :navigationOptions #js {:headerLeft (fn []
                                                                                                   (r/as-element
                                                                                                     [HostDrawerButton]))
                                                                                     :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))
