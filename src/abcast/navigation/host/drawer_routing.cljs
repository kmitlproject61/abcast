(ns abcast.navigation.host.drawer-routing
  (:require [reagent.core :as r]
            [abcast.constance :as constance]
            [abcast.navigation.host.tabbar-navigation :refer [HostTabs]]
            [abcast.screens.selectclass.stack-selectclass :refer [SelectClassStack]]
            [abcast.navigation.host.stack-setting :refer [SettingStack]]))

(def host-drawer-routing
  "Add drawer screen here"
  #js {:SelectClassDrawer #js {:screen (r/reactify-component SelectClassStack)
                               :navigationOptions #js {:drawerLockMode "locked-closed"}}
       :HostDrawer #js {:screen (r/reactify-component HostTabs)
                        :navigationOptions #js {:drawerLockMode "locked-closed"}}
       :SettingDrawer #js {:screen (r/reactify-component SettingStack)
                           :navigationOptions #js {:drawerLockMode "locked-closed"}}})