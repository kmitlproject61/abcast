(ns abcast.navigation.host.drawer
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.navigation.host.drawer-routing :refer [host-drawer-routing]]
            [abcast.navigation.host.drawer-screen :refer [HostDrawerScreen]]))

(def HostDrawerNavigator (c/createDrawerNavigator
                          host-drawer-routing
                          #js {:contentComponent (r/reactify-component HostDrawerScreen)}))

(def Routing (r/adapt-react-class HostDrawerNavigator))

(defn HostMain []
  (fn []
    (if js/goog.DEBUG
      [Routing {:persistenceKey "NavigationState"}]
      [Routing])))

(comment
  (-> (.removeItem c/AsyncStorage "NavigationState")
      (.then (fn [r] (prn "Removed"))))
  (-> (.removeItem c/AsyncStorage "selected-class")
      (.then (fn [r] (prn "Removed"))))
  (-> (.getAllKeys c/AsyncStorage)
      (.then (fn [r] (prn r)))))