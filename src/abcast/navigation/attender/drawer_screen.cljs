(ns abcast.navigation.attender.drawer-screen
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.styles :as s]
            [abcast.states.attender-global-state :as state]
            [abcast.navigation.attender.drawer-routing :refer [attender-drawer-routing]]
            [abcast.screens.attender.main.tabview-main :refer [TabViewMain pre-get-attendance-timeline]]
            [abcast.components.loading :refer [LoadingScreen]]
            [abcast.requires.images :as image]))

;; --------------------------
;; ---------- Atom ----------

(def colors [:darkblue
             :darkcyan
             :darkgreen
             :darkkhaki
             :darkmagenta
             :darkolivegreen
             :darkorange
             :darkorchid
             :darkred
             :darksalmon
             :darkviolet])

(def ViewAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:delayChildren 80
                                                                         :staggerChildren 70}})))
(def ItemAnimated (r/adapt-react-class (.View c/posed #js {:visible #js {:x 0 :transition #js {:ease "easeOut" :duration 200}}
                                                           :hidden #js {:x -200 :transition #js {:duration 10}}})))

;; --------------------------
;; -------- Function --------

(defn selected-class [navigation classroom]
  (swap! state/selected-class {} classroom)
  (.navigate navigation "AttenderDrawer")
  (reset! TabViewMain #(fn [] [LoadingScreen]))
  (pre-get-attendance-timeline)
  (.closeDrawer navigation))

;; --------------------------
;; ---------- View ----------

(defn Header []
  (fn [navigation]
    [c/View {:style {:width "100%" :height 110 :background-color s/default :padding 10 :justify-content "space-between"}}
     [c/Image {:source {:uri (.-photo @state/account)}
               :style {:width 50 :height 50 :border-radius 25}}]
     [c/View {:style {:flex-direction "row" :justify-content "space-between" :align-items "center"}}
      [c/Text {:style {:color s/white :font-size s/txt-sm}} (.-name @state/account)]
      [c/TouchableOpacity {:style {:width 30 :height 30 :justify-content "center" :align-items "center"}
                           :on-press #(.navigate navigation "SettingDrawer")}
       [c/Icon {:name "ios-settings" :size s/txt-md :color s/white}]]]
     [c/View {:width "120%" :height 110 :position "absolute" :opacity 0.6 :right 0 :background-color s/black :zIndex -1}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 90 :height 90 :zIndex -1 :left (rand-nth [10 125 250])}}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 30 :height 30 :zIndex -1 :left (rand-nth [10 125 250])}}]
     [c/Image {:source image/logo-large :style {:position "absolute" :opacity 0.3 :width 190 :height 90 :zIndex -1 :left (rand-nth [10 125 250])}}]]))

(defn AttenderDrawerScreen []
  (fn [{:keys [navigation]} props]
    [c/ScrollView
     [ViewAnimated {:style {:background-color s/white} :pose (if (= @state/drawer-screen-visible true) "visible" "hidden")}
      [Header navigation]
      [ItemAnimated {:style {:padding 10 :background-color s/white :margin-top 2}}
       [c/View {:style {:padding 10 :flex-direction "row" :justify-content "space-between"}}
        [c/View {:style {:width 50}}
         [c/Icon {:name "ios-book" :color s/black :size s/txt-md}]]
        [c/TouchableOpacity {:style {:width 200} :on-press #(.navigate navigation "SelectClassDrawer")}
         [c/Text {:style {:color s/black :font-size s/txt-sm}} "All Courses"]]]]
      [c/View {:style {:background-color s/default :height 1}}]
      [c/View {:style {:padding 10 :background-color s/white}}
       [c/Text {:style {:color s/black :font-size s/txt-sm :padding-bottom 20}} "Courses"]
       (if (empty? @state/classes)
         [ItemAnimated {:style {:padding 10 :flex-direction "row" :padding-bottom 20}}
          [c/View {:style {:width 200}}
           [c/Text {:style {:color s/black :font-size s/txt-sm :opacity 0.5}} "No Course"]]]
         (for [[i classroom] (map-indexed vector @state/classes)]
           [ItemAnimated {:key i}
            [c/TouchableOpacity {:style {:padding 10 :flex-direction "row" :padding-bottom 20}
                                 :on-press (fn [] (selected-class navigation classroom))}
             [c/View {:style {:width 50}}
              [c/View {:width s/txt-lg :height s/txt-lg :border-radius (/ s/txt-lg 2) :justify-content "center" :align-items "center" :background-color (rand-nth colors)}
               [c/Text {:style {:color s/white :font-size s/txt-xsm}} (first (get classroom "3"))]]]
             [c/View {:style {:width 200}}
              [c/Text {:style {:color s/black :font-size s/txt-sm}} (get classroom "3")]]]]))]]]))
