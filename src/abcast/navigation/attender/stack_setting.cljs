(ns abcast.navigation.attender.stack-setting
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.screens.attender.main.setting.setting :refer [SettingScreen]]
            [abcast.navigation.attender.drawer-button :refer [AttenderDrawerButton]]))

(def SettingStack (createStackNavigator
                   #js {:SettingScreen #js {:screen (r/reactify-component SettingScreen)
                                            :navigationOptions #js {:headerLeft (fn []
                                                                                  (r/as-element
                                                                                   [c/View {:style {:width constance/WIDTH
                                                                                                    :padding-right s/ml
                                                                                                    :flex-direction "row"
                                                                                                    :justify-content "space-between"}}
                                                                                    [AttenderDrawerButton]]))
                                                                    :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))
