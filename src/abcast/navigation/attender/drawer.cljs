(ns abcast.navigation.attender.drawer
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]
            [abcast.navigation.attender.drawer-routing :refer [attender-drawer-routing]]
            [abcast.navigation.attender.drawer-screen :refer [AttenderDrawerScreen]]))

(def AttenderDrawerNavigator (c/createDrawerNavigator
                              attender-drawer-routing
                              #js {:contentComponent (r/reactify-component AttenderDrawerScreen)}))

(def Routing (r/adapt-react-class AttenderDrawerNavigator))

(defn AttenderMain []
  (fn []
    (if js/goog.DEBUG
      [Routing {:persistenceKey "NavigationState"}]
      [Routing])))

(comment
  (-> (.removeItem c/AsyncStorage "NavigationState")
      (.then (fn [r] (prn "Removed"))))
  (-> (.removeItem c/AsyncStorage "selected-class")
      (.then (fn [r] (prn "Removed"))))
  (-> (.getAllKeys c/AsyncStorage)
      (.then (fn [r] (prn r)))))