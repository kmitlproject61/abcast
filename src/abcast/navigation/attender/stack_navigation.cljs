(ns abcast.navigation.attender.stack-navigation
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.constance :as constance]
            [abcast.requires.library :as c :refer [createStackNavigator]]
            [abcast.navigation.attender.drawer-button :refer [AttenderDrawerButton]]
            [abcast.screens.attender.main.mainscreen :refer [MainScreen]]
            [abcast.screens.attender.schedule.schedule :refer [ScheduleScreen]]
            [abcast.screens.attender.schedule.absent-letter :refer [AbsentLetterScreen]]
            [abcast.screens.attender.pairbluetooth.pairbluetooth :refer [PairScreen]]))

(def MainStack (createStackNavigator
                #js {:MainScreen #js {:screen (r/reactify-component MainScreen)
                                      :navigationOptions #js {:headerLeft (fn []
                                                                            (r/as-element
                                                                             [c/View {:style {:width constance/WIDTH
                                                                                              :padding-right s/ml
                                                                                              :flex-direction "row"
                                                                                              :justify-content "space-between"}}
                                                                              [AttenderDrawerButton]]))
                                                              :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))

(def ScheduleStack (createStackNavigator
                    #js {:ScheduleScreen #js {:screen (r/reactify-component ScheduleScreen)
                                              :navigationOptions #js {:headerLeft (fn []
                                                                                    (r/as-element
                                                                                     [c/View {:style {:width constance/WIDTH
                                                                                                      :padding-right s/ml
                                                                                                      :flex-direction "row"
                                                                                                      :justify-content "space-between"}}
                                                                                      [AttenderDrawerButton]]))
                                                                      :headerStyle #js {:elevation 0 :backgroundColor s/white}}}
                         :AbsentLetterScreen #js {:screen (r/reactify-component AbsentLetterScreen)
                                                  :navigationOptions #js {:headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))

(def PairStack (createStackNavigator
                #js {:PairScreen #js {:screen (r/reactify-component PairScreen)
                                      :navigationOptions #js {:headerLeft (fn []
                                                                            (r/as-element
                                                                             [c/View {:style {:width constance/WIDTH
                                                                                              :padding-right s/ml
                                                                                              :flex-direction "row"
                                                                                              :justify-content "space-between"}}
                                                                              [AttenderDrawerButton]]))
                                                              :headerStyle #js {:elevation 0 :backgroundColor s/white}}}}))
