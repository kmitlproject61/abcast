(ns abcast.navigation.attender.tabbar-navigation
  (:require [reagent.core :as r]
            [abcast.styles :as s]
            [abcast.requires.library :refer [createBottomTabNavigator Icon]]
            [abcast.constance :as constance]
            [abcast.navigation.attender.stack-navigation :refer [MainStack ScheduleStack PairStack]]))

(def AttenderTabs (createBottomTabNavigator
                   #js {:MainTab #js {:screen (r/reactify-component MainStack)
                                      :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                  :style #js {:height constance/TABBAR-HEIGHT}}
                                                              :tabBarIcon (fn [props]
                                                                            (r/as-element
                                                                             [Icon {:name "ios-home"
                                                                                    :size s/txt-xlg
                                                                                    :color (if props.focused s/primary s/black)}]))}}
                        :ScheduleTab #js {:screen (r/reactify-component ScheduleStack)
                                          :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                      :style #js {:height constance/TABBAR-HEIGHT}}
                                                                  :tabBarIcon (fn [props]
                                                                                (r/as-element
                                                                                 [Icon {:name (if props.focused "md-calendar" "ios-calendar")
                                                                                        :size s/txt-xlg
                                                                                        :color (if props.focused s/primary s/black)}]))}}
                        :PairTab #js {:screen (r/reactify-component PairStack)
                                      :navigationOptions #js {:tabBarOptions #js {:showLabel false
                                                                                  :style #js {:height constance/TABBAR-HEIGHT}}
                                                              :tabBarIcon (fn [props]
                                                                            (r/as-element
                                                                             [Icon {:name "md-bluetooth"
                                                                                    :size s/txt-xlg
                                                                                    :color (if props.focused s/primary s/black)}]))}}}))
