(ns abcast.navigation.attender.drawer-routing
  (:require [reagent.core :as r]
            [abcast.navigation.attender.tabbar-navigation :refer [AttenderTabs]]
            [abcast.screens.selectclass.attender-stack-selectclass :refer [SelectClassStack]]
            [abcast.navigation.attender.stack-setting :refer [SettingStack]]))

(def attender-drawer-routing
  "Add drawer screen here"
  #js {:SelectClassDrawer #js {:screen (r/reactify-component SelectClassStack) :name "Main"
                               :navigationOptions #js {:drawerLockMode "locked-closed"}}
       :AttenderDrawer #js {:screen (r/reactify-component AttenderTabs) :name "Main"
                            :navigationOptions #js {:drawerLockMode "locked-closed"}}
       :SettingDrawer #js {:screen (r/reactify-component SettingStack)
                           :navigationOptions #js {:drawerLockMode "locked-closed"}}})
