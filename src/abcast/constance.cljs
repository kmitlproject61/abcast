(ns abcast.constance
  (:require [reagent.core :as r]
            [abcast.requires.library :as c]))

(def WIDTH (.-width (.get c/dimensions "window")))
(def HEIGHT (.-height (.get c/dimensions "window")))
(defonce times-to-present 3)
(def TABBAR-HEIGHT 42)
(def NAVIGATIONBAR 56)
(def STATUSBAR-HEIGHT (.-currentHeight c/statusbar))
(def MONTH ["Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"])
(def DAYS ["Mon" "Tue" "Wed" "Thu" "Fri" "Sat" "Sun"])
(def FULLDAYS ["Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday"])
(defonce wallet-pass "root")


; API
(def api "http://api.z3n.pw/api/v1/")
(defonce api-register (str api "register"))
(defonce api-login (str api "login"))
(defonce api-excel (str api "import-excel"))
(defonce api-export-excel (str api "export-excel"))
(defonce get-address (str api "admin/address"))