package com.abcast;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.bitgo.randombytes.RandomBytesPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.ocetnik.timer.BackgroundTimerPackage;
import com.reactlibrary.RNBluetoothInfoPackage;
import io.github.douglasjunior.ReactNativeEasyBluetooth.classic.ClassicPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.horcrux.svg.SvgPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.abcast.AbcastBluetoothPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new RNFetchBlobPackage(),
            new ReactNativeDocumentPicker(),
            new ReactNativeRestartPackage(),
            new SplashScreenReactPackage(),
            new RandomBytesPackage(),
            new RNGoogleSigninPackage(),
            new BackgroundTimerPackage(),
            new RNBluetoothInfoPackage(),
                    new ClassicPackage(),
                    new VectorIconsPackage(),
                    new SvgPackage(),
                    new AbcastBluetoothPackage()
                    );
        }


    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
