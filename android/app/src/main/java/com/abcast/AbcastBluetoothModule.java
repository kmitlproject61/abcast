package com.abcast;



import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;


public class AbcastBluetoothModule extends ReactContextBaseJavaModule {

    public AbcastBluetoothModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AbcastBluetooth";
    }

    @ReactMethod
    public void switchBluetooth(Callback callback) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean isEnabled = bluetoothAdapter.isEnabled();
        if(!isEnabled) {
            callback.invoke(bluetoothAdapter.enable());
        }
        else {
            callback.invoke(bluetoothAdapter.disable());
        }
    }

    @ReactMethod
    public void getPairedDevices(Callback callback) {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        List<String> s = new ArrayList<>();
        for(BluetoothDevice bt : pairedDevices) {
            JSONObject device = new JSONObject();
            try {
                device.put("name", bt.getName());
                device.put("address", bt.getAddress());
            } catch (Exception e) {
                Log.e("ERROR", "JSON cannot encode");
            }
            s.add(device.toString());
        }
        callback.invoke(s.toString());
    }

    @ReactMethod
    public void getAddress(Callback callback) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        if (String.valueOf(Build.VERSION.RELEASE).equals("8.1.0")) {
            try {
                List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface nif : all) {
                    if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                    byte[] macBytes = nif.getHardwareAddress();

                    if (macBytes != null) {

                        StringBuilder res1 = new StringBuilder();
                        for (byte b : macBytes) {
                            if (b < 16 && b >= 0)
                                res1.append("0" + Integer.toHexString(b & 0xF) + ":");
                            else
                                res1.append(Integer.toHexString(b & 0xFF) + ":");
                        }

                        if (res1.length() > 0) {
                            res1.deleteCharAt(res1.length() - 1);
                        }

                        // UPDATE hardware address (wifi address) to bluetooth address by decrease last char by 1
                        String address = res1.toString();
                        String last_addr = address.substring(address.length() - 1);
                        int dec_last_addr = Integer.parseInt(last_addr, 16);
                        String last_bt_addr = Integer.toHexString(dec_last_addr - 1);
                        String bt_address = address.substring(0, address.length()-1) + last_bt_addr;

                        callback.invoke(bt_address.toUpperCase());
                    }
                }
            } catch (Exception ex) {
                callback.invoke("");
            }
        } else {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            String bluetoothMacAddress = "";
            try {
                Field mServiceField = bluetoothAdapter.getClass().getDeclaredField("mService");
                mServiceField.setAccessible(true);

                Object btManagerService = mServiceField.get(bluetoothAdapter);

                if (btManagerService != null) {
                    bluetoothMacAddress = (String) btManagerService.getClass().getMethod("getAddress").invoke(btManagerService);
                }
            } catch (Exception error) {
                callback.invoke("");
            }
            callback.invoke(bluetoothMacAddress);
        }

    }
}
