# ABCAS

. Application Of Blockchain For Class Attendance Systems 

  - Check attedance with bluetooth
  - Store data on blockchain
  - Distribute application

# System requirement
  - NodeJS (Tested in v.11.x.x)
  - lein V2.9.1
  - react-native V0.55.4
  - react-native-cli V2.0.1
  - Android (API >= 16)

# Technical
- React native with ClojureScript
- web3.js
- Figwheel Repl

# Installation
  1.  ``` yarn install ``` or ``` npm install ```
  2. ``` react-native run-android ```
  3. ``` ./node_modules/.bin/rn-nodeify --hack --install ``` // Make sure you don't have shim.js file before use this command
# Run development
  1. ``` rlwrap lein repl ```  (or ``` rlwrap figwheel lein figwheel ``` and skip step 2)
  2. ``` (start-figwheel "android") ```
  2. Open other session
  3. ``` yarn start ```

# Note
- Support only android

# License
MIT

---
# Author
- ### Jitiwattana Robru  (babyjazz)